import React from "react"
import { useHistory } from "react-router-dom"
import { useSetRecoilState } from "recoil"
import {
  currentCurriculumState,
  searchbarState,
  createDialogState,
} from "../../state/app"

export const Settings = () => {
  const setSearchbar = useSetRecoilState(searchbarState)
  const setCreateDialog = useSetRecoilState(createDialogState)

  let history = useHistory()
  history.listen(() => {
    setCreateDialog(false)
    setSearchbar("")
  })

  return (
    <div id="content-container" className="">
      <div id="content">settings</div>
    </div>
  )
}
