import React, { useEffect, useState } from "react"
import { mutate } from "swr"
import { useRecoilValue, useSetRecoilState } from "recoil"
import {
  selectedCurriculumForDetailsState,
  snackbarTextState,
  snackbarVisibleState,
} from "../../state/app"
import {
  createStyles,
  lighten,
  makeStyles,
  Theme,
} from "@material-ui/core/styles"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import DeleteIcon from "@material-ui/icons/DeleteRounded"
import {
  Checkbox,
  Divider,
  IconButton,
  Paper,
  TableContainer,
  Toolbar,
  Tooltip,
  Typography,
  TextField,
  FormControl,
  MenuItem,
  Select,
  FormControlLabel,
  FormGroup,
} from "@material-ui/core"
import { Dialog } from "../Dialog"
import Button from "@material-ui/core/Button/Button"
import AddIcon from "@material-ui/icons/AddRounded"
import { CurriculumDetailsMOT } from "."
import {
  checkValidationErrors,
  DialogError,
  Errors,
  ExceptionResponse,
  ExceptionResponseState,
} from "../../utils"
import { newMOTTaskValidationObject } from "./ValidationObjects"
import { ValidationError } from "yup"

interface HeadCell {
  disablePadding: boolean
  id: keyof CurriculumDetailsMOT
  label: string
  numeric: boolean
  minWidth: number
}

const headCells: HeadCell[] = [
  {
    id: "name",
    numeric: false,
    disablePadding: false,
    label: "Taak",
    minWidth: 200,
  },
  {
    id: "type",
    numeric: false,
    disablePadding: false,
    label: "Type",
    minWidth: 150,
  },
  {
    id: "hours",
    numeric: true,
    disablePadding: false,
    label: "Uren",
    minWidth: 75,
  },
  {
    id: "active_op1",
    numeric: false,
    disablePadding: false,
    label: "OP1",
    minWidth: 40,
  },
  {
    id: "active_op2",
    numeric: false,
    disablePadding: false,
    label: "OP2",
    minWidth: 40,
  },
  {
    id: "active_op3",
    numeric: false,
    disablePadding: false,
    label: "OP3",
    minWidth: 40,
  },
  {
    id: "active_op4",
    numeric: false,
    disablePadding: false,
    label: "OP4",
    minWidth: 40,
  },
]

interface EnhancedTableProps {
  numSelected: number
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void
  rowCount: number
}

const EnhancedTableHead = (props: EnhancedTableProps) => {
  const { onSelectAllClick, numSelected, rowCount } = props

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            padding={headCell.disablePadding ? "none" : "default"}
            style={{ minWidth: `${headCell.minWidth}px` }}
          >
            {headCell.label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
}

interface EnhancedTableToolbarProps {
  numSelected: number
  setDeleteConfirmationDialog: React.Dispatch<React.SetStateAction<boolean>>
}

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === "light"
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: "1 1 100%",
    },
  })
)

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  const classes = useToolbarStyles()
  const { numSelected, setDeleteConfirmationDialog } = props

  return (
    <Toolbar>
      {numSelected > 0 ? (
        <Typography
          className={classes.title}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          {numSelected} geselecteerd
        </Typography>
      ) : (
        <Typography
          className={classes.title}
          variant="h6"
          id="tableTitle"
          component="div"
        >
          MOT taken
        </Typography>
      )}
      {numSelected > 0 && (
        <Tooltip title="Verwijder">
          <IconButton
            aria-label="Verwijder"
            onClick={() => setDeleteConfirmationDialog(true)}
          >
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      )}
    </Toolbar>
  )
}

type CurriculumMOTTableProps = {
  errors: { message: string; index: number; path: string }[]
  mot_tasks: CurriculumDetailsMOT[]
  setCurriculumMOTTasks: (mot_tasks: CurriculumDetailsMOT[]) => void
}

export type newMOTTaskType = {
  type: string
  name: string
  hours: number
  active_op1: boolean
  active_op2: boolean
  active_op3: boolean
  active_op4: boolean
}

export const CurriculumMOTTable = (props: CurriculumMOTTableProps) => {
  const { errors, mot_tasks, setCurriculumMOTTasks } = props
  const selectedCurriculum = useRecoilValue(selectedCurriculumForDetailsState)
  const setSnackbarText = useSetRecoilState(snackbarTextState)
  const setSnackbarVisible = useSetRecoilState(snackbarVisibleState)
  const [selected, setSelected] = useState<CurriculumDetailsMOT[]>([])
  const [createMOTTaskDialog, setCreateMOTTaskDialog] = useState<boolean>(false)
  const [newMOTTask, setNewMOTTask] = useState<newMOTTaskType>({
    type: "Organisatorische",
    name: "",
    hours: 0,
    active_op1: false,
    active_op2: false,
    active_op3: false,
    active_op4: false,
  })
  const [newMOTTaskErrors, setnewMOTTaskErrors] = useState<
    Errors<newMOTTaskType>[]
  >([])
  const [
    newMOTTaskCreateErrorMessage,
    setNewMOTTaskCreateErrorMessage,
  ] = useState<ExceptionResponseState>({
    status: -1,
    statusText: "",
    type: "",
    message: "",
  })
  const [deleteConfirmationDialog, setDeleteConfirmationDialog] = useState<
    boolean
  >(false)

  useEffect(() => {
    if (!createMOTTaskDialog) {
      setNewMOTTask({
        type: "Organisatorische",
        name: "",
        hours: 0,
        active_op1: false,
        active_op2: false,
        active_op3: false,
        active_op4: false,
      })
      setnewMOTTaskErrors([])
    }
  }, [createMOTTaskDialog])

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = mot_tasks.map((n) => n)
      setSelected(newSelecteds)
      return
    }
    setSelected([])
  }

  const handleClick = (
    event: React.MouseEvent<unknown>,
    task: CurriculumDetailsMOT
  ) => {
    const selectedIndex = selected.indexOf(task)
    let newSelected: CurriculumDetailsMOT[] = []

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, task)
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1))
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1))
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      )
    }

    setSelected(newSelected)
  }

  const handleChange = (task: CurriculumDetailsMOT) => {
    if (process.env.NODE_ENV === "development")
      console.log(`[${task.id}] ${task.name}: ${task.hours}`)

    let new_mot_tasks = mot_tasks
    const index = new_mot_tasks.findIndex((t) => t.id === task.id)
    new_mot_tasks[index] = task

    setCurriculumMOTTasks(new_mot_tasks)
  }

  const createMOTTask = () => {
    newMOTTaskValidationObject
      .validate(newMOTTask, { abortEarly: false })
      .then(() => {
        setnewMOTTaskErrors([])
        fetch(`/api/curricula/${selectedCurriculum}/mot/create`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(newMOTTask),
        })
          .then(async (r) => {
            if (r.ok) {
              mutate(`/api/curricula/${selectedCurriculum}`)
              setCreateMOTTaskDialog(false)
              setNewMOTTaskCreateErrorMessage({
                status: -1,
                statusText: "",
                type: "",
                message: "",
              })
              setSnackbarText("De MOT Taak is aangemaakt")
              setSnackbarVisible(true)
            } else {
              const json = (await r.json()) as ExceptionResponse
              setNewMOTTaskCreateErrorMessage({
                status: r.status,
                statusText: r.statusText,
                type: json.type,
                message: json.message,
              })
            }
          })
          .catch((e) => {
            console.log(e)
            setNewMOTTaskCreateErrorMessage({
              status: -1,
              statusText: "Unknown",
              type: "Unknown",
              message:
                "Is is iets fout gegaan bij het aanmaken van de MOT taak.",
            })
          })
      })
      .catch((err: ValidationError) =>
        checkValidationErrors<newMOTTaskType>(err, setnewMOTTaskErrors)
      )
  }

  const deleteSelected = () => {
    const payload = selected
    fetch(`/api/curricula/${selectedCurriculum}/mot/delete-multiple`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    })
      .then((r) => {
        if (r.ok) {
          setDeleteConfirmationDialog(false)
          setSnackbarText(
            `De geselecteerde MOT ${
              selected.length > 1 ? "taken" : "taak"
            } is verwijderd.`
          )
          setSnackbarVisible(true)
          setSelected([])
          mutate(`/api/curricula/${selectedCurriculum}`)
        }
      })
      .catch((e) => {
        console.log(e)
        setSnackbarText(
          `Is is iets fout gegaan bij het verwijderen van de MOT ${
            selected.length > 1 ? "taken" : "taak"
          }.`
        )
        setSnackbarVisible(true)
      })
  }

  const isSelected = (task: CurriculumDetailsMOT) =>
    selected.indexOf(task) !== -1

  return (
    <Paper elevation={0} variant="outlined" className="curriculum-mot-table">
      <EnhancedTableToolbar
        numSelected={selected.length}
        setDeleteConfirmationDialog={setDeleteConfirmationDialog}
      />
      <Divider />
      <TableContainer>
        <Table>
          <EnhancedTableHead
            numSelected={selected.length}
            onSelectAllClick={handleSelectAllClick}
            rowCount={mot_tasks.length}
          />
          <TableBody>
            {mot_tasks.length > 0 ? (
              <>
                <TableRow>
                  <TableCell padding="checkbox">
                    <IconButton onClick={() => setCreateMOTTaskDialog(true)}>
                      <AddIcon />
                    </IconButton>
                  </TableCell>
                  <TableCell
                    colSpan={headCells.length + 1}
                    component="th"
                    scope="row"
                  >
                    Maak een nieuwe MOT taak aan
                  </TableCell>
                </TableRow>
                {mot_tasks.map((task, key) => {
                  const isItemSelected = isSelected(task)

                  return (
                    <TableRow key={key}>
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          onClick={(event) => handleClick(event, task)}
                        />
                      </TableCell>
                      <TableCell component="th" scope="row">
                        <TextField
                          variant="outlined"
                          size="small"
                          value={task.name}
                          onChange={(e) =>
                            handleChange({
                              ...task,
                              name: e.target.value,
                            })
                          }
                          type="text"
                          error={errors.some(
                            (e) => e.index === key && e.path === "name"
                          )}
                          helperText={
                            errors.find(
                              (e) => e.index === key && e.path === "name"
                            )?.message
                          }
                        />
                      </TableCell>
                      <TableCell scope="row">
                        <FormControl variant="outlined" size="small">
                          <Select
                            value={task.type}
                            onChange={(e) => {
                              handleChange({
                                ...task,
                                type: e.target.value as string,
                              })
                            }}
                          >
                            <MenuItem value={"Organisatorische"}>
                              Organisatorische
                            </MenuItem>
                            <MenuItem value={"Professionalisering"}>
                              Professionalisering
                            </MenuItem>
                          </Select>
                        </FormControl>
                      </TableCell>
                      <TableCell scope="row">
                        <TextField
                          variant="outlined"
                          size="small"
                          value={task.hours}
                          onChange={(e) =>
                            handleChange({
                              ...task,
                              hours:
                                e.target.value.length > 0
                                  ? Number(e.target.value)
                                  : ((e.target.value as unknown) as number),
                            })
                          }
                          type="number"
                          error={errors.some(
                            (e) => e.index === key && e.path === "hours"
                          )}
                          helperText={
                            errors.find(
                              (e) => e.index === key && e.path === "hours"
                            )?.message
                          }
                        />
                      </TableCell>
                      <TableCell scope="row">
                        <Checkbox
                          checked={task.active_op1}
                          onChange={(e) =>
                            handleChange({
                              ...task,
                              active_op1: e.target.checked,
                            })
                          }
                          inputProps={{ "aria-label": "primary checkbox" }}
                        />
                      </TableCell>
                      <TableCell scope="row">
                        <Checkbox
                          checked={task.active_op2}
                          onChange={(e) =>
                            handleChange({
                              ...task,
                              active_op2: e.target.checked,
                            })
                          }
                          inputProps={{ "aria-label": "primary checkbox" }}
                        />
                      </TableCell>
                      <TableCell scope="row">
                        <Checkbox
                          checked={task.active_op3}
                          onChange={(e) =>
                            handleChange({
                              ...task,
                              active_op3: e.target.checked,
                            })
                          }
                          inputProps={{ "aria-label": "primary checkbox" }}
                        />
                      </TableCell>
                      <TableCell scope="row">
                        <Checkbox
                          checked={task.active_op4}
                          onChange={(e) =>
                            handleChange({
                              ...task,
                              active_op4: e.target.checked,
                            })
                          }
                          inputProps={{ "aria-label": "primary checkbox" }}
                        />
                      </TableCell>
                    </TableRow>
                  )
                })}
              </>
            ) : (
              <>
                <TableRow>
                  <TableCell padding="checkbox">
                    <IconButton onClick={() => setCreateMOTTaskDialog(true)}>
                      <AddIcon />
                    </IconButton>
                  </TableCell>
                  <TableCell
                    colSpan={headCells.length + 1}
                    component="th"
                    scope="row"
                  >
                    Maak een nieuwe MOT taak aan
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell colSpan={headCells.length + 1}>
                    Er zijn geen MOT taken. Maak een MOT taak aan.
                  </TableCell>
                </TableRow>
              </>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <Dialog
        open={deleteConfirmationDialog}
        onClose={() => setDeleteConfirmationDialog(false)}
        title={`MOT ${selected.length > 1 ? "taken" : "taak"} verwijderen`}
        content={() => (
          <p>
            Weet je zeker dat je de geselecteerde MOT{" "}
            {selected.length > 1 ? "taken" : "taak"} wilt verwijderen?
          </p>
        )}
        actions={() => (
          <>
            <Button onClick={() => setDeleteConfirmationDialog(false)}>
              Annuleer
            </Button>
            <Button
              disableElevation
              autoFocus
              onClick={() => deleteSelected()}
              color="primary"
              variant="contained"
            >
              Verwijder MOT {selected.length > 1 ? "taken" : "taak"}
            </Button>
          </>
        )}
      />
      <Dialog
        open={createMOTTaskDialog}
        onClose={() => setCreateMOTTaskDialog(false)}
        title={`Nieuwe MOT taak`}
        content={() => (
          <div className="card-content">
            <div className="column-3">
              <TextField
                label="Naam"
                variant="outlined"
                type="text"
                name="name"
                value={newMOTTask.name}
                onChange={(e) =>
                  setNewMOTTask({ ...newMOTTask, name: e.target.value })
                }
                error={newMOTTaskErrors.some((e) => e.path === "name")}
                helperText={
                  newMOTTaskErrors.find((e) => e.path === "name")?.message
                }
              />
              <FormControl variant="outlined">
                <Select
                  value={newMOTTask.type}
                  onChange={(e) => {
                    setNewMOTTask({
                      ...newMOTTask,
                      type: e.target.value as string,
                    })
                  }}
                >
                  <MenuItem value={"Organisatorische"}>
                    Organisatorische
                  </MenuItem>
                  <MenuItem value={"Professionalisering"}>
                    Professionalisering
                  </MenuItem>
                </Select>
              </FormControl>
              <TextField
                label="Uren"
                variant="outlined"
                type="number"
                name="hours"
                value={newMOTTask.hours}
                onChange={(e) =>
                  setNewMOTTask({
                    ...newMOTTask,
                    hours:
                      e.target.value.length > 0
                        ? Number(e.target.value)
                        : ((e.target.value as unknown) as number),
                  })
                }
                error={newMOTTaskErrors.some((e) => e.path === "hours")}
                helperText={
                  newMOTTaskErrors.find((e) => e.path === "hours")
                    ? newMOTTaskErrors.find((e) => e.path === "hours")?.message
                    : "Uren worden per OP toegewezen"
                }
              />
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
              }}
            >
              <FormGroup aria-label="position" row>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={newMOTTask.active_op1}
                      onChange={(e) =>
                        setNewMOTTask({
                          ...newMOTTask,
                          active_op1: e.target.checked,
                        })
                      }
                      color="primary"
                      inputProps={{ "aria-label": "primary checkbox" }}
                    />
                  }
                  label="OP1"
                  labelPlacement="top"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={newMOTTask.active_op2}
                      onChange={(e) =>
                        setNewMOTTask({
                          ...newMOTTask,
                          active_op2: e.target.checked,
                        })
                      }
                      color="primary"
                      inputProps={{ "aria-label": "primary checkbox" }}
                    />
                  }
                  label="OP2"
                  labelPlacement="top"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={newMOTTask.active_op3}
                      onChange={(e) =>
                        setNewMOTTask({
                          ...newMOTTask,
                          active_op3: e.target.checked,
                        })
                      }
                      color="primary"
                      inputProps={{ "aria-label": "primary checkbox" }}
                    />
                  }
                  label="OP3"
                  labelPlacement="top"
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={newMOTTask.active_op4}
                      onChange={(e) =>
                        setNewMOTTask({
                          ...newMOTTask,
                          active_op4: e.target.checked,
                        })
                      }
                      color="primary"
                      inputProps={{ "aria-label": "primary checkbox" }}
                    />
                  }
                  label="OP4"
                  labelPlacement="top"
                />
              </FormGroup>
            </div>
            {newMOTTaskCreateErrorMessage.message.length > 0 && (
              <DialogError error={newMOTTaskCreateErrorMessage} />
            )}
          </div>
        )}
        actions={() => (
          <>
            <Button onClick={() => setCreateMOTTaskDialog(false)}>
              Annuleer
            </Button>
            <Button
              disableElevation
              autoFocus
              onClick={() => createMOTTask()}
              color="primary"
              variant="contained"
            >
              Maak taak aan
            </Button>
          </>
        )}
      />
    </Paper>
  )
}
