import React, {
  Component,
  ErrorInfo,
  ReactNode,
  useEffect,
  useState,
} from "react"
import ReactPDF, {
  Text,
  Image,
  View,
  StyleSheet,
  Font,
  pdf,
  Canvas,
} from "@react-pdf/renderer"
import { getBase64Logo } from "../../utils"
import "./Export.css"
import { Button, CircularProgress } from "@material-ui/core"
import InfoRoundedIcon from "@material-ui/icons/InfoRounded"
import WarningRoundedIcon from "@material-ui/icons/WarningRounded"
import ErrorRoundedIcon from "@material-ui/icons/ErrorRounded"

Font.register({
  family: "Open Sans",
  fonts: [
    {
      src:
        "https://fonts.gstatic.com/s/opensans/v18/mem8YaGs126MiZpBA-U1Ug.ttf",
      fontWeight: 400,
    },
    {
      src:
        "https://fonts.gstatic.com/s/opensans/v18/mem5YaGs126MiZpBA-UN7rg-VQ.ttf",
      fontWeight: 700,
    },
  ],
})

export const pageDefaultStyles: ReturnType<typeof StyleSheet["resolve"]> = {
  fontFamily: "Open Sans",
  fontSize: "11pt",
  paddingTop: 35,
  paddingBottom: 35,
  paddingHorizontal: 35,
}

const ExportDefaultHeaderStyles = StyleSheet.create({
  container: {
    borderBottomWidth: "1pt",
    borderBottomStyle: "solid",
    borderBottomColor: "rgba(0, 0, 0, .12)",
    paddingBottom: "6pt",
    marginBottom: "12pt",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  title_container: {
    display: "flex",
    flexDirection: "column",
    marginRight: "auto",
  },
  title: {
    fontWeight: "bold",
  },
  subtitle: {
    fontSize: "10pt",
  },
  company_container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
  },
  company: {
    fontWeight: "bold",
    alignSelf: "flex-end",
  },
  company_tagline: {
    fontSize: "10pt",
    alignSelf: "flex-end",
  },
  logo: {
    width: "30px",
    height: "auto",
    marginLeft: "6pt",
  },
})

type ExportDefaultHeaderProps = {
  title: string
  subtitle?: string
}

export const ExportDefaultHeader = (props: ExportDefaultHeaderProps) => {
  const { title, subtitle } = props
  const styles = ExportDefaultHeaderStyles

  return (
    <View fixed style={styles.container}>
      <View style={styles.title_container}>
        <Text style={styles.title}>{title}</Text>
        {subtitle && <Text style={styles.subtitle}>{subtitle}</Text>}
      </View>
      <View style={styles.company_container}>
        <Text style={styles.company}>Hogeschool Rotterdam</Text>
        <Text style={styles.company_tagline}>
          Instituut voor Communicatie, Media en IT
        </Text>
      </View>
      <Image src={getBase64Logo()} style={styles.logo} />
    </View>
  )
}

const ExportDefaultFooterStyles = StyleSheet.create({
  container: {
    fontSize: "10pt",
    position: "absolute",
    bottom: 25,
    left: 35,
    right: 35,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  left: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  right: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  page_number: {},
})

export const ExportDefaultFooter = () => {
  const styles = ExportDefaultFooterStyles

  return (
    <View fixed style={styles.container}>
      <View style={styles.left}>
        <Text fixed>
          {new Date(Date.now()).toLocaleString()} {}
        </Text>
      </View>
      <View style={styles.right}>
        <Text
          fixed
          render={({ pageNumber, totalPages }) =>
            `Pagina ${pageNumber} van ${totalPages}`
          }
        />
      </View>
    </View>
  )
}

const ExportDefaultSectionStyles = StyleSheet.create({
  name_container: {
    display: "flex",
    flexDirection: "row",
    marginBottom: "6pt",
  },
  name_block: {
    backgroundColor: "#cc0033",
    width: "5pt",
    height: "100%",
    marginRight: "6pt",
  },
  name: {
    fontSize: "14pt",
    fontWeight: "bold",
  },
})

type ExportDefaultSectionProps = {
  name: string
}

export const ExportDefaultSection = (
  props: React.PropsWithChildren<ExportDefaultSectionProps>
) => {
  const { name, children } = props
  const styles = ExportDefaultSectionStyles

  return (
    <>
      <View style={styles.name_container}>
        <View style={styles.name_block} />
        <Text style={styles.name}>{name}</Text>
      </View>
      {children}
    </>
  )
}

const ExportInformationCardStyles = StyleSheet.create({
  container: {
    marginVertical: "6pt",
    display: "flex",
    flexDirection: "row",
    borderStyle: "solid",
    borderWidth: 1,
    borderColor: "black",
    borderRadius: 4,
    padding: 8,
  },
  image: {
    height: 15,
    marginRight: 8,
  },
  info: {
    borderColor: "#2196f3",
    backgroundColor: "#E8F4FE",
  },
  warning: {
    borderColor: "#ff9800",
    backgroundColor: "#FFF4E5",
  },
  error: {
    borderColor: "#f44336",
    backgroundColor: "#FEECEB",
  },
})

type ExportInformationCardProps = {
  type: "info" | "warning" | "error"
  maxWidth?: {
    value: number
    unit: "pt" | "in" | "mm" | "cm" | "%" | "vw" | "vh"
  }
  disableIcon?: boolean
}

export const ExportInformationCard = (
  props: React.PropsWithChildren<ExportInformationCardProps>
) => {
  const { type, maxWidth, disableIcon, children } = props
  const styles = ExportInformationCardStyles

  const InfoRoundedIcon = Buffer.from(
    "iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAZKADAAQAAAABAAAAZAAAAAAMc/x7AAAIF0lEQVR4Ae1da3LbNhBeaOx4OrFS9wRhT1D5BFZOEOcEcX7G6UztE9Q3sDtTOz8tnyDKCSKfoOoJqp6gie1MR06H6C74EEjxAVIgAVHAjIYUCGAX30dgAZBcALjgEHAI5CPA8i/Zc8W7uj8AYB4w7gGHAWq2F2jHPUbxUuDAZ5gWfyJ8BgZT4PSfz2bH/dsw3tqDdYR453wPtpGAHhsi+EPGBAHaAOQcCWIwAZ9P4Fv/dnbKPmsrXENBVhAiSNj5+hLv4iNUaKihXspFcEBygI1g/vSjDeQYJcT7/e4l3q2HjLEjZQQbTMg5H2GrHM9+fvaxQTGFRRshxLt6eA3gn6X7/0JNW7wY2KHe2ex496ZFsUJUq4TYTkQafBPEtEKI9/5+CD6c6zbQaQCb+i8GAj04nb3tT5qSEZXbKCHCWD+5RyLssBFRpesehY157J82afwbI8S7ujtEA3mNZIRzhrow2JUPSaG5zZvZ8bNxE5r1migUJ3LnaLA/dI0MworqRHWjOjaBndYW4r3/1wP/PyRC72SuiYrrKDOwLVuvZm+/m+koj8rQRoh3+TDAoeynLraKIrBFFwa9F7N3u9OidKrXtHRZYhS1gWQQyMEN6H8KMFCFPT/dyoRgyzhiHDauZciQCruCGBAWcnyd85W6LEEG49d1BHc1D+fsDXZfo7r1q00INVFqGXUFdzkfkrJf16bU6rKEAff5hy6Dulrd0KaIQU71Uiq3kGBo++2PTRtNVYVWjL562/tVh8TVW4iYZ3Rr9l0VbJX04oZFrFTSymkqEeJd3l9syqRPBqnuOWFFmFXJr9xl0doULRlUKdylDRDAZfxXqmtfSoSEq7Z/ObtR7xYT9uSx/6PKKrFal7VDXZWzG/XoCGfziKFK/tIW4uYbKjCqpeEMXpQ95NoqLQqf9OlbgiyVppRApWJW3kiEJcB+USULu6xgaWQzltKLQNJ1LRh1Fa93FRICzP9VlzKunBCBEkxzCRGtA5gXFuMOmhCgV58I27zicglxrSMPMg3xBa0kk5BwEuhpEO2KyEBAtBJ6CSQjZBKC6TITZ+R3UfURyMR4iRCalSOD+KqnC00iQBiLFZCUkCVC4MnXTOZS+dxfHQhkYJ0xMeS/6JDVaBk+f40Tv4NCGT5+3IMDf6sD49QTjWQdExqL7mrn4R85gTtvFgE+3/1BXnRMdlk7+FK0C+0ikMI8SQjHz8hcaBeBFOZJQgCK++V2Vd0UaQnME4S4x7Pt3wNpzGNCaLm6fXWcREJAxj4mBHzm0UUXDCDA8VPwMCwIARy3u2AGAc4GkWCJEPcgKgLFwDEmZDFTZ5G7CgPq1BXJ+d+c9S7Q5cZUFIF3GuP+Cc7Qn9ct0kg+CfsFIUY0qS+UA9zCY/9QnuViaRNcbRjBzsMYlyASw8n6ktrNKXVZfH3uKmwZMN9NkyGQEwThNaA0axMW2MeE0EOTddGfuqlUy0ioTtdEV5aItfePjH1MiL3qZmgW2YyMS3GUSpo4sT0n60mIPfhp18QRoh3S1QpcELJWRnC1SluXW8I+JoSz2C2edfp2XSEZ+5iQrld6Xeq3IITD53VRunN6StgvCAF0DumCKQRi7CVCnA0xxUa8FocKLAjpkb9bF4wgwBauamNCyr7sMaLohgiVsY8JobrjCuqfG4KBNdVMY54gBBmZWKPppiiSwjxJCEP32y60i0AK8yQh8+bdoLZb2zWQlsI8QYh4jkBP4lxoBQH02bjkbz5BiNCCs1Er2jghiAAbp2FYJuTx6VKidCb3XxMCGVgvERJ0W/xGk0hXTA4C6JDmJusx9BIhYX7XSnKA1BidiXEmIcKVkPTQRKMSrihCALHNc9eUSYjIA70zOloZVN5DVkljqHK8ANtcQoSrU2tbicp3kCppDDBCraPAjWwuIaRqEZMGqhKLDJy43F3HEakT7/LuOv3dRSqJsb9lmOIbl8UBdwGYYqKfilOZuYoLcxPcbe0i2jNK7GnVYyeo79CMRsVSaSERt+4bFKUqJ8Q5TC7Cr9I1FT9fhV0WSaO1ehozV5LsEi8hgMskv8nPPZYShBGlhIh08/4JWpQveYW4+DIEELvH3bOyVHRdiZBw0fFIpUCXZhkBtB1HWbPy5ZSKhFBGmshQs8sqxMXlIyC6qgr7VZUa9bQom0ddaV1N/1cZVaV1VOqyEpnY1qGzJwlEcv6g3RBY5VzOia5MCHn757w3dKTkICqi+RfCqOrOCJS1cpcVqWGlX9xIOcPH1jd0ofqK+Qlu72O47taJD7c8mtZVrHYLiQQGzpbdPlSEx6r7T1EZKxNChQTdFx9jcd/T/80Loc3QsJehFkKIAGwpA8b8yeaRoo8MwrHyKIsyZQXalYyz7QGNvbOudzGO6kp1rrsjWxYm2lqIXHi4NZL9zjRlpSueixn4O1rj0xsaIYRUDLxjk8fNrtkV7KJobarCckgVyrR1WWmhYu1r3ve6tHRPdeFYp6bIIAwbayEyQcJjGocLFGblk0dZ16xzYRcZnKg8z8jKXyWuFUIihYItMPyztXGfhC8k0DPwopcSorrpOrZKSKS09cQYICLCxgghkXAy/Hh+aIvz/9DejZu0EVHd845GCYmUErsEkGN6xo9QoYMovo0j2odboDf+8cVn1ad6TeplBSFyBQU55H6bPD4zGKKCWgcCwkDTZ2T05RJ+LGMDCXL9rSNEVi46D0Zp6Eo18N45QKL26Brj6Ek17V+R+v/Ib0vgIWEqvgPHT4/bGCVFOrujQ8Ah0AQC/wMq+qbQASHebwAAAABJRU5ErkJggg==",
    "base64"
  )
  const WarningRoundedIcon = Buffer.from(
    "iVBORw0KGgoAAAANSUhEUgAAAGQAAABaCAYAAABOkvOJAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAZKADAAQAAAABAAAAWgAAAAASYvqcAAAGfUlEQVR4Ae1dTXbbNhAG9fJeu6t7gjAniHyCMCeoblB10zQruyeoeoJ216QbuydoeoIoJ4h7giQnqLOLN2a/gUBblEQJPzMAaAHv6ZEigeHM93EAcIaQKjXS0r5W36tWzaD+CT6NMWOJ7bWq1JvqhfrLHBvVphqVtlC2faXOsFngQ0TsK9cg7OfqpbrcVym3c6MhpL0AATfqAgCSV7iUS/UViPkBnjOCMhmBjisVb9Tf2HElg9rOQSS1HUUZBSHtH7qLagIQbYyMABFxmmbfZQHIBoP0WxY4WvUcY8qSRZaQkPw9pFK/sdnOKYtNqb6grAmBd8yh7rSvctC3qZEZJESycbZdlplVfYDxJ8wAXGPW9STXWVe+HvJFnQuQQdyeqJVsZp55xGXpIehWagzk5B1ypYWXvFQf5S7gJzlPD6n0A6CfRbatYlzDVpe1etkRoqe597GpNVXZd+nZpGGXGigwO0LQVVF4JE6JeS1Li7IiBBFcGshrS905qtXmmhyyWGRkM6gLTnMPAZXVNDgfD/mifgdy3M8ch8ig8zQNpmtnUbLwkPZPPI3fqvdJEZmo0+pHdZVUB1w8Dw+5ZYxX+SKagw45EIJBdQY9Gl8cGds1RhdGke6iknZZZiCnrqp2V12kxUfEuU5TxrnSdlmrmFIuZBDDdeo4VzIPMfEq8o4UMysCf6jQyxGnqeJcKT1kAURyI4NIIp0WtJOiJPEQHUPiSstKoZYo3ZvGQ0aQSkVMjS917HDTRCcE3jGHflMHHVNVTZLujdplJYxX+ZIaPc4V10Pk0rK+gB9qFz3dG81DzDRXNi17CF7f8xHTvfE8JMNkkDU/EXWP4iGi09xWvQOwSwMuveX4zBpol4qRpsGPXHTyritzh/2LWPV8M2RuQvmX0PWpt767Gq5seLLrFOcx8S5LJC3bqk8IAjabZBAw+hjOIfzxiRMoyIqS7hXtssSmuRbdh1A3KT4NlvUQmbTsZ5s32E2dz8xeQouGFswye+LECNF9eYV1gNyldUizutS11/NM22Zf36mmGCHIkSeJBTlZ71tZ0DYRQjJKy/pCfqidWLqXnRA9kLcP2Ds6qoRsZCfEpEDrTu8HvK0xk1tw28dKiIlXnXErma28Sp1pmxkVZCUEei3wyTEtywhZTxR7upeNEPMgxj/N7dmf4RdM7bXtTKqxEZIq5cmEQ5iYSv0SJuC+NQshuEPmEDmGtOy95bx7tPiHMAguwYToaW6iFwKCrecUAAw0FoEygwkx09xjGsiHIGdJ9wZFe+Gm8qtlt82/rn5S324f3j6Cn3L6D0fj3iyB6d4wD5FJPG0j2z9yYjOrMXXikkF6Bnbf3h6iDU739uEVElTPh95SN3mY94CnJoyiF4t8zZBO/h6Sxjs6O6bIS7zdFQbXx3AOFeuucvRtADZeHqLTskLBNQ/wlugmlrpdqxps6ZO+VPgVuxfuaxedCTHdwQdYHL9/Tg+ziwZe6V73LksmLeti6FjqeqV7nTxE98+pV8uOhY5OT8fVvW4eIpi67PR/cFtHzKwJOYK0rNS94JTutSJED+T5zKqkgJOT64CdFSFHlJaVIsU63XtwUDfxKnrqLdPcMLqsVvfaeMiikBHGhGlNN/TikKS9HpI4XnVI93GePxDnerTXqsDI5V7ZviexHgTv7TY2zXFDUVjlmU3daHVW6d7l0PUGuywYM0ej6VDDctwbgb3p3p2E6Glujt7hjUFmDfeke3cSUtKy4gQOpnu3CDHTXLbXWsRNG+sFMJZorDf03yIEg+DFRp3yVQqBHcNCjxAw1uDa9CklDgIzg/nd1XqEFO+4wyXezkaPdEeIyGrZeGaN+Uq91b2aEBPNLQN5KlpbDPD0L3Qoqyf11crScQQPK/UY3mx387TqcSqMHa/bpXvPK83Mjf6vjnEQ4mjpqKrjrccJHgJnULqQkQNzEzWjMWSegy5FByBwS4RU6psCRiYIVOopecg0E3WKGhg67p5DChp5IDDBzxi9y0OVogVxQWPIVYEiEwTABXnIm0zUKWpM1OVE/65U6bbS3wz0rgD+4acb1BfpNTp6DTQHmhDtJVhgcvSQpAKAFveY/3nvPETp1T6t+jWVTkd73Y2VVlsvyum33G+xFAtR1aMFKYbh9KupE3UOR+hNqrYI6XTRCavVmr3vumNly4LAPzSzRRd1uUvaICHrlc3bEfX6sbLviMDX6mpoGfe6pP8BwMSNa+kaKkMAAAAASUVORK5CYII=",
    "base64"
  )
  const ErrorRoundedIcon = Buffer.from(
    "iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAAAXNSR0IArs4c6QAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAAAZKADAAQAAAABAAAAZAAAAAAMc/x7AAAIQ0lEQVR4Ae1dXXLbNhAGIDPTjp2pbxD1BFFmar+Gdvpe9wR2TlD3BHZOEOUEtk+Q9L1JmFenM1VOEPcG7liedGxL212QkCiKEEiJBCEJnElA4W+X32dgsSAAcub4BWFn+z8ePB0I3ubA2sAhHKkMvM04a49+0w2wK8bhSsVx4BFwdtUawtV3cP+FR71rleZiyF1T6lvYaQ9aj54zIOB5OAX4ogoTYQwixnnUGtx9+j7q4W93LicIkSSI4Dck4KByAkxYxwS9aw3v37hATmOEUFd02woOEa8jJKJjws1OOvRQl+7m4O6Ppro264QkreEEGDvgnG/bAbqcFAC4RmDOm2g11ghRRGDffVQOnoZzAxAxr2x1Z7UTIrsmEZwgEccNQ7uYeIDuJhJTd1dWKyG3L3YPANhr64Z6Mej1pXEAwDn7ffP95Tt9psVSaiEkHroGZ2ggw8XUc7U04JD5/mUd3VjlhFCrGAKcuWqwq6KYDL/g/GXVrUVUpSDV09/feY2jp7erTgY9Kz0jPSs9M/2u6qqkhSQ+xUd3/Imq4ClaD/Q2B/d7VRj8hVtIP9zpoIO3xmQQaVxiQLazKIW6fAu1ECIDBPu4Dl2UDsB0vHQoh2xvK/qMHv9819yEeDLyAV+UlLkI8WTkk6FiFyGlNCGeDAX77JBI2RjePyvrq5Qy6jSaYi228j7GbKiLpZJdHbSCtxKzYkVkrlKE+NFUCWRl1nj0VaZUYULQAequr59RBtJsXt6JscvG5/8uZEPkJCF6pflV+NgiCCDQvxaZZjESQs7Ogwj+9r5GEdj1eYoaeWOXhYbJG3E9zoVTEiN/ZiowkxDqqtBuhKZKfHpRBHgYY6rPr+2yaLjWF8FX31XpwZsrBV9ybQ7vnukmIrUt5FYEp66Tgf3yJ+DDva33l5z+0T3FzQWUrUK4sI+w1YnLbSHxG79HX3WFXIjHV8OvHn+4zH2wm/1d/GNiJy7oqdMBF+n9mOfF57YQNOS5D6qr3HY8tQIdGaQLpbneUnQYTxESz+nzQ9sgl5In4NSYv0geYyV1ZuCHee9PpgjRMVenamXrfvznX5GpTJE8pjrqTh+I4DgrY4IQGllh3/xLNpP/XQ8CwNhhdvJxgpBbsXHk+siqHmiaqZWwxhEX+nrja4IQXErhtu0Y6706d5zhqv/xNSIkMeadcZK/s4MA76SN+4iQPANjRyEvJY39iBCcs/LGvKm/Dc6eK9GSENlksnv1VA4fWkBg3G1JQrDJhBakehEzEFAcxF0WZ56QGWBZSUo4iAkBPurDrAj3QqYRSDgQ0lP09mMaINsxCQeiv7HhfQ/b4Gvk3fz8Uyj4kLc16T7aMgLEhQDG25blenEaBIgLNOqps0M0GX20JQQ468SjLEvyvBgDAgDbnhADRraTBWf8iW2hXl4+AsSFWJlN/fnPuFyx6Iv4LssxyjwhnpDFESCP1lSLaQ2tqXxT6QLPKPynKeHzysU1oyemssBg4l21Kb8T6cgFeurjAyOdUKqQEriKfH9XS0qctnyr9omLpbUheNLoaf/FzsdvezujVwd0T3GUVohXBzNtOKhTCZV4OBAs6r/YlWUGJUo6mZXza2whPHJSuXVUClgPPfVltCGryRZxIUB4Qlyhl7gQWw8PPVcUWnc9aMW+kHvdltAXWTnyEg7iYS/Hs9D91SwCCQcxIcA8Ic3SQV91kBxIQvDkZk9Iw4QoDiQhcjeotyPNUQLsi9qRm/LU4R06iUsxIUd70oughx48FMnXfJ6xDY9tCGqETabbvGLrqUEa+xEhSbf1ZT0hafCpU90VaTEiJFZpeB6H/n97CMBEzzRByObw4Rzn5P+1p8x6SyKs8RMYaLvH1wQh5LWjtZzIMM7q76pGAL86d549FWiCEBKIn2E4rVqwry8fgbQxVzmmCInHw3ChMviwLgTgQvkeaQlThFCibyVpiOq512GcS0g8BIY39ajia8WPZmq/maj1eGmr26141MOlpk88hBUigFNUeMRfJ2vMlYTcFkKJcsTF2bHK6MNqEOCIqY4MkqBtIUr8zf5OhKfWjJbaqHgflkcgPgnvczirpLaFqEJ4wv+RdxYVGvOHhCFhaarBSAgZeNyzYKzIJGjd0wnDvGFuFhcjIVRAnlmOI4NsYf+7IAKIXZFz36k2ow1Ji+zv79Ko62k6zt8bEMDZ3K0Plx1DrlFyoRaicuNwLcR3v36KXgFiChEriZkpXyq9VAuhcnSU00Mr6OF+uB9S9fjbDALSiA/uO0XsRrpoqRZCBUkAH7DQj7zSME7eEzaEUVkyqJbSLUSJlh8Ha7HItxSFSBwqMub9luHchJB4T0q1ZFBtpbustAr0V7CB/aQ39IgKGnDCYt6WoXBdqIWoSpKJyGhth8TJaGrWHJXCyhRWQogSIr9GxpdjbZfSeeEQnb6tD58rm4StlBB6ONqOPGRwvurGnow3TYcU9cCLEl85ISRY+ioiOF/VWWKataWJwnmGtSZiaiFECaXWgl9b6KJtWY2XXPhyid5nVN0qFF4U1koICYgNfnCKB/0vxbph0jnvok8sbQ3vulUY7rz6VVzthChB1I3FH4tZti8wwAUtSKije1LYpENrhCihkhj8sgxu7j9y1fBLbxsXsdG6KVtEKHysE6IEJ13ZAfaax9hxujGlL2eyoUvLO+vumhQO2bAxQtKKqFaD5BxYHwDEJERNtIY0BureCUKUMhQm5IRIDL57wQNkqh6h0U4x2mCJe/qQhMh2l5R+1rx75wjJKkldG52+TYcMyzOG8ShVXGi2TfnQBrWnCEPA0QZcYTJm4NcIPL67gSvalL8MX277H4B3AFUGbKmPAAAAAElFTkSuQmCC",
    "base64"
  )

  let containerWidth = ""
  let contentWidth = ""

  if (typeof maxWidth !== "undefined") {
    containerWidth = `${maxWidth.value}${maxWidth.unit}`
    contentWidth = !disableIcon
      ? `${maxWidth.value - 39}${maxWidth.unit}`
      : `${maxWidth.value}${maxWidth.unit}`
  }

  return (
    <View
      style={{ ...styles.container, ...styles[type], width: containerWidth }}
    >
      {!disableIcon && type === "info" ? (
        <Image
          style={styles.image}
          src={{ format: "png", data: InfoRoundedIcon }}
        />
      ) : !disableIcon && type === "warning" ? (
        <Image
          style={styles.image}
          src={{ format: "png", data: WarningRoundedIcon }}
        />
      ) : !disableIcon && type === "error" ? (
        <Image
          style={styles.image}
          src={{ format: "png", data: ErrorRoundedIcon }}
        />
      ) : (
        <></>
      )}
      <View style={{ width: contentWidth }}>{children}</View>
    </View>
  )
}

type ExportPDFButtonProps = {
  pdf: ReturnType<typeof pdf>
  filename: string
}

export const ExportPDFButton = (props: ExportPDFButtonProps) => {
  const { pdf, filename } = props
  const [blob, setBlob] = useState<Blob | undefined>(undefined)
  const [url, setUrl] = useState<string | undefined>(undefined)

  useEffect(() => {
    if (typeof blob === "undefined") {
      setTimeout(() => {
        pdf.toBlob().then((blob) => {
          setBlob(blob)
          setUrl(URL.createObjectURL(blob))
        })
      }, 500)
    }
  }, [pdf])

  const downloadOnIE = (blob: Blob) => {
    if (window.navigator.msSaveBlob) {
      window.navigator.msSaveBlob(blob, "")
    }
  }

  return (
    <>
      {typeof blob !== "undefined" && typeof url !== "undefined" ? (
        <a
          download={filename}
          href={url}
          onClick={() => downloadOnIE(blob)}
          style={{ textDecoration: "none" }}
        >
          <Button disableElevation variant="contained" color="primary">
            Exporteer PDF
          </Button>
        </a>
      ) : (
        <CircularProgress color="primary" size={30} />
      )}
    </>
  )
}
