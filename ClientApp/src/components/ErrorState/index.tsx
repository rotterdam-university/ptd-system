import "./ErrorState.css"
import React from "react"

export const ErrorState = () => (
  <div className="error-state">
    Er is een fout opgetreden. Probeer het opnieuw.
  </div>
)
