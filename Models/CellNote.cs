using System;

namespace ptd_system.Models
{
  public class CellNote
  {
    public int CellNoteID { get; set; }
    public DateTime date { get; set; }
    public string note { get; set; }
  }
}