using System.Collections.Generic;

namespace ptd_system.Models
{
  public class Label
  {
    public int LabelID { get; set; }
    public int EducationID { get; set; }
    public string name { get; set; }
    public string color { get; set; }

    public Education Education { get; set; }
  }
}