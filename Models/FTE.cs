namespace ptd_system.Models
{
  public class FTE
  {
    public int FTEID { get; set; }
    public int TeacherID { get; set; }
    public string contract { get; set; }
    public int months_active { get; set; }
    public float fte_op1 { get; set; }
    public float fte_op2 { get; set; }
    public float fte_op3 { get; set; }
    public float fte_op4 { get; set; }
    public float hours_subtracted_from_available_hours { get; set; }
    public float organisational { get; set; }
    public float professional { get; set; }

    public Teacher Teacher { get; set; }
  }
}