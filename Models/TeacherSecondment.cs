namespace ptd_system.Models
{
  public class TeacherSecondment
  {
    public int TeacherSecondmentID { get; set; }
    public int TeacherID { get; set; }
    public float hours { get; set; }
    public string reason { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }

    public Teacher teacher { get; set; }
  }
}