namespace ptd_system.Models
{
  public class TeacherMOT
  {
    public int TeacherMOTID { get; set; }
    public int TeacherID { get; set; }
    public string type { get; set; }
    public string name { get; set; }
    public float hours { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }

    public Teacher Teacher { get; set; }
  }
}