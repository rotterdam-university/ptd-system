using System.Collections.Generic;

namespace ptd_system.Models
{
  public class Position
  {
    public int PositionID { get; set; }
    public int CurriculumID { get; set; }
    public string name { get; set; }

    public Curriculum Curriculum { get; set; }
    public List<Teacher> Teachers { get; set; }
  }
}