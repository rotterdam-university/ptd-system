using System;
using Microsoft.EntityFrameworkCore;
using ptd_system.Models;

namespace ptd_system.Models
{
  public class DatabaseContext : DbContext
  {
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) {}

    public DbSet<Institute> Institute { get; set; }
    public DbSet<Education> Education { get; set; }
    public DbSet<CurriculumKind> CurriculumKind { get; set; }
    public DbSet<Curriculum> Curriculum { get; set; }
    public DbSet<Label> Label { get; set; }
    public DbSet<StudyYear> StudyYear { get; set; }
    public DbSet<OP> OP { get; set; }
    public DbSet<Manager> Manager { get; set; }
    public DbSet<MOT> MOT { get; set; }
    public DbSet<AvailabilityKind> AvailabilityKind { get; set; }
    public DbSet<_Class> _Class { get; set; }
    public DbSet<Cell> Cell { get; set; }
    public DbSet<CellNote> CellNote { get; set; }
    public DbSet<CellLabel> CellLabel { get; set; }
    public DbSet<TeacherCells> TeacherCells { get; set; }
    public DbSet<Course> Course { get; set; }
    public DbSet<CourseOwner> CourseOwner { get; set; }
    public DbSet<CourseTeams> CourseTeams { get; set; }
    public DbSet<CourseWorkmethods> CourseWorkmethods { get; set; }
    public DbSet<Workmethod> Workmethod { get; set; }
    public DbSet<Team> Team { get; set; }
    public DbSet<Teacher> Teacher { get; set; }
    public DbSet<Supervision> Supervision { get; set; }
    public DbSet<Position> Position { get; set; }
    public DbSet<TeacherNote> TeacherNote { get; set; }
    public DbSet<TeacherAvailability> TeacherAvailability { get; set; }
    public DbSet<TeacherSecondment> TeacherSecondment { get; set; }
    public DbSet<FTE> FTE { get; set; }
    public DbSet<DI> DI { get; set; }
    public DbSet<TeacherMOT> TeacherMOT { get; set; }
    public DbSet<TeacherLeave> TeacherLeave { get; set; }
    public DbSet<TeacherPreference> TeacherPreference { get; set; }
    public DbSet<TeacherTeams> TeacherTeams { get; set; }

    protected override void OnModelCreating(ModelBuilder mb)
    {
      // Institute
      mb.Entity<Institute>()
        .HasKey(e => e.InstituteID);
      // Education
      mb.Entity<Education>()
        .HasKey(e => e.EducationID);

      // CurriculumKind
      mb.Entity<CurriculumKind>()
        .HasKey(e => e.CurriculumKindID);

      // Curriculum
      mb.Entity<Curriculum>()
        .HasKey(e => e.CurriculumID);

      // Label
      mb.Entity<Label>()
        .HasKey(e => e.LabelID);

      // Study year
      mb.Entity<StudyYear>()
        .HasKey(e => e.StudyYearID);

      // OP
      mb.Entity<OP>()
        .HasKey(e => e.OPID);

      // ------------------------------------------------

      // Manager
      mb.Entity<Manager>()
        .HasKey(e => new { e.ManagerID, e.EducationID });

      // ------------------------------------------------

      // MOT
      mb.Entity<MOT>()
        .HasKey(e => e.MOTID);

      // ------------------------------------------------

      // AvailabilityKind
      mb.Entity<AvailabilityKind>()
        .HasKey(e => e.AvailabilityKindID);

      // ------------------------------------------------

      // Class
      mb.Entity<_Class>()
        .HasKey(e => e.ClassID);

      // ------------------------------------------------

      // Cell
      mb.Entity<Cell>()
        .HasKey(e => e.CellID);

      // CellNote
      mb.Entity<CellNote>()
        .HasKey(e => e.CellNoteID);

      // CellLabel
      mb.Entity<CellLabel>()
        .HasKey(e => e.CellLabelID);

      // TeacherCells
      mb.Entity<TeacherCells>()
        .HasKey(e => new { e.CellID, e.TeacherID });

      // ------------------------------------------------

      // Course
      mb.Entity<Course>()
        .HasKey(e => e.CourseID);

      // CourseOwner
      mb.Entity<CourseOwner>()
        .HasKey(e => new { e.TeacherID, e.CourseID });

      // CourseTeams
      mb.Entity<CourseTeams>()
        .HasKey(e => new { e.CourseID, e.TeamID });

      mb.Entity<Workmethod>()
        .HasKey(e => e.WorkmethodID);

      mb.Entity<CourseWorkmethods>()
        .HasKey(e => new { e.CourseID, e.WorkmethodID });

      // ------------------------------------------------

      // Team
      mb.Entity<Team>()
        .HasKey(e => e.TeamID);

      // ------------------------------------------------

      // Teacher
      mb.Entity<Teacher>()
        .HasKey(e => e.TeacherID);

      // Supervision
      mb.Entity<Supervision>()
        .HasKey(e => e.SupervisionID);

      // Position
      mb.Entity<Position>()
        .HasKey(e => e.PositionID);

      // TeacherNote
      mb.Entity<TeacherNote>()
        .HasKey(e => e.TeacherNoteID);

      // TeacherAvailability
      mb.Entity<TeacherAvailability>()
        .HasKey(e => e.TeacherAvailabilityID);

      // TeacherSecondment
      mb.Entity<TeacherSecondment>()
        .HasKey(e => e.TeacherSecondmentID);

      // FTE
      mb.Entity<FTE>()
        .HasKey(e => e.FTEID);

      // DI
      mb.Entity<DI>()
        .HasKey(e => e.DIID);

      // TeacherMOT
      mb.Entity<TeacherMOT>()
        .HasKey(e => e.TeacherMOTID);

      // TeacherLeave
      mb.Entity<TeacherLeave>()
        .HasKey(e => e.TeacherLeaveID);

      // TeacherPreference
      mb.Entity<TeacherPreference>()
        .HasKey(e => e.TeacherPreferenceID);

      // TeacherTeams
      mb.Entity<TeacherTeams>()
        .HasKey(e => new { e.TeacherID, e.TeamID });
    }
  }
}