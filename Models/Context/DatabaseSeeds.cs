using System;
using System.Linq;
using System.Collections.Generic;

namespace ptd_system.Models
{
  public static class DatabaseSeeds
  {
    public static void Initialize(DatabaseContext context)
    {
      Dictionary<int, Institute> _Institute = new Dictionary<int, Institute>();
      Dictionary<int, Education> _Education = new Dictionary<int, Education>();
      Dictionary<int, CurriculumKind> _CurriculumKind = new Dictionary<int, CurriculumKind>();
      Dictionary<int, Curriculum> _Curriculum = new Dictionary<int, Curriculum>();
      Dictionary<int, Label> _Label = new Dictionary<int, Label>();
      Dictionary<int, StudyYear> _StudyYear = new Dictionary<int, StudyYear>();
      Dictionary<int, OP> _OP = new Dictionary<int, OP>();

      Dictionary<int, Manager> _Manager = new Dictionary<int, Manager>();

      Dictionary<int, MOT> _MOT = new Dictionary<int, MOT>();

      Dictionary<int, AvailabilityKind> _AvailabilityKind = new Dictionary<int, AvailabilityKind>();

      Dictionary<int, _Class> __Class = new Dictionary<int, _Class>();

      Dictionary<int, Cell> _Cell = new Dictionary<int, Cell>();
      Dictionary<int, CellNote> _CellNote = new Dictionary<int, CellNote>();
      Dictionary<int, CellLabel> _CellLabel = new Dictionary<int, CellLabel>();
      Dictionary<int, TeacherCells> _TeacherCells = new Dictionary<int, TeacherCells>();

      Dictionary<int, Course> _Course = new Dictionary<int, Course>();
      Dictionary<int, CourseOwner> _CourseOwner = new Dictionary<int, CourseOwner>();
      Dictionary<int, CourseTeams> _CourseTeams = new Dictionary<int, CourseTeams>();
      Dictionary<int, Workmethod> _Workmethod = new Dictionary<int, Workmethod>();
      Dictionary<int, CourseWorkmethods> _CourseWorkmethods = new Dictionary<int, CourseWorkmethods>();

      Dictionary<int, Team> _Team = new Dictionary<int, Team>();

      Dictionary<int, Teacher> _Teacher = new Dictionary<int, Teacher>();
      Dictionary<int, Supervision> _Supervision = new Dictionary<int, Supervision>();
      Dictionary<int, Position> _Position = new Dictionary<int, Position>();
      Dictionary<int, TeacherNote> _TeacherNote = new Dictionary<int, TeacherNote>();
      Dictionary<int, TeacherAvailability> _TeacherAvailability = new Dictionary<int, TeacherAvailability>();
      Dictionary<int, FTE> _FTE = new Dictionary<int, FTE>();
      Dictionary<int, DI> _DI = new Dictionary<int, DI>();
      Dictionary<int, TeacherMOT> _TeacherMOT = new Dictionary<int, TeacherMOT>();
      Dictionary<int, TeacherLeave> _TeacherLeave = new Dictionary<int, TeacherLeave>();
      Dictionary<int, TeacherPreference> _TeacherPreference = new Dictionary<int, TeacherPreference>();
      Dictionary<int, TeacherTeams> _TeacherTeams = new Dictionary<int, TeacherTeams>();

      // Ensure database has been created
      context.Database.EnsureCreated();

      // Create data and commit to database
      Dictionary<int, Institute> Institute = SeedInstitute();
      Dictionary<int, Education> Education = SeedEducation();
      Dictionary<int, CurriculumKind> CurriculumKind = SeedCurriculumKind();
      Dictionary<int, AvailabilityKind> AvailabilityKind = SeedAvailabilityKind();

      Dictionary<int, Curriculum> Curriculum = SeedCurriculum();
      Dictionary<int, Label> Label = SeedLabel();
      Dictionary<int, StudyYear> StudyYear = SeedStudyYear();
      Dictionary<int, OP> OP = SeedOP();

      Dictionary<int, Manager> Manager = SeedManager();

      Dictionary<int, MOT> MOT = SeedMOT();


      Dictionary<int, _Class> _Class = Seed_Class();

      Dictionary<int, Team> Team = SeedTeam();

      Dictionary<int, Course> Course = SeedCourse();
      Dictionary<int, CourseTeams> CourseTeams = SeedCourseTeams();
      Dictionary<int, Workmethod> Workmethod = SeedWorkmethod();
      Dictionary<int, CourseWorkmethods> CourseWorkmethods = SeedCourseWorkmethods();


      Dictionary<int, Position> Position = SeedPosition();
      Dictionary<int, Teacher> Teacher = SeedTeacher();
      Dictionary<int, Supervision> Supervision = SeedSupervision();
      Dictionary<int, TeacherNote> TeacherNote = SeedTeacherNote();
      Dictionary<int, TeacherAvailability> TeacherAvailability = SeedTeacherAvailability();
      Dictionary<int, FTE> FTE = SeedFTE();
      Dictionary<int, DI> DI = SeedDI();
      Dictionary<int, TeacherMOT> TeacherMOT = SeedTeacherMOT();
      Dictionary<int, TeacherLeave> TeacherLeave = SeedTeacherLeave();
      Dictionary<int, TeacherPreference> TeacherPreference = SeedTeacherPreference();
      Dictionary<int, TeacherTeams> TeacherTeams = SeedTeacherTeams();

      Dictionary<int, CourseOwner> CourseOwner = SeedCourseOwner();

      Dictionary<int, CellNote> CellNote = SeedCellNote();
      Dictionary<int, CellLabel> CellLabel = SeedCellLabel();
      Dictionary<int, Cell> Cell = SeedCell();
      Dictionary<int, TeacherCells> TeacherCells = SeedTeacherCells();

      Dictionary<int, Institute> SeedInstitute()
      {
        if (!context.Institute.Any())
        {
          _Institute.Add(1, new Institute { name = "CMI" });
          _Institute.Add(2, new Institute { name = "RAC" });

          foreach (var item in _Institute)
          {
              context.Institute.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Institute;
      }

      Dictionary<int, Education> SeedEducation()
      {
        if (!context.Education.Any())
        {
          _Education.Add(1, new Education { InstituteID = _Institute[1].InstituteID, name = "INF", default_curr_start_date = new DateTime(2018, 09, 01), default_curr_end_date = new DateTime(2019, 07, 15) });
          _Education.Add(2, new Education { InstituteID = _Institute[1].InstituteID, name = "TI", default_curr_start_date = new DateTime(2018, 09, 01), default_curr_end_date = new DateTime(2019, 07, 15) });
          _Education.Add(3, new Education { InstituteID = _Institute[1].InstituteID, name = "CMD", default_curr_start_date = new DateTime(2018, 09, 01), default_curr_end_date = new DateTime(2019, 07, 15) });
          _Education.Add(4, new Education { InstituteID = _Institute[1].InstituteID, name = "CMT", default_curr_start_date = new DateTime(2018, 09, 01), default_curr_end_date = new DateTime(2019, 07, 15) });
          _Education.Add(5, new Education { InstituteID = _Institute[2].InstituteID, name = "ICT", default_curr_start_date = new DateTime(2018, 09, 01), default_curr_end_date = new DateTime(2019, 07, 15) });
          _Education.Add(6, new Education { InstituteID = _Institute[2].InstituteID, name = "CMC", default_curr_start_date = new DateTime(2018, 09, 01), default_curr_end_date = new DateTime(2019, 07, 15) });
          _Education.Add(7, new Education { InstituteID = _Institute[2].InstituteID, name = "MGT", default_curr_start_date = new DateTime(2018, 09, 01), default_curr_end_date = new DateTime(2019, 07, 15) });

          foreach (var item in _Education)
          {
              context.Education.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Education;
      }

      Dictionary<int, CurriculumKind> SeedCurriculumKind()
      {
        if (!context.CurriculumKind.Any())
        {
          _CurriculumKind.Add(1, new CurriculumKind { EducationID = _Education[1].EducationID, name = "Voltijd" });
          _CurriculumKind.Add(2, new CurriculumKind { EducationID = _Education[1].EducationID, name = "Deeltijd" });

          foreach (var item in _CurriculumKind)
          {
              context.CurriculumKind.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _CurriculumKind;
      }

      Dictionary<int, AvailabilityKind> SeedAvailabilityKind()
      {
        if (!context.AvailabilityKind.Any())
        {
          _AvailabilityKind.Add(1, new AvailabilityKind { EducationID = _Education[1].EducationID, block_duration = 45F, block_amount = 15 });
          _AvailabilityKind.Add(2, new AvailabilityKind { EducationID = _Education[1].EducationID, block_duration = 20F, block_amount = 52 });

          foreach (var item in _AvailabilityKind)
          {
              context.AvailabilityKind.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _AvailabilityKind;
      }

      Dictionary<int, Curriculum> SeedCurriculum()
      {
        if (!context.Curriculum.Any())
        {
          _Curriculum.Add(1, new Curriculum { EducationID = _Education[1].EducationID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, name = "18/19", start_date = new DateTime(2018, 09, 01), end_date = new DateTime(2019, 07, 15), visibility_for_teachers = false, default_fte = 1, default_fte_hours = 1659F, default_organisational = 0F, default_professional = 0F, default_di = 0.006f, course_formula_w = 4F, course_formula_ct = 50F, course_formula_opslag = 1.2F, course_formula_n = 30F, course_formula_cor = 20F, course_formula_r = 0F });

          foreach (var item in _Curriculum)
          {
              context.Curriculum.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Curriculum;
      }

      Dictionary<int, StudyYear> SeedStudyYear()
      {
        if (!context.StudyYear.Any())
        {
          _StudyYear.Add(1, new StudyYear { CurriculumID = _Curriculum[1].CurriculumID, year = 1 });
          _StudyYear.Add(2, new StudyYear { CurriculumID = _Curriculum[1].CurriculumID, year = 2 });
          _StudyYear.Add(3, new StudyYear { CurriculumID = _Curriculum[1].CurriculumID, year = 3 });
          _StudyYear.Add(4, new StudyYear { CurriculumID = _Curriculum[1].CurriculumID, year = 4 });

          foreach (var item in _StudyYear)
          {
              context.StudyYear.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _StudyYear;
      }

      Dictionary<int, OP> SeedOP()
      {
        if (!context.OP.Any())
        {
          _OP.Add(1, new OP { StudyYearID = _StudyYear[1].StudyYearID, period = 1 });
          _OP.Add(2, new OP { StudyYearID = _StudyYear[1].StudyYearID, period = 2 });
          _OP.Add(3, new OP { StudyYearID = _StudyYear[1].StudyYearID, period = 3 });
          _OP.Add(4, new OP { StudyYearID = _StudyYear[1].StudyYearID, period = 4 });

          _OP.Add(5, new OP { StudyYearID = _StudyYear[2].StudyYearID, period = 1 });
          _OP.Add(6, new OP { StudyYearID = _StudyYear[2].StudyYearID, period = 2 });
          _OP.Add(7, new OP { StudyYearID = _StudyYear[2].StudyYearID, period = 3 });
          _OP.Add(8, new OP { StudyYearID = _StudyYear[2].StudyYearID, period = 4 });

          _OP.Add(9, new OP { StudyYearID = _StudyYear[3].StudyYearID, period = 1 });
          _OP.Add(10, new OP { StudyYearID = _StudyYear[3].StudyYearID, period = 2 });
          _OP.Add(11, new OP { StudyYearID = _StudyYear[3].StudyYearID, period = 3 });
          _OP.Add(12, new OP { StudyYearID = _StudyYear[3].StudyYearID, period = 4 });

          _OP.Add(13, new OP { StudyYearID = _StudyYear[4].StudyYearID, period = 1 });
          _OP.Add(14, new OP { StudyYearID = _StudyYear[4].StudyYearID, period = 2 });
          _OP.Add(15, new OP { StudyYearID = _StudyYear[4].StudyYearID, period = 3 });
          _OP.Add(16, new OP { StudyYearID = _StudyYear[4].StudyYearID, period = 4 });

          foreach (var item in _OP)
          {
              context.OP.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _OP;
      }

      Dictionary<int, Label> SeedLabel()
      {
        if (!context.Label.Any())
        {
          _Label.Add(1, new Label { EducationID = _Education[1].EducationID, name = "Herzien", color = "#ffe082" });
          _Label.Add(2, new Label { EducationID = _Education[1].EducationID, name = "Confirmatie", color = "#81d4fa" });
          _Label.Add(3, new Label { EducationID = _Education[1].EducationID, name = "Niet mogelijk", color = "#ef9a9a" });

          foreach (var item in _Label)
          {
              context.Label.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Label;
      }

      Dictionary<int, Manager> SeedManager()
      {
        if (!context.Manager.Any())
        {
          _Manager.Add(1, new Manager { EducationID = _Education[1].EducationID, code = "DEV", email = "DEV@hr.nl" });

          foreach (var item in _Manager)
          {
              context.Manager.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Manager;
      }

      Dictionary<int, MOT> SeedMOT()
      {
        if (!context.MOT.Any())
        {
          _MOT.Add(1, new MOT { CurriculumID = _Curriculum[1].CurriculumID, type = "Organisatorische", name = "Teamvergaderingen", hours = 45F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });
          _MOT.Add(2, new MOT { CurriculumID = _Curriculum[1].CurriculumID, type = "Organisatorische", name = "Cursusbeheer", hours = 20F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });
          _MOT.Add(3, new MOT { CurriculumID = _Curriculum[1].CurriculumID, type = "Organisatorische", name = "Leerlijncoördinator", hours = 68F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });
          _MOT.Add(4, new MOT { CurriculumID = _Curriculum[1].CurriculumID, type = "Organisatorische", name = "Toetscommissie", hours = 123F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });
          _MOT.Add(5, new MOT { CurriculumID = _Curriculum[1].CurriculumID, type = "Organisatorische", name = "Opleidingscommissie", hours = 62F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });
          _MOT.Add(6, new MOT { CurriculumID = _Curriculum[1].CurriculumID, type = "Organisatorische", name = "Examencommissie", hours = 12F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });
          _MOT.Add(7, new MOT { CurriculumID = _Curriculum[1].CurriculumID, type = "Organisatorische", name = "Curriculumcommissie", hours = 12F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });
          _MOT.Add(8, new MOT { CurriculumID = _Curriculum[1].CurriculumID, type = "Organisatorische", name = "IMR", hours = 57F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });
          _MOT.Add(9, new MOT { CurriculumID = _Curriculum[1].CurriculumID, type = "Organisatorische", name = "CMR", hours = 46F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });

          foreach (var item in _MOT)
          {
              context.MOT.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _MOT;
      }

      Dictionary<int, _Class> Seed_Class()
      {
        if (!context._Class.Any())
        {
          __Class.Add(1, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF1A", class_size_op1 = 30, class_size_op2 = 30, class_size_op3 = 30, class_size_op4 = 30 });
          __Class.Add(2, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF1B", class_size_op1 = 29, class_size_op2 = 29, class_size_op3 = 29, class_size_op4 = 29 });
          __Class.Add(3, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF1C", class_size_op1 = 28, class_size_op2 = 28, class_size_op3 = 28, class_size_op4 = 28 });
          __Class.Add(4, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF1D", class_size_op1 = 30, class_size_op2 = 30, class_size_op3 = 30, class_size_op4 = 30 });
          __Class.Add(5, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF1E", class_size_op1 = 27, class_size_op2 = 27, class_size_op3 = 27, class_size_op4 = 27 });
          __Class.Add(6, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF1F", class_size_op1 = 30, class_size_op2 = 30, class_size_op3 = 30, class_size_op4 = 30 });
          __Class.Add(7, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF1G", class_size_op1 = 25, class_size_op2 = 25, class_size_op3 = 25, class_size_op4 = 25 });
          __Class.Add(8, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF1H", class_size_op1 = 28, class_size_op2 = 28, class_size_op3 = 28, class_size_op4 = 26 });
          __Class.Add(9, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF1I", class_size_op1 = 30, class_size_op2 = 30, class_size_op3 = 30, class_size_op4 = 30 });
          __Class.Add(10, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF1J", class_size_op1 = 31, class_size_op2 = 31, class_size_op3 = 31, class_size_op4 = 31 });

          __Class.Add(11, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF2A", class_size_op1 = 30, class_size_op2 = 30, class_size_op3 = 30, class_size_op4 = 30 });
          __Class.Add(12, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF2B", class_size_op1 = 23, class_size_op2 = 23, class_size_op3 = 23, class_size_op4 = 23 });
          __Class.Add(13, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF2C", class_size_op1 = 26, class_size_op2 = 26, class_size_op3 = 26, class_size_op4 = 26 });
          __Class.Add(14, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF2D", class_size_op1 = 28, class_size_op2 = 28, class_size_op3 = 28, class_size_op4 = 28 });
          __Class.Add(15, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF2E", class_size_op1 = 28, class_size_op2 = 28, class_size_op3 = 28, class_size_op4 = 28 });
          __Class.Add(16, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF2F", class_size_op1 = 30, class_size_op2 = 30, class_size_op3 = 30, class_size_op4 = 30 });
          __Class.Add(17, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF2G", class_size_op1 = 32, class_size_op2 = 32, class_size_op3 = 32, class_size_op4 = 32 });

          __Class.Add(18, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF3A", class_size_op1 = 28, class_size_op2 = 28, class_size_op3 = 28, class_size_op4 = 28 });
          __Class.Add(19, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF3B", class_size_op1 = 24, class_size_op2 = 24, class_size_op3 = 24, class_size_op4 = 24 });
          __Class.Add(20, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF3C", class_size_op1 = 26, class_size_op2 = 26, class_size_op3 = 26, class_size_op4 = 26 });
          __Class.Add(21, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF3D", class_size_op1 = 26, class_size_op2 = 26, class_size_op3 = 26, class_size_op4 = 26 });
          __Class.Add(22, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF3E", class_size_op1 = 23, class_size_op2 = 23, class_size_op3 = 23, class_size_op4 = 23 });
          __Class.Add(23, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF3F", class_size_op1 = 25, class_size_op2 = 25, class_size_op3 = 25, class_size_op4 = 25 });

          __Class.Add(24, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF4A", class_size_op1 = 24, class_size_op2 = 24, class_size_op3 = 24, class_size_op4 = 24 });
          __Class.Add(25, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF4B", class_size_op1 = 19, class_size_op2 = 19, class_size_op3 = 19, class_size_op4 = 19 });
          __Class.Add(26, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF4C", class_size_op1 = 29, class_size_op2 = 29, class_size_op3 = 29, class_size_op4 = 29 });
          __Class.Add(27, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF4D", class_size_op1 = 27, class_size_op2 = 27, class_size_op3 = 27, class_size_op4 = 27 });
          __Class.Add(28, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF4E", class_size_op1 = 24, class_size_op2 = 24, class_size_op3 = 24, class_size_op4 = 24 });
          __Class.Add(29, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "INF4F", class_size_op1 = 27, class_size_op2 = 27, class_size_op3 = 27, class_size_op4 = 27 });

          __Class.Add(30, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "DINF1", class_size_op1 = 20, class_size_op2 = 20, class_size_op3 = 20, class_size_op4 = 20 });
          __Class.Add(31, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "DINF2", class_size_op1 = 18, class_size_op2 = 18, class_size_op3 = 18, class_size_op4 = 18 });
          __Class.Add(32, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "DINF3", class_size_op1 = 19, class_size_op2 = 19, class_size_op3 = 19, class_size_op4 = 19 });
          __Class.Add(33, new _Class { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, name = "DINF4", class_size_op1 = 17, class_size_op2 = 17, class_size_op3 = 17, class_size_op4 = 17 });

          foreach (var item in __Class)
          {
              context._Class.Add(item.Value);
          }
          context.SaveChanges();
        }

        return __Class;
      }

      Dictionary<int, Team> SeedTeam()
      {
        if (!context.Team.Any())
        {
          _Team.Add(1, new Team { CurriculumID = _Curriculum[1].CurriculumID, name = "Development" });
          _Team.Add(2, new Team { CurriculumID = _Curriculum[1].CurriculumID, name = "Analyse" });
          _Team.Add(3, new Team { CurriculumID = _Curriculum[1].CurriculumID, name = "Skills" });
          _Team.Add(4, new Team { CurriculumID = _Curriculum[1].CurriculumID, name = "SLC" });
          _Team.Add(5, new Team { CurriculumID = _Curriculum[1].CurriculumID, name = "Project" });

          foreach (var item in _Team)
          {
              context.Team.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Team;
      }

      Dictionary<int, Position> SeedPosition()
      {
        if (!context.Position.Any())
        {
          _Position.Add(1, new Position { CurriculumID = _Curriculum[1].CurriculumID, name = "Lector" });
          _Position.Add(2, new Position { CurriculumID = _Curriculum[1].CurriculumID, name = "Hoofddocent" });
          _Position.Add(3, new Position { CurriculumID = _Curriculum[1].CurriculumID, name = "Hogeschooldocent" });
          _Position.Add(4, new Position { CurriculumID = _Curriculum[1].CurriculumID, name = "Kerndocent" });
          _Position.Add(5, new Position { CurriculumID = _Curriculum[1].CurriculumID, name = "Docent" });
          _Position.Add(6, new Position { CurriculumID = _Curriculum[1].CurriculumID, name = "Trainee" });
          _Position.Add(7, new Position { CurriculumID = _Curriculum[1].CurriculumID, name = "Peercoach" });
          _Position.Add(8, new Position { CurriculumID = _Curriculum[1].CurriculumID, name = "Instructeur" });

          foreach (var item in _Position)
          {
              context.Position.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Position;
      }

      Dictionary<int, Teacher> SeedTeacher()
      {
        if (!context.Teacher.Any())
        {
          _Teacher.Add(1, new Teacher { CurriculumID = _Curriculum[1].CurriculumID, PositionID = _Position[3].PositionID, personnel_code = "PARIS", first_name = "Stelian", last_name = "Paraschiv", placeholder = false });

          foreach (var item in _Teacher)
          {
              context.Teacher.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Teacher;
      }

      Dictionary<int, Supervision> SeedSupervision()
      {
        if (!context.Supervision.Any())
        {
          _Supervision.Add(1, new Supervision { CurriculumID = _Curriculum[1].CurriculumID, TeacherID = _Teacher[1].TeacherID, supervision_kind = "Stagiair", start_op = 1, end_op = 2 });
          _Supervision.Add(2, new Supervision { CurriculumID = _Curriculum[1].CurriculumID, TeacherID = _Teacher[1].TeacherID, supervision_kind = "Stagiair", start_op = 1, end_op = 2 });
          _Supervision.Add(3, new Supervision { CurriculumID = _Curriculum[1].CurriculumID, TeacherID = _Teacher[1].TeacherID, supervision_kind = "Stagiair", start_op = 3, end_op = 4 });

          _Supervision.Add(4, new Supervision { CurriculumID = _Curriculum[1].CurriculumID, TeacherID = _Teacher[1].TeacherID, supervision_kind = "Afstudeerders", start_op = 1, end_op = 2 });
          _Supervision.Add(5, new Supervision { CurriculumID = _Curriculum[1].CurriculumID, TeacherID = _Teacher[1].TeacherID, supervision_kind = "Afstudeerders", start_op = 3, end_op = 4 });
          _Supervision.Add(6, new Supervision { CurriculumID = _Curriculum[1].CurriculumID, TeacherID = _Teacher[1].TeacherID, supervision_kind = "Afstudeerders", start_op = 3, end_op = 4 });
          _Supervision.Add(7, new Supervision { CurriculumID = _Curriculum[1].CurriculumID, TeacherID = _Teacher[1].TeacherID, supervision_kind = "Afstudeerders", start_op = 4, end_op = 0 });

          foreach (var item in _Supervision)
          {
              context.Supervision.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Supervision;
      }

      Dictionary<int, TeacherNote> SeedTeacherNote()
      {
        if (!context.TeacherNote.Any())
        {
          _TeacherNote.Add(1, new TeacherNote { TeacherID = _Teacher[1].TeacherID, note = "Dit is een notitie", date = new DateTime(2018, 12, 01, 14, 27, 05) });

          foreach (var item in _TeacherNote)
          {
              context.TeacherNote.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _TeacherNote;
      }

      Dictionary<int, TeacherAvailability> SeedTeacherAvailability()
      {
        if (!context.TeacherAvailability.Any())
        {
          _TeacherAvailability.Add(1, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 1, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(2, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 2, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(3, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 3, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(4, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 4, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(5, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 5, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(6, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 6, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(7, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 7, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(8, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 8, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(9, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 9, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(10, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 10, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(11, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 11, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(12, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 12, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(13, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 13, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(14, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 14, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(15, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 0, block = 15, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });

          _TeacherAvailability.Add(16, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 1, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(17, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 2, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(18, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 3, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(19, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 4, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(20, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 5, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(21, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 6, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(22, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 7, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(23, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 8, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(24, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 9, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(25, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 10, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(26, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 11, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(27, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 12, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(28, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 13, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(29, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 14, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(30, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 1, block = 15, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });

          _TeacherAvailability.Add(31, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 1, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(32, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 2, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(33, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 3, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(34, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 4, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(35, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 5, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(36, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 6, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(37, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 7, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(38, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 8, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(39, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 9, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(40, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 10, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(41, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 11, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(42, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 12, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(43, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 13, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(44, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 14, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(45, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 2, block = 15, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });

          _TeacherAvailability.Add(46, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 1, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(47, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 2, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(48, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 3, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(49, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 4, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(50, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 5, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(51, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 6, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(52, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 7, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(53, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 8, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(54, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 9, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(55, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 10, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(56, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 11, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = false });
          _TeacherAvailability.Add(57, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 12, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(58, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 13, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(59, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 14, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(60, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 3, block = 15, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });

          _TeacherAvailability.Add(61, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 1, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(62, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 2, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(63, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 3, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(64, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 4, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(65, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 5, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(66, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 6, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(67, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 7, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(68, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 8, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(69, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 9, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(70, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 10, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(71, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 11, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(72, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 12, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(73, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 13, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(74, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 14, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          _TeacherAvailability.Add(75, new TeacherAvailability { TeacherID = _Teacher[1].TeacherID, AvailabilityKindID = _AvailabilityKind[1].AvailabilityKindID, day = 4, block = 15, last_updated = new DateTime(2018, 07, 15, 00, 00, 00), definitive = true, available = true });
          foreach (var item in _TeacherAvailability)
          {
              context.TeacherAvailability.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _TeacherAvailability;
      }

      Dictionary<int, FTE> SeedFTE()
      {
        if (!context.FTE.Any())
        {
          _FTE.Add(1, new FTE { TeacherID = _Teacher[1].TeacherID, contract = "Vast", months_active = 12, fte_op1 = _Curriculum[1].default_fte, fte_op2 =  _Curriculum[1].default_fte, fte_op3 =  _Curriculum[1].default_fte, fte_op4 =  _Curriculum[1].default_fte, hours_subtracted_from_available_hours = 0, organisational = 0, professional = 0 });

          foreach (var item in _FTE)
          {
              context.FTE.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _FTE;
      }

      Dictionary<int, DI> SeedDI()
      {
        if (!context.DI.Any())
        {
          _DI.Add(1, new DI { TeacherID = _Teacher[1].TeacherID, di_op1 = 0.006F, di_op2 = 0.006F, di_op3 = 0.006F, di_op4 = 0.006F });

          foreach (var item in _DI)
          {
              context.DI.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _DI;
      }

      Dictionary<int, TeacherMOT> SeedTeacherMOT()
      {
        if (!context.TeacherMOT.Any())
        {
          _TeacherMOT.Add(1, new TeacherMOT { TeacherID = _Teacher[1].TeacherID, type = "Organisatorische", name = "Cursusbeheer", hours = 20F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });
          _TeacherMOT.Add(2, new TeacherMOT { TeacherID = _Teacher[1].TeacherID, type = "Organisatorische", name = "Curriculumcommissie", hours = 56F, active_op1 = false, active_op2 = true, active_op3 = false, active_op4 = true });

          foreach (var item in _TeacherMOT)
          {
              context.TeacherMOT.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _TeacherMOT;
      }

      Dictionary<int, TeacherLeave> SeedTeacherLeave()
      {
        if (!context.TeacherLeave.Any())
        {
          _TeacherLeave.Add(1, new TeacherLeave { TeacherID = _Teacher[1].TeacherID, name = "Zwangerschapsverlof", hours_override = 56F, start_date = new DateTime(2019, 02, 20), end_date = new DateTime(2019, 05, 10) });
          _TeacherLeave.Add(2, new TeacherLeave { TeacherID = _Teacher[1].TeacherID, name = "Ouderschapsverlof", hours_override = 0F, start_date = new DateTime(2018, 11, 02), end_date = new DateTime(2018, 11, 22) });

          foreach (var item in _TeacherLeave)
          {
              context.TeacherLeave.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _TeacherLeave;
      }

      Dictionary<int, TeacherTeams> SeedTeacherTeams()
      {
        if (!context.TeacherTeams.Any())
        {
          _TeacherTeams.Add(1, new TeacherTeams { TeacherID = _Teacher[1].TeacherID, TeamID = _Team[5].TeamID, is_team_leader = true });

          foreach (var item in _TeacherTeams)
          {
              context.TeacherTeams.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _TeacherTeams;
      }

      Dictionary<int, Course> SeedCourse()
      {
        if (!context.Course.Any())
        {
          _Course.Add(1, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[1].OPID, code = "INFANL04-1", name = "Analyse 1 - Foundation of Modeling 1", ects = 4, raise_factor = 2f, meetings = 18, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 36F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(2, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[1].OPID, code = "INFDEV02-1", name = "Development 1 - Basic imperative computational concepts", ects = 4, raise_factor = 1.6f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 36F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(3, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[1].OPID, code = "INFDEV02-1", name = "Development 1 - Basic imperative computational concepts", ects = 4, raise_factor = 1.6f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 36F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(4, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[1].OPID, code = "INFSKL05-1", name = "Skills 1 - Project and target group oriented working", ects = 0, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 18F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(5, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[1].OPID, code = "INFPRJ01A", name = "Project A", ects = 0, raise_factor = 2f, meetings = 14, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 36F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(6, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[1].OPID, code = "INFSLC04-1", name = "Studieloopbaancoaching jaar 1", ects = 0, raise_factor = 1.6f, meetings = 15, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 18F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(7, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[1].OPID, code = "INFINE00", name = "0-meting Nederlands", ects = 0, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 0F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(8, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[1].OPID, code = "INFIEN00", name = "0-meting Engels", ects = 0, raise_factor = 1.6f, meetings = 32, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 0F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(9, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[1].OPID, code = "INFDNE00", name = "Ondersteuningsmodule Nederlands", ects = 0, raise_factor = 1.6f, meetings = 1, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 12F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(10, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[1].OPID, code = "INFDEN02", name = "Ondersteuningsmodule Engels", ects = 0, raise_factor = 2f, meetings = 1, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 0F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });

          _Course.Add(11, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[2].OPID, code = "INFANL03-2", name = "Analyse 2 - Foundation of Modeling 2", ects = 4, raise_factor = 1.6f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(12, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[2].OPID, code = "INFDEV02-2", name = "Development 2 - Functions, lambda's, recursion, HOF's", ects = 4, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(13, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[2].OPID, code = "INFDEV02-2", name = "Development 2 - Functions, lambda's, recursion, HOF's", ects = 4, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(14, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[2].OPID, code = "INFSKL05-1", name = "Skills 1 - Project and target group oriented working", ects = 0, raise_factor = 1.6f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(15, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[2].OPID, code = "INFPRJ01A", name = "Project A", ects = 8, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(16, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[2].OPID, code = "INFSLC04-1", name = "Studieloopbaancoaching jaar 1", ects = 0, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          
          _Course.Add(17, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[3].OPID, code = "INFANL03-3", name = "Analyse 3 - Object Oriented Modeling", ects = 4, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(18, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[3].OPID, code = "INFDEV02-3", name = "Development 3 - Static typing, Object orientation", ects = 4, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(19, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[3].OPID, code = "INFDEV02-3", name = "Development 3 - Static typing, Object orientation", ects = 4, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(20, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[3].OPID, code = "INFSKL05-1", name = "Skills 1 - Project and target group oriented working", ects = 0, raise_factor = 1.6f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(21, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[3].OPID, code = "INFPRJ01B", name = "Project B", ects = 0, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(22, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[3].OPID, code = "INFSLC04-1", name = "Studieloopbaancoaching jaar 1", ects = 0, raise_factor = 1.6f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });

          _Course.Add(23, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[4].OPID, code = "INFANL03-4", name = "Analyse 4 - Operating Systems", ects = 4, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(24, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[4].OPID, code = "INFDEV02-4", name = "Development 4 - Polymorphism, Generics, basic design", ects = 4, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(25, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[4].OPID, code = "INFDEV02-4", name = "Development 4 - Polymorphism, Generics, basic design", ects = 4, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(26, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[4].OPID, code = "INFSKL05-1", name = "Skills 1 - Project and target group oriented working", ects = 4, raise_factor = 1.6f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(27, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[4].OPID, code = "INFPRJ01B", name = "Project B", ects = 8, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });
          _Course.Add(28, new Course { CurriculumID = _Curriculum[1].CurriculumID, CurriculumKindID = _CurriculumKind[1].CurriculumKindID, OPID = _OP[4].OPID, code = "INFSLC04-1", name = "Studieloopbaancoaching jaar 1", ects = 2, raise_factor = 2f, meetings = 10, meeting_duration = 20F, teachers_simultaniously_teaching = 1, contact_student = 56F, correction_time = 10F, travel_time = 0F, teached_on_monday = false, teached_on_tuesday = true, teached_on_wednesday = true, teached_on_thursday = false, teached_on_friday = true });

          foreach (var item in _Course)
          {
              context.Course.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Course;
      }

      Dictionary<int, CourseOwner> SeedCourseOwner()
      {
        if (!context.CourseOwner.Any())
        {
          _CourseOwner.Add(1, new CourseOwner { TeacherID = _Teacher[1].TeacherID, CourseID = 4 });
          _CourseOwner.Add(2, new CourseOwner { TeacherID = _Teacher[1].TeacherID, CourseID = 11 });
          _CourseOwner.Add(3, new CourseOwner { TeacherID = _Teacher[1].TeacherID, CourseID = 16 });
          _CourseOwner.Add(4, new CourseOwner { TeacherID = _Teacher[1].TeacherID, CourseID = 20 });

          foreach (var item in _CourseOwner)
          {
              context.CourseOwner.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _CourseOwner;
      }

      Dictionary<int, CourseTeams> SeedCourseTeams()
      {
        if (!context.CourseTeams.Any())
        {
          _CourseTeams.Add(1, new CourseTeams { CourseID = _Course[1].CourseID, TeamID = _Team[2].TeamID });
          _CourseTeams.Add(2, new CourseTeams { CourseID = _Course[2].CourseID, TeamID = _Team[1].TeamID });
          _CourseTeams.Add(3, new CourseTeams { CourseID = _Course[3].CourseID, TeamID = _Team[1].TeamID });
          _CourseTeams.Add(4, new CourseTeams { CourseID = _Course[4].CourseID, TeamID = _Team[3].TeamID });
          _CourseTeams.Add(5, new CourseTeams { CourseID = _Course[5].CourseID, TeamID = _Team[5].TeamID });
          _CourseTeams.Add(6, new CourseTeams { CourseID = _Course[6].CourseID, TeamID = _Team[4].TeamID });

          _CourseTeams.Add(7, new CourseTeams { CourseID = _Course[11].CourseID, TeamID = _Team[2].TeamID });
          _CourseTeams.Add(8, new CourseTeams { CourseID = _Course[12].CourseID, TeamID = _Team[1].TeamID });
          _CourseTeams.Add(9, new CourseTeams { CourseID = _Course[13].CourseID, TeamID = _Team[1].TeamID });
          _CourseTeams.Add(10, new CourseTeams { CourseID = _Course[14].CourseID, TeamID = _Team[3].TeamID });
          _CourseTeams.Add(11, new CourseTeams { CourseID = _Course[15].CourseID, TeamID = _Team[5].TeamID });
          _CourseTeams.Add(12, new CourseTeams { CourseID = _Course[16].CourseID, TeamID =_Team[4].TeamID });

          _CourseTeams.Add(13, new CourseTeams { CourseID = _Course[17].CourseID, TeamID =_Team[2].TeamID });
          _CourseTeams.Add(14, new CourseTeams { CourseID = _Course[18].CourseID, TeamID =_Team[1].TeamID });
          _CourseTeams.Add(15, new CourseTeams { CourseID = _Course[19].CourseID, TeamID =_Team[1].TeamID });
          _CourseTeams.Add(16, new CourseTeams { CourseID = _Course[20].CourseID, TeamID =_Team[3].TeamID });
          _CourseTeams.Add(17, new CourseTeams { CourseID = _Course[21].CourseID, TeamID =_Team[5].TeamID });
          _CourseTeams.Add(18, new CourseTeams { CourseID = _Course[22].CourseID, TeamID =_Team[4].TeamID });

          _CourseTeams.Add(19, new CourseTeams { CourseID = _Course[23].CourseID, TeamID =_Team[2].TeamID });
          _CourseTeams.Add(20, new CourseTeams { CourseID = _Course[24].CourseID, TeamID =_Team[1].TeamID });
          _CourseTeams.Add(21, new CourseTeams { CourseID = _Course[25].CourseID, TeamID =_Team[1].TeamID });
          _CourseTeams.Add(22, new CourseTeams { CourseID = _Course[26].CourseID, TeamID =_Team[3].TeamID });
          _CourseTeams.Add(23, new CourseTeams { CourseID = _Course[27].CourseID, TeamID =_Team[5].TeamID });
          _CourseTeams.Add(24, new CourseTeams { CourseID = _Course[28].CourseID, TeamID =_Team[4].TeamID });

          foreach (var item in _CourseTeams)
          {
              context.CourseTeams.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _CourseTeams;
      }

      Dictionary<int, Workmethod> SeedWorkmethod()
      {
        if (!context.Workmethod.Any())
        {
          _Workmethod.Add(1, new Workmethod { CurriculumID = _Curriculum[1].CurriculumID, name = "Standaard" });
          _Workmethod.Add(2, new Workmethod { CurriculumID = _Curriculum[1].CurriculumID, name = "Theorie" });
          _Workmethod.Add(3, new Workmethod { CurriculumID = _Curriculum[1].CurriculumID, name = "Praktijk" });

          foreach (var item in _Workmethod)
          {
              context.Workmethod.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Workmethod;
      }

      Dictionary<int, CourseWorkmethods> SeedCourseWorkmethods()
      {
        if (!context.CourseWorkmethods.Any())
        {
          _CourseWorkmethods.Add(1, new CourseWorkmethods { CourseID = _Course[1].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(2, new CourseWorkmethods { CourseID = _Course[2].CourseID, WorkmethodID = _Workmethod[2].WorkmethodID });
          _CourseWorkmethods.Add(3, new CourseWorkmethods { CourseID = _Course[3].CourseID, WorkmethodID = _Workmethod[3].WorkmethodID });
          _CourseWorkmethods.Add(4, new CourseWorkmethods { CourseID = _Course[4].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(5, new CourseWorkmethods { CourseID = _Course[5].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(6, new CourseWorkmethods { CourseID = _Course[6].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(7, new CourseWorkmethods { CourseID = _Course[7].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(8, new CourseWorkmethods { CourseID = _Course[8].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(9, new CourseWorkmethods { CourseID = _Course[9].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(10, new CourseWorkmethods { CourseID = _Course[10].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          
          _CourseWorkmethods.Add(11, new CourseWorkmethods { CourseID = _Course[11].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(12, new CourseWorkmethods { CourseID = _Course[12].CourseID, WorkmethodID = _Workmethod[2].WorkmethodID });
          _CourseWorkmethods.Add(13, new CourseWorkmethods { CourseID = _Course[13].CourseID, WorkmethodID = _Workmethod[3].WorkmethodID });
          _CourseWorkmethods.Add(14, new CourseWorkmethods { CourseID = _Course[14].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(15, new CourseWorkmethods { CourseID = _Course[15].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(16, new CourseWorkmethods { CourseID = _Course[16].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          
          _CourseWorkmethods.Add(17, new CourseWorkmethods { CourseID = _Course[17].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(18, new CourseWorkmethods { CourseID = _Course[18].CourseID, WorkmethodID = _Workmethod[2].WorkmethodID });
          _CourseWorkmethods.Add(19, new CourseWorkmethods { CourseID = _Course[19].CourseID, WorkmethodID = _Workmethod[3].WorkmethodID });
          _CourseWorkmethods.Add(20, new CourseWorkmethods { CourseID = _Course[20].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(21, new CourseWorkmethods { CourseID = _Course[21].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(22, new CourseWorkmethods { CourseID = _Course[22].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          
          _CourseWorkmethods.Add(23, new CourseWorkmethods { CourseID = _Course[23].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(24, new CourseWorkmethods { CourseID = _Course[24].CourseID, WorkmethodID = _Workmethod[2].WorkmethodID });
          _CourseWorkmethods.Add(25, new CourseWorkmethods { CourseID = _Course[25].CourseID, WorkmethodID = _Workmethod[3].WorkmethodID });
          _CourseWorkmethods.Add(26, new CourseWorkmethods { CourseID = _Course[26].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(27, new CourseWorkmethods { CourseID = _Course[27].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });
          _CourseWorkmethods.Add(28, new CourseWorkmethods { CourseID = _Course[28].CourseID, WorkmethodID = _Workmethod[1].WorkmethodID });

          foreach (var item in _CourseWorkmethods)
          {
              context.CourseWorkmethods.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _CourseWorkmethods;
      }

      Dictionary<int, TeacherPreference> SeedTeacherPreference()
      {
        if (!context.TeacherPreference.Any())
        {
          _TeacherPreference.Add(1, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[1].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(2, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[2].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(3, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[3].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = true, preferred = false });
          _TeacherPreference.Add(4, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[4].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = false, preferred = true });
          _TeacherPreference.Add(5, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[5].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = false, preferred = true });
          _TeacherPreference.Add(6, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[6].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(7, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[7].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(8, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[8].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(9, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[9].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });

          _TeacherPreference.Add(10, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[10].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(11, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[11].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(12, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[12].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = true, preferred = false });
          _TeacherPreference.Add(13, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[13].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = false, preferred = true });
          _TeacherPreference.Add(14, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[14].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = false, preferred = true });

          _TeacherPreference.Add(15, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[15].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(16, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[16].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(17, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[17].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = true, preferred = false });
          _TeacherPreference.Add(18, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[18].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = false, preferred = true });
          _TeacherPreference.Add(19, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[19].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = false, preferred = true });

          _TeacherPreference.Add(20, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[20].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(21, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[21].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = true, no_preference = false, preferred = false });
          _TeacherPreference.Add(22, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[22].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = true, preferred = false });
          _TeacherPreference.Add(23, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[23].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = false, preferred = true });
          _TeacherPreference.Add(24, new TeacherPreference { TeacherID = _Teacher[1].TeacherID, CourseID = _Course[24].CourseID, last_updated = new DateTime(2018, 07, 10, 00 ,00, 00), definitive = true, not_preferred = false, no_preference = false, preferred = true });

          foreach (var item in _TeacherPreference)
          {
              context.TeacherPreference.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _TeacherPreference;
      }

      Dictionary<int, CellNote> SeedCellNote()
      {
        if (!context.CellNote.Any())
        {
          _CellNote.Add(1, new CellNote { date = new DateTime(2020, 12, 20), note = "Dit is een notitie voor een cell." });

          foreach (var item in _CellNote)
          {
              context.CellNote.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _CellNote;
      }


      Dictionary<int, CellLabel> SeedCellLabel()
      {
        if (!context.CellLabel.Any())
        {
          _CellLabel.Add(1, new CellLabel { name = "Herzien", color = "#ffe082" });

          foreach (var item in _CellLabel)
          {
              context.CellLabel.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _CellLabel;
      }

      Dictionary<int, Cell> SeedCell()
      {
        if (!context.Cell.Any())
        {
          _Cell.Add(1, new Cell { OPID = _OP[1].OPID, CourseID = _Course[1].CourseID, ClassID = _Class[1].ClassID, x_position = 1, y_position = 1 });

          foreach (var item in _Cell)
          {
              context.Cell.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _Cell;
      }

      Dictionary<int, TeacherCells> SeedTeacherCells()
      {
        if (!context.TeacherCells.Any())
        {
          _TeacherCells.Add(1, new TeacherCells { TeacherID = _Teacher[1].TeacherID, CellID = _Cell[1].CellID });

          foreach (var item in _TeacherCells)
          {
              context.TeacherCells.Add(item.Value);
          }
          context.SaveChanges();
        }

        return _TeacherCells;
      }
    }
  }
}