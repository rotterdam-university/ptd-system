using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace ptd_system.Models {
  static public class DatabaseDefaultData
  {
    static public void SeedData(DatabaseContext _context)
    {
      _context.Database.EnsureCreated();

      Dictionary<int, Institute> institutes = new Dictionary<int, Institute>();
      Dictionary<int, Education> educations = new Dictionary<int, Education>();
      Dictionary<int, CurriculumKind> curriculumKinds = new Dictionary<int, CurriculumKind>();
      Dictionary<int, Manager> managers = new Dictionary<int, Manager>();
      Dictionary<int, AvailabilityKind> availabilityKinds = new Dictionary<int, AvailabilityKind>();

      institutes.Add(1, new Institute { name = "CMI" });
      institutes.ToList().ForEach(institute => {
        if (_context.Institute.FirstOrDefault(i => i.name.Equals(institute.Value.name)) == null) {
          _context.Institute.Add(institute.Value);
        }
      });
      _context.SaveChanges();

      educations.Add(1, new Education { InstituteID = institutes[1].InstituteID, name = "INF", default_curr_start_date = new DateTime(2021, 09, 01), default_curr_end_date = new DateTime(2022, 07, 15) });
      educations.ToList().ForEach(education => {
        if (_context.Education.FirstOrDefault(e => e.name.Equals(education.Value.name)) == null) {
          _context.Education.Add(education.Value);
        }
      });
      _context.SaveChanges();

      curriculumKinds.Add(1, new CurriculumKind { EducationID = educations[1].EducationID, name = "Voltijd" });
      curriculumKinds.Add(2, new CurriculumKind { EducationID = educations[1].EducationID, name = "Deeltijd" });
      curriculumKinds.ToList().ForEach(curriculumKind => {
        if (_context.CurriculumKind.FirstOrDefault(ck => ck.name.Equals(curriculumKind.Value.name)) == null) {
          _context.CurriculumKind.Add(curriculumKind.Value);
        }
      });
      _context.SaveChanges();

      managers.Add(1, new Manager { EducationID = educations[1].EducationID, code = "HOFSA", email = "a.hofstede@hr.nl" });
      managers.ToList().ForEach(manager => {
        if (_context.Manager.FirstOrDefault(m => m.code.Equals(manager.Value.code)) == null) {
          _context.Manager.Add(manager.Value);
        }
      });
      _context.SaveChanges();

      availabilityKinds.Add(1, new AvailabilityKind { EducationID = educations[1].EducationID, block_duration = 45, block_amount = 15 });
      availabilityKinds.Add(2, new AvailabilityKind { EducationID = educations[1].EducationID, block_duration = 20, block_amount = 52 });
      availabilityKinds.ToList().ForEach(AvailabilityKind => {
        if (_context.AvailabilityKind.FirstOrDefault(ak => ak.block_amount.Equals(AvailabilityKind.Value.block_amount) && ak.block_duration.Equals(AvailabilityKind.Value.block_duration)) == null) {
          _context.AvailabilityKind.Add(AvailabilityKind.Value);
        }
      });
      _context.SaveChanges();
    }
  }  
}
