using System;

namespace ptd_system.Models
{
  public class TeacherNote
  {
    public int TeacherNoteID { get; set; }
    public int TeacherID { get; set; }
    public string note { get; set; }
    public DateTime date { get; set; }
    
    public Teacher Teacher { get; set; }
  }
}