using System;
using System.Collections.Generic;

namespace ptd_system.Models
{
  public class Curriculum
  {
    public int CurriculumID { get; set; }
    public int EducationID { get; set; }
    public int AvailabilityKindID { get; set; }
    public string name { get; set; }
    public DateTime start_date { get; set; }
    public DateTime end_date { get; set; }
    public bool visibility_for_teachers { get; set; }
    public float default_fte { get; set; }
    public float default_fte_hours { get; set; }
    public float default_di { get; set; }
    public float default_organisational { get; set; }
    public float default_professional { get; set; }
    public float course_formula_w { get; set; }
    public float course_formula_ct { get; set; }
    public float course_formula_opslag { get; set; }
    public float course_formula_n { get; set; }
    public float course_formula_cor { get; set; }
    public float course_formula_r { get; set; }

    public Education Education { get; set; }
    public List<MOT> MOTs { get; set; }
    public List<StudyYear> StudyYears { get; set; }
    public AvailabilityKind AvailabilityKind { get; set; }
    public List<_Class> _Classes { get; set; }
    public List<Position> Positions { get; set; }
    public List<Supervision> Supervisions { get; set; }
    public List<Teacher> Teachers { get; set; }
    public List<Team> Teams { get; set; }
    public List<Course> Courses { get; set; }
  }
}