using System;

namespace ptd_system.Models
{
  public class TeacherPreference
  {
    public int TeacherPreferenceID { get; set; }
    public int TeacherID { get; set; }
    public int CourseID { get; set; }
    public DateTime last_updated { get; set; }
    public bool definitive { get; set; }
    public bool not_preferred { get; set; }
    public bool no_preference { get; set; }
    public bool preferred { get; set; }

    public Teacher Teacher { get; set; }
    public Course Course { get; set; }
  }
}