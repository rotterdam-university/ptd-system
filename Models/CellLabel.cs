using System.Collections.Generic;

namespace ptd_system.Models
{
  public class CellLabel
  {
    public int CellLabelID { get; set; }
    public string name { get; set; }
    public string color { get; set; }

    public List<Cell> Cells { get; set; }
  }
}