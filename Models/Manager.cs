namespace ptd_system.Models
{
  public class Manager
  {
    public int ManagerID { get; set; }
    public int EducationID { get; set; }
    public string code { get; set; }
    public string email { get; set; }

    public Education Education { get; set; }
  }
}