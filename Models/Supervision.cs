namespace ptd_system.Models
{
  public class Supervision
  {
    public int SupervisionID { get; set; }
    public int CurriculumID { get; set; }
    public int TeacherID { get; set; }
    public string supervision_kind { get; set; }
    public int start_op { get; set; }
    public int end_op { get; set; }

    public Curriculum Curriculum { get; set; }
    public Teacher Teacher { get; set; }
  }
}