using System.Collections.Generic;

namespace ptd_system.Models
{
  public class AvailabilityKind
  {
    public int AvailabilityKindID { get; set; }
    public int EducationID { get; set; }
    public float block_duration { get; set; }
    public int block_amount { get; set; }

    public Education Education { get; set; }
    public List<Curriculum> Curricula { get; set; }
    public List<TeacherAvailability> TeacherAvailabilities { get; set; }
  }
}