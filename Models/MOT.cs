namespace ptd_system.Models
{
  public class MOT
  {
    public int MOTID { get; set; }
    public int CurriculumID { get; set; }
    public string type { get; set; }
    public string name { get; set; }
    public float hours { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }

    public Curriculum Curriculum { get; set; }
  }
}