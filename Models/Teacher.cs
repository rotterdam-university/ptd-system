using System.Collections.Generic;

namespace ptd_system.Models
{
  public class Teacher
  {
    public int TeacherID { get; set; }
    public int CurriculumID { get; set; }
    public int PositionID { get; set; }
    public string personnel_code { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public bool placeholder { get; set; }

    public Curriculum Curriculum { get; set; }
    public Position Position { get; set; }
    public List<Supervision> Supervisions { get; set; }
    public List<TeacherNote> Notes { get; set; }
    public List<TeacherAvailability> Availabilities { get; set; }
    public FTE FTE { get; set; }
    public DI DI { get; set; }
    public List<TeacherLeave> Leaves { get; set; }
    public List<TeacherMOT> MOTs { get; set; }
    public List<TeacherCells> Cells { get; set; }
    public List<TeacherPreference> Preferences { get; set; }
    public List<CourseOwner> CourseOwnerOf { get; set; }
    public List<TeacherTeams> Teams { get; set; }
    public List<TeacherSecondment> TeacherSecondments { get; set; }
  }
}