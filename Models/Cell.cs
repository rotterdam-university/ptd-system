using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ptd_system.Models
{
  public class Cell
  {
    public int CellID { get; set; }
    public int OPID { get; set; }
    #nullable enable
    public int? CourseID { get; set; }
    public int? ClassID { get; set; }
    public int? CellNoteID { get; set; }
    public int? CellLabelID { get; set; }
    #nullable disable
    public int x_position { get; set; }
    public int y_position { get; set; }

    public OP OP { get; set; }
    #nullable enable
    public Course? Course { get; set; }
    public _Class? Class { get; set; }
    public CellNote? CellNote { get; set; }
    public CellLabel? Label { get; set; }
    #nullable disable
    public List<TeacherCells> Teachers { get; set; }
  }
}