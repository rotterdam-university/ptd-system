using System.Collections.Generic;

namespace ptd_system.Models
{
  public class CurriculumKind
  {
    public int CurriculumKindID { get; set; }
    public int EducationID { get; set; }
    public string name { get; set; }

    public Education Education { get; set; }
    public List<_Class> Classes { get; set; }
    public List<Course> Courses { get; set; }
  }
}