using System;

namespace ptd_system.Models
{
  public class TeacherLeave
  {
    public int TeacherLeaveID { get; set; }
    public int TeacherID { get; set; }
    public string name { get; set; }
    public float hours_override { get; set; }
    public DateTime start_date { get; set; }
    public DateTime end_date { get; set; }

    public Teacher Teacher { get; set; }
  }
}