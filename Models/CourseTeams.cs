namespace ptd_system.Models
{
  public class CourseTeams
  {
    public int CourseTeamsID { get; set; }
    public int CourseID { get; set; }
    public int TeamID { get; set; }

    public Course Course { get; set; }
    public Team Team { get; set; }
  }
}