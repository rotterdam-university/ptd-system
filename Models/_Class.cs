using System.Collections.Generic;

namespace ptd_system.Models
{
  public class _Class
  {
    public int ClassID { get; set; }
    public int CurriculumID { get; set; }
    public int CurriculumKindID { get; set; }
    public string name { get; set; }
    public int class_size_op1 { get; set; }
    public int class_size_op2 { get; set; }
    public int class_size_op3 { get; set; }
    public int class_size_op4 { get; set; }

    public Curriculum Curriculum { get; set; }
    public CurriculumKind CurriculumKind { get; set; }
    public List<Cell> Cells { get; set; }
  }
}