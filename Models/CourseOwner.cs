namespace ptd_system.Models
{
  public class CourseOwner
  {
    public int CourseOwnerID { get; set; }
    public int TeacherID { get; set; }
    public int CourseID { get; set; }

    public Teacher Teacher { get; set; }
    public Course Course { get; set; }
  }
}