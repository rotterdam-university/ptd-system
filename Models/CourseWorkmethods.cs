namespace ptd_system.Models
{
  public class CourseWorkmethods
  {
    public int CourseID { get; set; }
    public int WorkmethodID { get; set; }

    public Course Course { get; set; }
    public Workmethod Workmethod { get; set; }
  }
}