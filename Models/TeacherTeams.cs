namespace ptd_system.Models
{
  public class TeacherTeams
  {
    public int TeacherTeamsID { get; set; }
    public int TeacherID { get; set; }
    public int TeamID { get; set; }
    public bool is_team_leader { get; set; }

    public Teacher Teacher { get; set; }
    public Team Team { get; set; }
  }
}