using System.Collections.Generic;

namespace ptd_system.Models
{
  public class Course
  {
    public int CourseID { get; set; }
    public int CurriculumID { get; set; }
    public int CurriculumKindID { get; set; }
    public int OPID { get; set; }
    public string code { get; set; }
    public string name { get; set; }
    public int ects { get; set; }
    public float raise_factor { get; set; }
    public int meetings { get; set; }
    public float meeting_duration { get; set; }
    public int teachers_simultaniously_teaching { get; set; }
    public float contact_student { get; set; }
    public float correction_time { get; set; }
    public float travel_time { get; set; }
    public bool teached_on_monday { get; set; }
    public bool teached_on_tuesday { get; set; }
    public bool teached_on_wednesday { get; set; }
    public bool teached_on_thursday { get; set; }
    public bool teached_on_friday { get; set; }

    public Curriculum Curriculum { get; set; }
    public CurriculumKind CurriculumKind { get; set; }
    public OP OP { get; set; }
    public List<Cell> Cells { get; set; }
    public List<TeacherPreference> TeacherPreferences { get; set; }
    public List<CourseOwner> CourseOwners { get; set; }
    public List<CourseTeams> Teams { get; set; }
    public List<CourseWorkmethods> Workmethods { get; set; }
  }
}