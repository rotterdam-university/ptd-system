using System.Collections.Generic;

namespace ptd_system.Models
{
  public class Institute
  {
    public int InstituteID { get; set; }
    public string name { get; set; }

    public List<Education> Educations { get; set; }
  }
}