namespace ptd_system.Models
{
  public class StudyYear
  {
    public int StudyYearID { get; set; }
    public int CurriculumID { get; set; }
    public int year { get; set; }

    public Curriculum Curriculum { get; set; }
  }
}