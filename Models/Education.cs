using System;
using System.Collections.Generic;

namespace ptd_system.Models
{
  public class Education
  {
    public int EducationID { get; set; }
    public int InstituteID { get; set; }
    public string name { get; set; }
    public DateTime default_curr_start_date { get; set; }
    public DateTime default_curr_end_date { get; set; }

    public Institute Institute { get; set; }
    public List<CurriculumKind> CurriculumKinds { get; set; }
    public List<Manager> Managers { get; set; }
    public List<Curriculum> Curriculums { get; set; }
    public List<AvailabilityKind> AvailabilityKinds { get; set; }
    public List<Label> Labels { get; set; }
  }
}