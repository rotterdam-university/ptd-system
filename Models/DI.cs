namespace ptd_system.Models
{
  public class DI
  {
    public int DIID { get; set; }
    public int TeacherID { get; set; }
    public float di_op1 { get; set; }
    public float di_op2 { get; set; }
    public float di_op3 { get; set; }
    public float di_op4 { get; set; }

    public Teacher Teacher { get; set; }
  }
}