using System.Collections.Generic;

namespace ptd_system.Models
{
  public class Team
  {
    public int TeamID { get; set; }
    public int CurriculumID { get; set; }
    public string name { get; set; }

    public Curriculum Curriculum { get; set; }
    public Course Course { get; set; }
    public List<TeacherTeams> Teachers { get; set; }
  }
}