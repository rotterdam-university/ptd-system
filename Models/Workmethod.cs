using System.Collections.Generic;

namespace ptd_system.Models
{
  public class Workmethod
  {
    public int WorkmethodID { get; set; }
    public int CurriculumID { get; set; }
    public string name { get; set; }

    public Curriculum Curriculum { get; set; }
    public List<CourseWorkmethods> Courses { get; set; }
  }
}