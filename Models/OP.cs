using System.Collections.Generic;

namespace ptd_system.Models
{
  public class OP
  {
    public int OPID { get; set; }
    public int StudyYearID { get; set; }
    public int period { get; set; }

    public StudyYear StudyYear { get; set; }
    public List<Cell> Cells { get; set; }
    public List<Course> Courses { get; set; }
  }
}