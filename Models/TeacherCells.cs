namespace ptd_system.Models
{
  public class TeacherCells
  {
    public int TeacherCellsID { get; set; }
    public int TeacherID { get; set; }
    public int CellID { get; set; }
    public float teacher_workload_override { get; set; }

    public Teacher Teacher { get; set; }
    public Cell Cell { get; set; }
  }
} 