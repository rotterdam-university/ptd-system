using System;

namespace ptd_system.Models
{
  public class TeacherAvailability
  {
    public int TeacherAvailabilityID { get; set; }
    public int TeacherID { get; set; }
    public int AvailabilityKindID { get; set; }
    public int day { get; set; }
    public int block { get; set; }
    public DateTime last_updated { get; set; }
    public bool definitive { get; set; }
    public bool available { get; set; }

    public Teacher Teacher { get; set; }
    public AvailabilityKind AvailabilityKind { get; set; }
  }
}