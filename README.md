# ptd-system

## Setup

1. Make sure that you have the .NET Core version installed that's specified in `global.json`. Currently it's `3.1.201`.
2. Create a `appsettings.Development.json` file and paste the json below. Change the parameters to suite your setup.

```json
{
  "Logging": {
    "LogLevel": {
      "Default": "Warning"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "ptd-system": "Host=<HOST>;Database=<DATABASE_NAME>;Username=<USERNAME>;Password=<PASSWORD>"
  },
  "Sentry": {
    "Dsn": "<DNS>",
    "IncludeRequestPayload": true,
    "SendDefaultPii": true,
    "MinimumBreadcrumbLevel": "Debug",
    "MinimumEventLevel": "Warning",
    "AttachStackTrace": true,
    "Debug": true,
    "DiagnosticsLevel": "Error"
  }
}
```

3. Run `dotnet restore` to restore all the dependencies.
4. Run `dotnet ef database update` to create the database and tables.

## Seeding the database

The seeds are located in the following file: `Models/Context/DatabaseSeeds.cs`.

To seed your database, you have to drop your database, update and run the seed command. Here's a shorthand:

```zsh
dotnet ef database drop -f && dotnet ef database update && dotnet run seed
```

## Running the project


Run `dotnet run` to run the project locally. Make sure the environnment is set to development. Mac and Linux users can export the env variable with this: `export ASPNETCORE_ENVIRONMENT=Development`

⚠️ If you're using Chrome, make sure to use `https://localhost:5001`. The SSO won't work otherwise.

## Building an image

Run the following command to build a docker image locally:

```zsh
docker build -t registry.gitlab.com/rotterdam-university/ptd-system:<TAG> -f Dockerfile .
```

where `<TAG>` is the current version.

## Pushing to container registry

You can push the image to the container registry on Gitlab with the following command:

```zsh
docker push registry.gitlab.com/rotterdam-university/ptd-system:<TAG>
```

where `<TAG>` is version that you'd like to push.

## Running a docker image

When your desired version of the application is pushed to the container registry, you can run it with this command:

```zsh
docker run -it --rm -p 5000:80 --hostname ptd.ruas.nl -d --name ptd registry.gitlab.com/rotterdam-university/ptd-system:<TAG>
```

where `<TAG>` is version that you'd like to run.

### Inspecting logs of the container

With `docker logs -tf --tail 100 ptd` you output the last 100 lines of logs and follow and output any new logs.