using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using HR.Common.Security.Client.Cwips;
using HR.Common.Security.Client.Cwips.Extensions;
using Microsoft.AspNetCore.Authentication.Cookies;
using ptd_system.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using Sentry;
 
namespace ptd_system
{
  public class Startup
  {
    private readonly IWebHostEnvironment _currentEnvironment;

    public Startup(IConfiguration configuration, IWebHostEnvironment env)
    {
      Configuration = configuration;
      _currentEnvironment = env;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {

      services.AddDbContext<DatabaseContext>(options =>
          options.UseNpgsql(Configuration.GetConnectionString("ptd-system")));

      services.AddControllersWithViews();

      services.AddAuthentication(CwipsAuthenticationDefaults.AuthenticationScheme).AddCwips(options =>
      {
        // De onderstaande configuratie opties worden doorgaans per HR-applicatie, naar behoefte ingesteld. 
        // Er zijn er meer, maar die hebben in de meeste gevallen al de juiste standaardwaarden.
        options.SingleSignOn = SingleSignOnMode.Preferred;
        options.AllowAuthenticationMethod = AuthenticationMethods.NetworkCredentialsForTokenless | AuthenticationMethods.UserNameAndPasswordWithToken;

        // Als je zoals in dit voorbeeld geen default sign-in scheme hebt gedefineerd, zul je dat hier alsnog moeten doen. 
        // Dit moet iets anders zijn dan 'CWIPS' omdat een remote authentication handler niet is toegestaan, wat de CWIPS module wel is!
        // 'Cookies' is hier een prima alternatief: 
        options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;

      }).AddCookie(options =>
      {
        // Geef hier je Cookie-authenticatie opties op...
        options.LoginPath = new PathString("/login");
        options.Events.OnRedirectToAccessDenied =
        options.Events.OnRedirectToLogin = c =>
        {
          c.Response.StatusCode = 401;
          return Task.FromResult<object>(null);
        };
      });

      // CWIPS, the SSO from RUAS, only provides three roles: dop, obp and stud
      // Both teachers and managers are dop, so that's why all controller endpoints
      // are configured with policy "teacher". Inside the endpoint, there is an
      // additional check for the manager - teacher rights.

      services.AddAuthorization(options => 
      {
        options.AddPolicy("teacher", policy =>
          policy.RequireClaim(System.Security.Claims.ClaimTypes.Role, "dop", "obp", "stud")
        );
      });

      // if (_currentEnvironment.IsDevelopment())
      // {
      //   services.AddAuthorization(options => 
      //   {
      //     options.AddPolicy("teacher", policy =>
      //       policy.RequireClaim(System.Security.Claims.ClaimTypes.Role, "dop", "obp", "stud")
      //     );
      //   });
      // } else
      // {
      //   services.AddAuthorization(options => 
      //   {
      //     options.AddPolicy("student", policy => 
      //       policy.RequireClaim(System.Security.Claims.ClaimTypes.Role, "stud")
      //     );
      //   });

      //   services.AddAuthorization(options => 
      //   {
      //     options.AddPolicy("teacher", policy =>
      //       policy.RequireClaim(System.Security.Claims.ClaimTypes.Role, "dop", "obp")
      //     );
      //   });
      // }

      // In production, the React files will be served from this directory
      services.AddSpaStaticFiles(configuration =>
      {
        configuration.RootPath = "ClientApp/build";
      });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseExceptionHandler("/Error");
        // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
        app.UseHsts();
        // app.UseHttpsRedirection();
      }

      app.UseStaticFiles();
      app.UseSpaStaticFiles();
      
      app.UseRouting();

      app.UseAuthentication();
      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllerRoute(
                  name: "default",
                  pattern: "{controller}/{action=Index}/{id?}");
      });

      app.UseSpa(spa =>
      {
        spa.Options.SourcePath = "ClientApp";

        if (env.IsDevelopment())
        {
          spa.UseReactDevelopmentServer(npmScript: "start");
        }
      });
    }
  }
}
