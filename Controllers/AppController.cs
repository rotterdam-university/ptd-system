using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using ptd_system.Helpers;
using ptd_system.Models;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace ptd_system.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class AppController : Controller
  {
    private readonly DatabaseContext _context;
    private readonly AuthenticationHelper _AuthenticationHelper;

    public AppController(DatabaseContext context)
    {
      _context = context;
      _AuthenticationHelper = new AuthenticationHelper(context);
    }

    [HttpGet("all-curricula")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult AllCurricula()
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var curricula = _context.Education
          .Include(e => e.Curriculums)
          .FirstOrDefault(e => e.name.Equals(user.education)).Curriculums.OrderByDescending(c => c.name)
          .Select(c => new {
            id = c.CurriculumID,
            name = c.name,
            kinds = _context.CurriculumKind.Where(ck => ck.Education.name.Equals(user.education)).OrderByDescending(ck => ck.name).Select(ck => ck.name).ToList()
          }).ToList();
        return Ok(curricula);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("logout")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult LogOut()
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Response.Cookies.Delete(".AspNetCore.Cookies");
        return Ok();
      } else {
        return Unauthorized();
      }
    }
  }
}