using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using ptd_system.Helpers;
using ptd_system.Models;
using System.Linq;
using System.Collections.Generic;
using ptd_system.Controllers.DTO;
using Microsoft.AspNetCore.Authorization;

namespace ptd_system.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class CourseController : Controller
  {
    private readonly DatabaseContext _context;
    private readonly AuthenticationHelper _AuthenticationHelper;

    public CourseController(DatabaseContext context)
    {
      _context = context;
      _AuthenticationHelper = new AuthenticationHelper(context);
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult AllCourses(int curriculum_1, int curriculum_2, string kind)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var courses = _context.Education
          .Include(e => e.Curriculums)
            .ThenInclude(c => c.Courses)
              .ThenInclude(c => c.OP)
          .Include(e => e.Curriculums)
            .ThenInclude(c => c.Courses)
              .ThenInclude(c => c.Workmethods)
                .ThenInclude(wm => wm.Workmethod)
          .Include(e => e.Curriculums)
            .ThenInclude(c => c.Courses)
              .ThenInclude(c => c.CurriculumKind)
          .FirstOrDefault(e => e.name.Equals(user.education)).Curriculums
          .FirstOrDefault(c => c.name.Equals($"{curriculum_1}/{curriculum_2}")).Courses
          .Where(c => c.CurriculumKind.name.Equals(kind))
          .Select(c => new { id = c.CourseID, code = c.code, workmethod = c.Workmethods.First().Workmethod.name, op = $"OP{c.OP.period}" })
          .OrderBy(c => c.code)
          .OrderBy(c => c.op)
          .ToList();

        return Ok(courses);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/{course_id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult CourseDetails(int curriculum_1, int curriculum_2, string kind, int course_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Course c = _context.Curriculum
          .Include(c => c.Courses)
            .ThenInclude(c => c.CourseOwners)
              .ThenInclude(c => c.Teacher)
          .Include(c => c.Courses)
            .ThenInclude(c => c.Teams)
              .ThenInclude(t => t.Team)
          .Include(c => c.Courses)
            .ThenInclude(c => c.OP)
              .ThenInclude(op => op.StudyYear)
          .Include(c => c.Courses)
            .ThenInclude(c => c.Workmethods)
              .ThenInclude(c => c.Workmethod)
          .Include(c => c.Courses)
            .ThenInclude(c => c.CurriculumKind)
          .FirstOrDefault(c => c.name.Equals($"{curriculum_1}/{curriculum_2}")).Courses
          .FirstOrDefault(c => c.CourseID.Equals(course_id));

        var courseOwner = c.CourseOwners.FirstOrDefault();
        var team = c.Teams.FirstOrDefault();

        var course = new {
          code = c.code,
          name = c.name,
          ects = c.ects,
          raise_factor = c.raise_factor,
          meetings = c.meetings,
          meeting_duration = c.meeting_duration,
          teachers_simultaniously_teaching = c.teachers_simultaniously_teaching,
          contact_students = c.contact_student,
          study_year = c.OP.StudyYear.year,
          op = c.OP.period,
          correction_time = c.correction_time,
          travel_time = c.travel_time,
          workmethod = new { id = c.Workmethods.First().Workmethod.WorkmethodID, name = c.Workmethods.First().Workmethod.name },
          course_owner = new { id = courseOwner == null ? -1 : courseOwner.Teacher.TeacherID, personnel_code = courseOwner == null ? "Geen cursusbeheerder" : courseOwner.Teacher.personnel_code },
          team = new { id = team == null ? -1 : team.Team.TeamID, name = team == null ? "Geen team" : team.Team.name },
          kind = c.CurriculumKind.name
        };

        return Ok(course);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/available-course-owners")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetAvailableCourseOwnersForCourse(int curriculum_1, int curriculum_2, string kind)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var availableCourseOwners = _context.Teacher
          .Include(t => t.Curriculum)
          .Include(t => t.CourseOwnerOf)
          .Where(t => t.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          .Where(t => t.CourseOwnerOf.Count <= 0)
          .Select(t => new {
            id = t.TeacherID,
            personnel_code = t.personnel_code
          }).ToList();

        return Ok(availableCourseOwners);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/available-teams")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetTeamsCourse(int curriculum_1, int curriculum_2, string kind)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var availableTeams = _context.Team
          .Include(t => t.Curriculum)
          .Include(t => t.Course)
          .Where(t => t.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          .Select(t => new {
            id = t.TeamID,
            name = t.name
          }).ToList();

        return Ok(availableTeams);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/available-workmethods")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetWorkmethodsCourse(int curriculum_1, int curriculum_2, string kind)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var availableWorkmethods = _context.Workmethod
          .Include(wm => wm.Curriculum)
          .Where(wm => wm.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          .Select(wm => new {
            id = wm.WorkmethodID,
            name = wm.name
          })
          .ToList();

        return Ok(availableWorkmethods);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/default-values")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetDefaultCourseValues(int curriculum_1, int curriculum_2, string kind)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum c = _context.Curriculum
          .FirstOrDefault(c => c.name.Equals($"{curriculum_1}/{curriculum_2}"));

        Course_DefaultValuesResponse res = new Course_DefaultValuesResponse {
          meetings = c.course_formula_w,
          contact_students = c.course_formula_ct,
          raise_factor = c.course_formula_opslag,
          correction_time = c.course_formula_cor,
          travel_time = c.course_formula_r
        };

        return Ok(res);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("update/{id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult UpdateCourse(int id, [FromBody] CourseDTOModel uc)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Course course = _context.Course
          .Include(c => c.CourseOwners)
          .Include(c => c.Workmethods)
          .Include(c => c.Teams)
          .FirstOrDefault(c => c.CourseID.Equals(id));
        if (course == null)
          return NotFound(ExceptionResponse.Create("course-not-found", $"Course with ID: {id} was not found."));

        course.code = uc.code;
        course.name = uc.name;
        course.ects = uc.ects;
        course.raise_factor = uc.raise_factor;
        course.meetings = uc.meetings;
        course.meeting_duration = uc.meeting_duration;
        course.correction_time = uc.correction_time;
        course.travel_time = uc.travel_time;
        course.teachers_simultaniously_teaching = uc.teachers_simultaniously_teaching;
        course.contact_student = uc.contact_students;
        course.OPID = _context.OP.Include(op => op.StudyYear).FirstOrDefault(op => op.period.Equals(uc.op) && op.StudyYear.year.Equals(uc.study_year) && op.StudyYear.CurriculumID.Equals(course.CurriculumID)).OPID;

        if (course.Workmethods.Count <= 0) {
          if (uc.workmethod.id != -1)
          {
            CourseWorkmethods newCourseWorkmethod = new CourseWorkmethods() {
              CourseID = course.CourseID,
              WorkmethodID = uc.workmethod.id
            };
            _context.CourseWorkmethods.Add(newCourseWorkmethod);
            _context.SaveChanges();
          }
        } else {
          if (uc.workmethod.id == -1)
          {
            CourseWorkmethods oldCourseWorkmethod = course.Workmethods.First();
            _context.CourseWorkmethods.Remove(oldCourseWorkmethod);
          } else {
            CourseWorkmethods oldCourseWorkmethod = course.Workmethods.First();
            CourseWorkmethods newCourseWorkmethod = new CourseWorkmethods() {
              CourseID = course.CourseID,
              WorkmethodID = uc.workmethod.id
            };

            _context.CourseWorkmethods.Remove(oldCourseWorkmethod);
            _context.CourseWorkmethods.Add(newCourseWorkmethod);
            _context.SaveChanges();
          }
        }
        
        if (course.CourseOwners.Count <= 0) {
          if (uc.course_owner.id != -1)
          {
            CourseOwner newCourseOwner = new CourseOwner() {
              CourseID = course.CourseID,
              TeacherID = uc.course_owner.id
            };
            _context.CourseOwner.Add(newCourseOwner);
            _context.SaveChanges();
          }
        } else {
          if (uc.course_owner.id == -1)
          {
            CourseOwner oldCourseOwner = course.CourseOwners.First();
            _context.CourseOwner.Remove(oldCourseOwner);
          } else {
            CourseOwner oldCourseOwner = course.CourseOwners.First();
            CourseOwner newCourseOwner = new CourseOwner() {
              CourseID = course.CourseID,
              TeacherID = uc.course_owner.id
            };

            _context.CourseOwner.Remove(oldCourseOwner);
            _context.CourseOwner.Add(newCourseOwner);
            _context.SaveChanges();
          }
        }

        if (uc.team.id == -1) {
          CourseTeams oldCourseTeams = course.Teams.First();
          _context.CourseTeams.Remove(oldCourseTeams);

          Team team = _context.Team.FirstOrDefault(t => t.name.Equals("Geen team"));

          if (team == null) {
            Team new_team = new Team { CurriculumID = course.CurriculumID, name = "Geen team" };
            _context.Team.Add(new_team);
            _context.SaveChanges();

            CourseTeams newCourseTeams = new CourseTeams() {
              CourseID = course.CourseID,
              TeamID = new_team.TeamID
            };
            _context.CourseTeams.Add(newCourseTeams);
            _context.SaveChanges();
          } else {
            CourseTeams newCourseTeams = new CourseTeams() {
              CourseID = course.CourseID,
              TeamID = team.TeamID
            };
            _context.CourseTeams.Add(newCourseTeams);
            _context.SaveChanges();
          }
        } else {
          CourseTeams oldCourseTeams = course.Teams.First();
          _context.CourseTeams.Remove(oldCourseTeams);
          _context.SaveChanges();

          CourseTeams newCourseTeams = new CourseTeams() {
            CourseID = course.CourseID,
            TeamID = uc.team.id
          };

          _context.CourseTeams.Add(newCourseTeams);
          _context.SaveChanges();
        }

        _context.Course.Update(course);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("create/{curriculum_1}/{curriculum_2}/{kind}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult CreateCourse(int curriculum_1, int curriculum_2, string kind, [FromBody] CourseDTOModel nc)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum curriculum = _context.Curriculum
          .Include(c => c.Education)
            .ThenInclude(c => c.CurriculumKinds)
          .FirstOrDefault(c => c.name.Equals($"{curriculum_1}/{curriculum_2}"));

        CurriculumKind curriculumKind = _context.CurriculumKind
          .FirstOrDefault(ck => ck.name.Equals(nc.kind));

        Course course = new Course()
        {
          CurriculumID = curriculum.CurriculumID,
          CurriculumKindID = curriculumKind.CurriculumKindID,
          OPID = _context.OP.Include(op => op.StudyYear).FirstOrDefault(op => op.period.Equals(nc.op) && op.StudyYear.year.Equals(nc.study_year) && op.StudyYear.Curriculum.CurriculumID.Equals(curriculum.CurriculumID)).OPID,
          code = nc.code,
          name = nc.name,
          ects = nc.ects,
          raise_factor = nc.raise_factor,
          meetings = nc.meetings,
          meeting_duration = nc.meeting_duration,
          correction_time = nc.correction_time,
          travel_time = nc.travel_time,
          teachers_simultaniously_teaching = nc.teachers_simultaniously_teaching,
          contact_student = nc.contact_students,
        };

        _context.Course.Add(course);
        _context.SaveChanges();

        Workmethod workmethod = _context.Workmethod.FirstOrDefault(wm => wm.name.Equals(nc.workmethod.name));

        CourseWorkmethods courseWorkmethod = new CourseWorkmethods()
        {
          CourseID = course.CourseID,
          WorkmethodID = workmethod.WorkmethodID
        };

        _context.CourseWorkmethods.Add(courseWorkmethod);
        _context.SaveChanges();

        if (!nc.course_owner.id.Equals(-1))
        {
          CourseOwner courseOwner = new CourseOwner()
          {
            CourseID = course.CourseID,
            TeacherID = nc.course_owner.id
          };

          _context.CourseOwner.Add(courseOwner);
          _context.SaveChanges();
        }

        if (nc.team.id.Equals(-1)) {
          Team team = _context.Team.FirstOrDefault(t => t.name.Equals("Geen team"));

          if (team == null) {
            Team new_team = new Team { CurriculumID = course.CurriculumID, name = "Geen team" };
            _context.Team.Add(new_team);
            _context.SaveChanges();

            CourseTeams newCourseTeams = new CourseTeams() {
              CourseID = course.CourseID,
              TeamID = new_team.TeamID
            };
            _context.CourseTeams.Add(newCourseTeams);
            _context.SaveChanges();
          } else {
            CourseTeams newCourseTeams = new CourseTeams() {
              CourseID = course.CourseID,
              TeamID = team.TeamID
            };
            _context.CourseTeams.Add(newCourseTeams);
            _context.SaveChanges();
          }
        } else {
          CourseTeams courseTeams = new CourseTeams()
          {
            CourseID = course.CourseID,
            TeamID = nc.team.id
          };

          _context.CourseTeams.Add(courseTeams);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("delete-multiple")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult DeleteMultipleCourses([FromBody] List<DeleteCourses> coursesToDelete)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        List<Course_DeleteMultipleCourseErrorResponse> courses_with_cells = new List<Course_DeleteMultipleCourseErrorResponse>();

        foreach (var courseToDeletee in coursesToDelete) {
          List<Course_DeleteCourseErrorResponse> cells = _context.Cell
            .Include(c => c.OP)
              .ThenInclude(op => op.StudyYear)
            .Include(c => c.Course)
            .Where(c => c.CourseID.Equals(courseToDeletee.id))
            .Select(c => new Course_DeleteCourseErrorResponse {
              year = c.OP.StudyYear.year,
              op = c.OP.period
            })
            .Distinct()
            .ToList();

          if (cells.Count > 0) {
            int id = courses_with_cells.FindIndex(x => x.code.Equals(_context.Course.FirstOrDefault(c => c.CourseID.Equals(courseToDeletee.id)).name));
            Course course = _context.Course
              .Include(c => c.OP)
                .ThenInclude(op => op.StudyYear)
              .FirstOrDefault(c => c.CourseID.Equals(courseToDeletee.id));
            
            if (id == -1) {
              courses_with_cells.Add(new Course_DeleteMultipleCourseErrorResponse{
                code = course.name,
                year_and_op = new List<Course_DeleteCourseErrorResponse>() {
                  new Course_DeleteCourseErrorResponse {
                year = course.OP.StudyYear.year,
                op = course.OP.period,
                  }
                }
              });
            } else {
              courses_with_cells[id].year_and_op.Add(new Course_DeleteCourseErrorResponse {
                year = course.OP.StudyYear.year,
                op = course.OP.period,
              });
            }
          }
        }

        if (courses_with_cells.Count > 0) {
          return BadRequest(courses_with_cells);
        } else {
          foreach (var courseToDelete in coursesToDelete)
          {
            Course course = _context.Course.FirstOrDefault(c => c.CourseID.Equals(courseToDelete.id));
            if (course == null)
              return NotFound(ExceptionResponse.Create("course-not-found", $"Course wiht ID: {courseToDelete.id} was not found"));
           
            _context.Course.Remove(course);
            _context.SaveChanges();
          }

          return Ok();
        }
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("delete/{id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult DeleteCourse(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Course course = _context.Course.FirstOrDefault(c => c.CourseID.Equals(id));
        if (course == null)
          return NotFound(ExceptionResponse.Create("course-not-found", $"Course wiht ID: {id} was not found"));

        List<Course_DeleteCourseErrorResponse> cells = _context.Cell
          .Include(c => c.OP)
            .ThenInclude(op => op.StudyYear)
          .Include(c => c.Course)
          .Where(c => c.CourseID.Equals(course.CourseID))
          .Select(c => new Course_DeleteCourseErrorResponse {
            year = c.OP.StudyYear.year,
            op = c.OP.period
          })
          .Distinct()
          .ToList();

        if (cells.Count > 0) {
          return BadRequest(cells);
        } else {
          _context.Course.Remove(course);
          _context.SaveChanges();

          return Ok();
        }

      } else {
        return Unauthorized();
      }
    }
  }
}