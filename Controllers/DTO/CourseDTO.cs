using System.Collections.Generic;

namespace ptd_system.Controllers.DTO
{
  public class CourseDTOModel
  {
    public string code { get; set; }
    public string name { get; set; }
    public int ects { get; set; }
    public float raise_factor { get; set; }
    public int meetings { get; set; }
    public float meeting_duration { get; set; }
    public int teachers_simultaniously_teaching { get; set; } 
    public float contact_students { get; set; }
    public int teached_on_day { get; set; }
    public int study_year { get; set; }
    public int op { get; set; }
    public int correction_time { get; set; }
    public int travel_time { get; set; }
    public CourseWorkmethodDTO workmethod { get; set; }
    public CourseCourseOwnerDTO course_owner { get; set; }
    public CourseTeamDTO team { get; set; }
    public string kind { get; set; }
  }

  public class CourseWorkmethodDTO
  {
    public int id { get; set; }
    public string name { get; set; }
  }

  public class CourseCourseOwnerDTO
  {
    public int id { get; set; }
    public string personnel_code { get; set; }
  }

  public class CourseTeamDTO
  {
    public int id { get; set; }
    public string name { get; set; }
  }

  public class DeleteCourses
  {
    public int id { get; set; }
  }

  public class Course_DefaultValuesResponse
  {
    public float meetings { get; set; }
    public float contact_students { get; set; }
    public float raise_factor { get; set; }
    public float correction_time { get; set; }
    public float travel_time { get; set; }
  }

  public class Course_DeleteMultipleCourseErrorResponse
  {
    public string code { get; set; }
    public List<Course_DeleteCourseErrorResponse> year_and_op { get; set; }
  }

  public class Course_DeleteCourseErrorResponse
  {
    public int year { get; set; }
    public int op { get; set; }
  }
}