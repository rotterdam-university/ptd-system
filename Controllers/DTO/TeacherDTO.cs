using System;
using System.Collections.Generic;

namespace ptd_system.Controllers.DTO
{
  public class TeacherDTO
  {
    public int id { get; set; }
    public int curriculum_id { get; set; }
    public string personnel_code { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public bool placeholder { get; set; }
    public Teacher_TeamDTO team { get; set; }
    public Teacher_PositionDTO position { get; set; }
  }

  public class NewTeacherDTO
  {
    public string personnel_code { get; set; }
    public string first_name { get; set; }
    public string last_name { get; set; }
    public bool isPlaceholder { get; set; }
    public Teacher_OrganisationDTO organisation { get; set; }
    public Teacher_TeamDTO team { get; set; }
    public Teacher_PositionDTO position { get; set; }
    public string contract { get; set; }
    public int months_active { get; set; }
    public float fte_op1 { get; set; }
    public float fte_op2 { get; set; }
    public float fte_op3 { get; set; }
    public float fte_op4 { get; set; }
    public float di_op1 { get; set; }
    public float di_op2 { get; set; }
    public float di_op3 { get; set; }
    public float di_op4 { get; set; }
    public float organisational { get; set; }
    public float professional { get; set; }
    public List<Teacher_MOTTask> mot_tasks { get; set; }
  }

  public class Teacher_MOTTask
  {
    public string name { get; set; }
    public string type { get; set; }
    public float hours { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }
  }

  public class Teacher_OrganisationDTO
  {
    public int id { get; set; }
    public string name { get; set; }
  }

  public class Teacher_TeamDTO
  {
    public int id { get; set; }
    public string name { get; set; }
  }

  public class Teacher_PositionDTO
  {
    public int id { get; set; }
    public string name { get; set; }
  }

  public class DeleteTeachers
  {
    public int id { get; set; }
  }

  public class Teacher_NotesDTO
  {
    public int id { get; set; }
    public string curriculum_name { get; set; }
    public string note { get; set; }
    public DateTime date { get; set; }
  }

  public class Teacher_NewNotesDTO
  {
    public string note { get; set; }
  }

  public class TeacherAanstellingResponse
  {
    public string contract { get; set; }
    public float default_fte { get; set; }
    public float fte_op1 { get; set; }
    public float fte_op2 { get; set; }
    public float fte_op3 { get; set; }
    public float fte_op4 { get; set; }
    public float default_di { get; set; }
    public float di_op1 { get; set; }
    public float di_op2 { get; set; }
    public float di_op3 { get; set; }
    public float di_op4 { get; set; }
    public float organisational { get; set; }
    public float professional { get; set; }
    public float hours_subtracted_from_available_hours { get; set; }
    public List<TeacherAanstellingSecondmentResponse> secondments { get; set; }

  }

  public class TeacherAanstellingRequest
  {
    public string contract { get; set; }
    public float fte_op1 { get; set; }
    public float fte_op2 { get; set; }
    public float fte_op3 { get; set; }
    public float fte_op4 { get; set; }
    public float di_op1 { get; set; }
    public float di_op2 { get; set; }
    public float di_op3 { get; set; }
    public float di_op4 { get; set; }
    public float organisational { get; set; }
    public float professional { get; set; }
    public float hours_subtracted_from_available_hours { get; set; }
    public List<TeacherAanstellingSecondmentResponse> secondments { get; set; }
  }

  public class TeacherAanstellingSecondmentResponse
  {
    public int id { get; set; }
    public float hours { get; set; }
    public string reason { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }
  }

  public class TeacherTaaktoedelingResponse
  {
    public int total_available_hours { get; set; }
    public int total_available_hours_op1 { get; set; }
    public int total_available_hours_op2 { get; set; }
    public int total_available_hours_op3 { get; set; }
    public int total_available_hours_op4 { get; set; }
    public TeacherTaaktoedelingDIResponse di { get; set; }
    public TeacherTaaktoedelingSecondmentsResponse secondments { get; set; }
    public TeacherTaaktoedelingOrganizationalTasksResponse organizational_tasks { get; set; }
    public TeacherTaaktoedelingOrganizationalTasksOtherResponse  organization_tasks_other { get; set; }
    public TeacherTaaktoedelingProfessionalizationTasksResponse professionalization_tasks { get; set; }
    public TeacherTaaktoedelingProfessionalizationTasksOtherResponse  professionalization_tasks_other { get; set; }
    public TeacherTaaktoedelingContactTimeResponse contact_time { get; set; }
    public TeacherTaaktoedelingContactTimeWithFactorResponse  contact_time_with_factor { get; set; }
    public TeacherTaaktoedelingInternsAndGraduatesSupervisionResponse supervision { get; set; }
    public double mean_raise_factor { get; set; }
    public int oddment_total { get; set; }
    public int oddment_op1 { get; set; }
    public int oddment_op2 { get; set; }
    public int oddment_op3 { get; set; }
    public int oddment_op4 { get; set; }
  }

  public class TeacherTaaktoedelingDIResponse
  {
    public int total { get; set; }
    public int op1 { get; set; }
    public int op2 { get; set; }
    public int op3 { get; set; }
    public int op4 { get; set; }
  }

  public class TeacherTaaktoedelingSecondmentsResponse
  {
    public int total { get; set; }
    public int op1 { get; set; }
    public int op2 { get; set; }
    public int op3 { get; set; }
    public int op4 { get; set; }
  }

  public class TeacherTaaktoedelingOrganizationalTasksResponse
  {
    public int total { get; set; }
    public int op1 { get; set; }
    public int op2 { get; set; }
    public int op3 { get; set; }
    public int op4 { get; set; }
  }

  public class TeacherTaaktoedelingOrganizationalTasksOtherResponse
  {
    public int total { get; set; }
    public int op1 { get; set; }
    public int op2 { get; set; }
    public int op3 { get; set; }
    public int op4 { get; set; }
  }

  public class TeacherTaaktoedelingProfessionalizationTasksResponse
  {
    public int total { get; set; }
    public int op1 { get; set; }
    public int op2 { get; set; }
    public int op3 { get; set; }
    public int op4 { get; set; }
  }

  public class TeacherTaaktoedelingProfessionalizationTasksOtherResponse
  {
    public int total { get; set; }
    public int op1 { get; set; }
    public int op2 { get; set; }
    public int op3 { get; set; }
    public int op4 { get; set; }
  }

  public class TeacherTaaktoedelingContactTimeResponse
  {
    public int total { get; set; }
    public int op1 { get; set; }
    public int op2 { get; set; }
    public int op3 { get; set; }
    public int op4 { get; set; }
  }

  public class TeacherTaaktoedelingContactTimeWithFactorResponse
  {
    public int total { get; set; }
    public int op1 { get; set; }
    public int op2 { get; set; }
    public int op3 { get; set; }
    public int op4 { get; set; }
  }

  public class TeacherTaaktoedelingInternsAndGraduatesSupervisionResponse
  {
    public TeacherTaaktoedelingSupervisionResponse interns { get; set; }
    public TeacherTaaktoedelingSupervisionResponse graduates { get; set; }
  }

  public class TeacherTaaktoedelingSupervisionResponse
  {
    public int total { get; set; }
    public int op1 { get; set; }
    public int op2 { get; set; }
    public int op3 { get; set; }
    public int op4 { get; set; }
  }

  public class TeacherNewDetacheringRequest
  {
    public int hours { get; set; }
    public string reason { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }
  }

  public class TeacherSupervisionResponse {
    public TeacherSupervisionSummaryResponse interns_summary { get; set; }
    public TeacherSupervisionSummaryResponse graduates_summary { get; set; }
    public List<TeacherSupervisionObjectResponse> interns { get; set; }
    public List<TeacherSupervisionObjectResponse> graduates { get; set; }
  }

  public class TeacherSupervisionSummaryResponse {
    public int total { get; set; }
    public int op1 { get; set; }
    public int op2 { get; set; }
    public int op3 { get; set; }
    public int op4 { get; set; }
  }

  public class TeacherSupervisionObjectResponse {
    public int id { get; set; }
    public int start_op { get; set; }
    public int end_op { get; set; }
  }

  public class TeacherCreateSupervisionRequest
  {
    public string type { get; set; }
    public int start_op { get; set; }
    public int end_op { get; set; }
  }

  public class TeacherUpdateSupervision
  {
    public int id { get; set; }
    public int start_op { get; set; }
    public int end_op { get; set; }
  }

  public class TeacherDeleteSupervisionRequest
  {
    public int id { get; set; }
  }

  public class TeacherOnderwijsResponse
  {
    public int period { get; set; }
    public string course_code { get; set; }
    public string course_name { get; set; }
    public string curriculum_kind { get; set; }
    public string workmethod { get; set; }
    public float student_amount { get; set; }
    public float contact_student { get; set; }
    public float meetings { get; set; }
    public float meeting_duration { get; set; }
    public float raise_factor { get; set; }
    public float correction_time { get; set; }
    public float travel_time { get; set; }
  }

  public class TeacherMOTsResponse
  {
    public int id { get; set; }
    public string name { get; set; }
    public float hours { get; set; }
    public string type { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }
  }

  public class TeacherMOTsUpdateRequest
  {
    public int id { get; set; }
    public string name { get; set; }
    public float hours { get; set; }
    public string type { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }
  }

  public class TeacherNewMOTTaskRequest
  {
    public string name { get; set; }
    public float hours { get; set; }
    public string type { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }
  }

  public class TeacherMOTOptionsResponse
  {
    public int id { get; set; }
    public string name { get; set; }
    public float hours { get; set; }
    public string type { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }
  }

  public class TeacherMOTDeleteRequest
  {
    public int id { get; set; }
  }

  public class Teacher_ExportRequest
  {
    public bool personalia { get; set; }
    public bool taaktoedeling { get; set; }
    public bool onderwijstaken { get; set; }
  }

  public class Teacher_ExportOnderwijstakenRequest
  {
    public string type { get; set; }
  }

  public class Teacher_ExportOnderwijstakenResponse
  {
    public string name { get; set; }
    public string surname { get; set; }
    public string personnel_code { get; set; }
    public string curriculum { get; set; }
    public List<TeacherOnderwijsResponse> onderwijstaken { get; set; }
  }
}