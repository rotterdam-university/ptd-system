using System;
using System.Collections.Generic;

namespace libri.Controllers.DTO
{
  public class Planning_StudyYearResponse
  {
    public List<Planning_StudyYearClassResponse> classes { get; set; }
    public List<Planning_StudyYearCourseResponse> courses { get; set; }
    public List<Planning_StudyYearCellResponse> cells { get; set; }
  }

  public class Planning_StudyYearClassResponse
  {
    public int id { get; set; }
    public int pos_x { get; set; }
    public int pos_y { get; set; }
    public int class_id { get; set; }
    public string name { get; set; }
  }

  public class Planning_StudyYearCourseResponse
  {
    public int id { get; set; }
    public int pos_x { get; set; }
    public int pos_y { get; set; }
    public int course_id { get; set; }
    public string name { get; set; }
    public string work_method { get; set; }
    public List<Planning_StudyYearCellResponse> cells { get; set; }
  }

  public class Planning_StudyYearCellResponse
  {
    public int id { get; set; }
    public int pos_x { get; set; }
    public int pos_y { get; set; }
    public Planning_StudyYearLabelResponse label { get; set; }
    public Planning_StudyYearNoteResponse note { get; set; }
    public List<Planning_StudyYearTeacherResponse> teachers { get; set; }
  }

  public class Planning_StudyYearLabelResponse
  {
    public string name { get; set; }
    public string color { get; set; }
  }
  
  public class Planning_StudyYearNoteResponse
  {
    public string note { get; set; }
  }
  
  public class Planning_StudyYearTeacherResponse
  {
    public int id { get; set; }
    public string personnel_code { get; set; }
    public float available_hours { get; set; }
    public float workload_override { get; set; }
  }

  public class Planning_StudyYearExistingClasses
  {
    public int class_id { get; set; }
    public string code { get; set; }
    public string group_by { get; set; }
    public int size_op1 { get; set; }
    public int size_op2 { get; set; }
    public int size_op3 { get; set; }
    public int size_op4 { get; set; }
  }

  public class Planning_StudyYearExistingCourses
  {
    public int course_id { get; set; }
    public string code { get; set; }
    public string group_by { get; set; }
    public string name { get; set; }
    public Planning_StudyYearAvailableWorkMethods workmethod { get; set; }
    public Planning_StudyYearAvailableTeams team { get; set; }
    public float meetings { get; set; }
    public float meeting_duration { get; set; }
    public float correction_time { get; set; }
    public float travel_time { get; set; }
    public float teachers_simultaniously_teaching { get; set; }
    public float contact_student { get; set; }
    public float raise_factor { get; set; }
  }

  public class Planning_StudyYearAvailableWorkMethods
  {
    public int id { get; set; }
    public string name { get; set; }
  }

  public class Planning_StudyYearAvailableTeams
  {
    public int id { get; set; }
    public string name { get; set; }
  }

  public class Planning_StudyYearCreateClass
  {
    public int id { get; set; }
    public int class_id { get; set; }
    public string code { get; set; }
    public int size_op1 { get; set; }
    public int size_op2 { get; set; }
    public int size_op3 { get; set; }
    public int size_op4 { get; set; }
    public int pos_x { get; set; }
    public int pos_y { get; set; }
    public bool update { get; set; }
    public string side { get; set; }
    public string type { get; set; }
  }

  public class Planning_StudyYearClassCellResponse
  {
    public int class_id { get; set; }
    public string code { get; set; }
    public int size_op1 { get; set; }
    public int size_op2 { get; set; }
    public int size_op3 { get; set; }
    public int size_op4 { get; set; }
  }

  public class Planning_StudyYearCourseCellResponse
  {
    public int course_id { get; set; }
    public string code { get; set; }
    public string name { get; set; }
    public Planning_StudyYearAvailableWorkMethods workmethod { get; set; }
    public Planning_StudyYearAvailableTeams team { get; set; }
    public float meetings { get; set; }
    public float meeting_duration { get; set; }
    public float correction_time { get; set; }
    public float travel_time { get; set; }
    public float teachers_simultaniously_teaching { get; set; }
    public float contact_student { get; set; }
    public float raise_factor { get; set; }
  }

  public class Planning_StudyYearCreateCourse
  {
    public int id { get; set; }
    public int course_id { get; set; }
    public string code { get; set; }
    public string name { get; set; }
    public Planning_StudyYearAvailableWorkMethods workmethod { get; set; }
    public Planning_StudyYearAvailableTeams team { get; set; }
    public int meetings { get; set; }
    public float meeting_duration { get; set; }
    public int teachers_simultaniously_teaching { get; set; }
    public float contact_student { get; set; }
    public float raise_factor { get; set; }
    public float correction_time { get; set; }
    public float travel_time { get; set; }
    public int pos_x { get; set; }
    public int pos_y { get; set; }
    public bool update { get; set; }
    public string side { get; set; }
    public string type { get; set; }
  }

  public class Planning_CellTeacherWorkload
  {
    public int workload { get; set; }
  }

  public class Planning_AvailableTeachersForCell
  {
    public string name { get; set; }
    public List<Planning_AvailableTeachers> teachers { get; set; }
  }

  public class Planning_AvailableTeachers
  {
    public int id { get; set; }
    public string personnel_code { get; set; }
    public float available_hours { get; set; }
    public int cell_id { get; set; }
    public float workload_override { get; set; }
  }

  public class Planning_TeacherWorkloadOverride
  {
    public int id { get; set; }
    public string personnel_code { get; set; }
    public float available_hours { get; set; }
  }

  public class Planning_CellLabel
  {
    public int id { get; set; }
    public string name { get; set; }
    public string color { get; set; }
  }

  public class Planning_AssignLabelToCellRequest
  {
    public int id { get; set; }
  }

  public class Planning_CellNote
  {
    public int id { get; set; }
    public string note { get; set; }
  }

  public class Planning_SaveCellNoteRequest
  {
    public int id { get; set; }
    public string note { get; set; }
  }

  public class Planning_AssignTeacherToCellRequest
  {
    public int id { get; set; }
  }

  public class Planning_SetWorkloadOverrideRequest
  {
    public int teacher_id { get; set; }
    public float workload { get; set; }
  }

  public class Planning_RemoveTeacherToCellRequest
  {
    public int id { get; set; }
  }

  public class Planning_StudyYearNotesResponse
  {
    public int id { get; set; }
    public int cell_id { get; set; }
    public DateTime date { get; set; }
    public string note { get; set; }
    public string _class_name { get; set; }
    public string course_name { get; set; }
    public List<string> teachers { get; set; }
  }

  public class Planning_DefaultValuesResponse
  {
    public float meetings { get; set; }
    public float contact_students { get; set; }
    public float raise_factor { get; set; }
    public float correction_time { get; set; }
    public float travel_time { get; set; }
  }
}