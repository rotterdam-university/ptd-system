using System;
using System.Collections.Generic;

namespace ptd_system.Controllers.DTO
{
  public class CurriculumDetailsWithMOTTasks
  {
    public int id { get; set; }
    public string name { get; set; }
    public float default_fte { get; set; }
    public float default_fte_hours { get; set; }
    public float default_di { get; set; }
    public float default_organisational { get; set; }
    public float default_professional { get; set; }
    public float course_formula_w { get; set; }
    public float course_formula_ct { get; set; }
    public float course_formula_opslag { get; set; }
    public float course_formula_n { get; set; }
    public float course_formula_cor { get; set; }
    public float course_formula_r { get; set; }
    public DateTime start_date { get; set; }
    public DateTime end_date { get; set; }
    public List<MOTTask> mot_tasks { get; set; }
    public string availability_type { get; set; }
  }

  public class NewCurriculum
  {
    public string name { get; set; }
    public float default_fte { get; set; }
    public float default_fte_hours { get; set; }
    public float default_di { get; set; }
    public float default_organisational { get; set; }
    public float default_professional { get; set; }
    public float course_formula_w { get; set; }
    public float course_formula_ct { get; set; }
    public float course_formula_opslag { get; set; }
    public float course_formula_n { get; set; }
    public float course_formula_cor { get; set; }
    public float course_formula_r { get; set; }
    public DateTime start_date { get; set; }
    public DateTime end_date { get; set; }
    public int availability_kind { get; set; }
    public int curriculum_to_copy_from_id { get; set; }
    public List<string> copy_entities { get; set; }
  }

  public class MOTTask
  {
    public int id { get; set; }
    public string type { get; set; }
    public string name { get; set; }
    public float hours { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }
  }

  public class DeleteCurricula
  {
    public int id { get; set; }
  }

  public class CreateMOTTask
  {
    public string type { get; set; }
    public string name { get; set; }
    public float hours { get; set; }
    public bool active_op1 { get; set; }
    public bool active_op2 { get; set; }
    public bool active_op3 { get; set; }
    public bool active_op4 { get; set; }
  }

  public class DeleteMOTTask
  {
    public int id { get; set; }
  }
}