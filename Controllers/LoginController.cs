using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ptd_system.Helpers;
using ptd_system.Models;
using Microsoft.EntityFrameworkCore;

namespace ptd_system.Controllers
{
  [ApiController]
  [Authorize]
  [Route("api/[controller]")]
  public class LoginController : Controller
  {
    private readonly DatabaseContext _context;
    private readonly IWebHostEnvironment _currentEnvironment;

    public LoginController(DatabaseContext context, IWebHostEnvironment env)
    {
      _context = context;
      _currentEnvironment = env;
    }

    [HttpGet]
    public ActionResult Authenticate()
    {
      var mycookie = Request.Cookies;

      var u = User;
      
      var claimsIdentity = User.Identities.FirstOrDefault(id => id.AuthenticationType == "CWIPS");
      if(claimsIdentity != null && claimsIdentity.IsAuthenticated)
      {
        return Redirect($"https://{HttpContext.Request.Host}");
      }     
      return Unauthorized();
    }

    [HttpGet("get-user-credentials")]
    public IActionResult GetCredentials()
    {
      var userCookie = Request.Cookies;

      if (userCookie.Any(c => c.Key.Equals(".AspNetCore.Cookies")))
      {
        var claimsIdentity = User.Identities.FirstOrDefault(id => id.AuthenticationType == "CWIPS");
        List<string> UserProfile = claimsIdentity.Claims
        .Where(claim => claim.Type == System.Security.Claims.ClaimTypes.Role)
          .Select(claim => claim.Value).ToList();

        string userCode = "";
        string role = "";
        string institute = "";

        foreach (var item in UserProfile)
        {
          if (item.Length == 7)
            userCode = item.ToUpper();

          if (item == "cmi")
            institute = item;

          if (item.Length == 5)
            userCode = item.ToUpper();
        }

        if (userCode.Length == 0) {
          return NotFound("User code not found");
        }

        foreach (var item in UserProfile)
        {
          if (item == "dop")
          {
            role = "dop"; // Medewerker docerend personeel
          } else if (item == "obp")
          {
            role = "obp"; // Medewerker ondersteunend personeel
          } else if (item == "stud")
          {
            role = "stud"; // Student
          }
        }

        Teacher teacher;
        Manager manager = _context.Manager.FirstOrDefault(m => m.code.Equals(userCode));
        if (manager == null)
        {
          if (role.Equals("stud")) {
            return Unauthorized(ExceptionResponse.Create("user-is-student", $"User {userCode} is a student."));
          }

          teacher = _context.Teacher
            .Where(t => t.personnel_code.Equals(userCode)).OrderByDescending(t => t.Curriculum.end_date)
            .Include(t => t.Curriculum)
              .ThenInclude(c => c.Education)
              .ThenInclude(e => e.Institute)
            .FirstOrDefault(t => t.Curriculum.end_date < DateTime.Now);

          if (teacher == null) {
            return NotFound(ExceptionResponse.Create("user-not-found", $"User {userCode} was not found as manager or teacher."));
          }

          return Ok(new { institute = teacher.Curriculum.Education.Institute.name, education = teacher.Curriculum.Education.name, email = $"{teacher.personnel_code}@hr.nl", personnel_code = teacher.personnel_code, role = "teacher" });
        } else {
          Education education = _context.Education.FirstOrDefault(e => e.EducationID.Equals(manager.EducationID));
          Institute institute1 = _context.Institute.FirstOrDefault(e => e.InstituteID.Equals(education.InstituteID));

          return Ok(new { institute = institute1.name, education = education.name, email = manager.email, personnel_code = manager.code, role = "manager" });
        }
      }

      return Unauthorized();
    }
  }
}