using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ptd_system.Models;
using Microsoft.AspNetCore.Authorization;
using ptd_system.Helpers;
using System.Threading.Tasks;
using libri.Controllers.DTO;

namespace ptd_system.Controllers
{
  [ApiController]
  [Authorize]
  [Route("api/[controller]")]
  public class PlanningController : Controller
  {
    private readonly DatabaseContext _context;
    private readonly AuthenticationHelper _AuthenticationHelper;
    private readonly WorkloadCalculatorHelper _WorkloadCalculatorHelper;
    private readonly ExportHelper _ExportHelper;

    public PlanningController(DatabaseContext context)
    {
      _context = context;
      _AuthenticationHelper = new AuthenticationHelper(context);
      _WorkloadCalculatorHelper = new WorkloadCalculatorHelper(context);
      _ExportHelper = new ExportHelper(context);
    }

    [HttpGet("all-curricula")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult AllCurricula()
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        var payload = _context.Curriculum
          .Where(c => c.Education.name.Equals(user.education))
          .Select(c => new {
            id = c.CurriculumID,
            curriculum = c.name,
            kinds = _context.CurriculumKind.Where(ck => ck.Education.name.Equals(user.education)).OrderByDescending(ck => ck.name).Select(ck => ck.name).ToList()
          }).ToList();

        return Ok(payload);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/{study_year}/{op}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public async Task<IActionResult> GetPlanningForCurriculumAsync(int curriculum_1, int curriculum_2, string kind, int study_year, int op)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        List<Planning_StudyYearClassResponse> classes = await _context.Cell
          .Include(c => c.OP)
            .ThenInclude(op => op.StudyYear)
              .ThenInclude(sy => sy.Curriculum)
                .ThenInclude(c => c.Education)
                  .ThenInclude(e => e.CurriculumKinds)
          .Include(c => c.Class)
            .ThenInclude(c => c.CurriculumKind)
          .Where(c => c.OP.period.Equals(op))
          .Where(c => c.OP.StudyYear.year.Equals(study_year))
          .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          .Where(c => c.Class.CurriculumKind.name.Equals(kind))
          .Where(c => c.y_position.Equals(1))
          .Select(c => new Planning_StudyYearClassResponse {
            id = c.CellID,
            pos_x = c.x_position,
            pos_y = c.y_position,
            class_id = c.Class.ClassID,
            name = c.Class.name
          })
          .OrderBy(c => c.pos_x)
          .ToListAsync();
        
        List<Planning_StudyYearCourseResponse> courses = await _context.Cell
          .Include(c => c.OP)
            .ThenInclude(op => op.StudyYear)
              .ThenInclude(sy => sy.Curriculum)
                .ThenInclude(c => c.Education)
                  .ThenInclude(e => e.CurriculumKinds)
          .Include(c => c.Course)
          .Include(c => c.Label)
          .Include(c => c.CellNote)
          .Where(c => c.OP.period.Equals(op))
          .Where(c => c.OP.StudyYear.year.Equals(study_year))
          .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          .Where(c => c.Course.CurriculumKind.name.Equals(kind))
          .Where(c => c.x_position.Equals(1))
          .Select(c => new Planning_StudyYearCourseResponse {
            id = c.CellID,
            pos_x = c.x_position,
            pos_y = c.y_position,
            course_id = c.Course.CourseID,
            name = c.Course.code,
            work_method = c.Course.Workmethods.First().Workmethod.name
          })
          .OrderBy(c => c.pos_y)
          .ToListAsync();

        courses.ForEach(course => {
          course.cells = _context.Cell
            .Where(c => c.Course.CourseID.Equals(course.course_id))
            .Select(c => new Planning_StudyYearCellResponse {
              id = c.CellID,
              pos_x = c.x_position,
              pos_y = c.y_position,
              label = new Planning_StudyYearLabelResponse {
                name = c.Label.name,
                color = c.Label.color
              },
              note = new Planning_StudyYearNoteResponse {
                note = c.CellNote.note
              },
              teachers = c.Teachers.Select(t => new Planning_StudyYearTeacherResponse {
                id = t.Teacher.TeacherID,
                personnel_code = t.Teacher.personnel_code,
                workload_override = t.teacher_workload_override
              }).ToList()
            })
            .OrderBy(c => c.pos_x)
            .ToList();

          course.cells.ForEach(cell => {
            cell.teachers.ForEach(teacher => {
              teacher.available_hours = _WorkloadCalculatorHelper.GetOddmentForOP(teacher.id, op);
            });
          });
        });

        return Ok(new Planning_StudyYearResponse { classes = classes, courses = courses });
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/{study_year}/{op}/existing-classes")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetExistingClassesForPlanning(int curriculum_1, int curriculum_2, string kind, int study_year, int op)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        List<Planning_StudyYearExistingClasses> classes = _context._Class
          .Include(c => c.Cells)
            .ThenInclude(c => c.OP)
          .Where(c => c.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          .Where(c => c.CurriculumKind.name.Equals(kind))
          .Where(c => c.Cells.Count.Equals(0) || (c.Cells.Count() < 4 && c.Cells.All(c => !c.OP.period.Equals(op))))
          // .Where(c => c.Cells.Count() < 4)
          // .Where(c => c.Cells.Any(c => !c.OP.period.Equals(op)))
          .Select(c => new Planning_StudyYearExistingClasses {
            class_id = c.ClassID,
            code = c.name,
            group_by = c.name.Substring(0, 4).Length > 0 ? c.name.Substring(0, 4) : c.name,
            size_op1 = c.class_size_op1,
            size_op2 = c.class_size_op2,
            size_op3 = c.class_size_op3,
            size_op4 = c.class_size_op4
          })
          .OrderBy(c => c.code) 
          .ToList();

        return Ok(classes);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/{study_year}/{op}/default-student-amount")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetDefaultStudentAmount(int curriculum_1, int curriculum_2, string kind, int study_year, int op)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        float amount = _context.Curriculum
          .FirstOrDefault(c => c.name.Equals($"{curriculum_1}/{curriculum_2}"))
          .course_formula_n;

        return Ok(amount);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/{study_year}/{op}/existing-courses")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetExistingCoursesForPlanning(int curriculum_1, int curriculum_2, string kind, int study_year, int op)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        List<Planning_StudyYearExistingCourses> courses = _context.Course
          .Include(c => c.Cells)
          .Include(c => c.Workmethods)
            .ThenInclude(c => c.Workmethod)
          .Include(c => c.Teams)
            .ThenInclude(c => c.Team)
          .Include(c => c.OP)
          .Where(c => c.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          .Where(c => c.CurriculumKind.name.Equals(kind))
          .Where(c => c.Cells.Count() <= 0)
          .Where(c => c.OP.period.Equals(op))
          .Select(c => new Planning_StudyYearExistingCourses {
            course_id = c.CourseID,
            group_by = c.Teams.First().Team.name.Length > 0 ? c.Teams.First().Team.name : "Geen team",
            code = c.code,
            name = c.name,
            workmethod = new Planning_StudyYearAvailableWorkMethods {
              id = c.Workmethods.First().Workmethod.WorkmethodID,
              name = c.Workmethods.First().Workmethod.name
            },
            team = new Planning_StudyYearAvailableTeams {
              id = c.Teams.First().Team.TeamID,
              name = c.Teams.First().Team.name
            },
            meetings = c.meetings,
            meeting_duration = c.meeting_duration,
            correction_time = c.correction_time,
            travel_time = c.travel_time,
            teachers_simultaniously_teaching = c.teachers_simultaniously_teaching,
            contact_student = c.contact_student,
            raise_factor = c.raise_factor
          })
          .OrderBy(c => c.code)
          .ToList();

        return Ok(courses);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/{study_year}/{op}/available-work-methods")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetAvailableWorkmethodsForPlanning(int curriculum_1, int curriculum_2, string kind, int study_year, int op)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Curriculum curriculum = _context.Curriculum
          .FirstOrDefault(c => c.name.Equals($"{curriculum_1}/{curriculum_2}"));

        List<Planning_StudyYearAvailableWorkMethods> workmethods = _context.Workmethod
          .Where(wm => wm.CurriculumID.Equals(curriculum.CurriculumID))
          .Select(wm => new Planning_StudyYearAvailableWorkMethods {
            id = wm.WorkmethodID,
            name = wm.name
          })
          .ToList();

        return Ok(workmethods);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/{study_year}/{op}/available-teams")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetAvailableTeamsForPlanning(int curriculum_1, int curriculum_2, string kind, int study_year, int op)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Curriculum curriculum = _context.Curriculum
          .FirstOrDefault(c => c.name.Equals($"{curriculum_1}/{curriculum_2}"));

        List<Planning_StudyYearAvailableTeams> teams = _context.Team
          .Where(t => t.CurriculumID.Equals(curriculum.CurriculumID))
          .Select(t => new Planning_StudyYearAvailableTeams {
            id = t.TeamID,
            name = t.name
          })
          .ToList();

        return Ok(teams);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("_class/{id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetClassDetailsForPlanning(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell _class = _context.Cell
          .Include(c => c.Class)
          .FirstOrDefault(c => c.CellID.Equals(id));
        if (_class == null) 
          return NotFound(ExceptionResponse.Create("cell-not-found", $"Cell with ID: {id} was not found."));

        Planning_StudyYearClassCellResponse ClassCell = new Planning_StudyYearClassCellResponse {
          class_id = _class.Class != null ? _class.Class.ClassID : -1,
          code = _class.Class != null ? _class.Class.name : "",
          size_op1 = _class.Class != null ? _class.Class.class_size_op1 : 0,
          size_op2 = _class.Class != null ? _class.Class.class_size_op2 : 0,
          size_op3 = _class.Class != null ? _class.Class.class_size_op3 : 0,
          size_op4 = _class.Class != null ? _class.Class.class_size_op4 : 0
        };

        return Ok(ClassCell);
      } else {
        return Unauthorized();
      }
    }

    [HttpDelete("_class/{id}/{op}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult RemoveClassFromPlanning(int id, int op)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        for (int i = 1; i < 5; i++)
        {
          List<Cell> cells = _context.Cell
            .Include(c => c.Class)
            .Include(c => c.Course)
            .Include(c => c.OP)
            .Where(c => c.OP.period.Equals(i))
            .Where(c => c.Class.ClassID.Equals(id))
            .ToList();

          if (cells.Count >= 1) {
            List<Cell> cellsToMove = _context.Cell
              .Where(c => c.OPID.Equals(cells.First().OPID))
              .Where(c => !c.ClassID.Equals(cells.First().ClassID))
              .Where(c => c.x_position > cells.First().x_position)
              .ToList();

            cellsToMove.ForEach(c => {
              c.x_position = c.x_position - 1;

              _context.Cell.Update(c);
              _context.SaveChanges();
            });
          }

          // List<Cell> otherCells = _context.Cell
          //   .Where(c => c.OPID.Equals(cells.First().OPID))
          //   .Where(c => !c.ClassID.Equals(cells.First().ClassID))
          //   .ToList();

          _context.Cell.RemoveRange(cells);
          _context.SaveChanges();
        }
        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{curriculum_1}/{curriculum_2}/{kind}/{study_year}/{op}/class")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult CreateClassInPlanning(int curriculum_1, int curriculum_2, string kind, int study_year, int op, [FromBody] Planning_StudyYearCreateClass newClass)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {

        _Class _class = new _Class();

        _Class existingClassWithSameName = _context._Class
          .Include(c => c.Curriculum)
          .Where(c => c.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          .FirstOrDefault(c => c.name.Contains(newClass.code));

        if (newClass.class_id.Equals(-1) & existingClassWithSameName == null) {
          // No class exists, create new
          _class.name = newClass.code;
          _class.CurriculumID = _context.Curriculum.FirstOrDefault(c => c.name.Equals($"{curriculum_1}/{curriculum_2}")).CurriculumID;
          _class.CurriculumKindID = _context.CurriculumKind.FirstOrDefault(ck => ck.name.Equals(kind)).CurriculumKindID;
          _class.class_size_op1 = newClass.size_op1;
          _class.class_size_op2 = newClass.size_op2;
          _class.class_size_op3 = newClass.size_op3;
          _class.class_size_op4 = newClass.size_op4;

          _context._Class.Add(_class);
          _context.SaveChanges();
        } else {
          // Class exists, look up id and update with new sizes per op
          _Class existingClass = existingClassWithSameName == null ? _context._Class.FirstOrDefault(c => c.ClassID.Equals(newClass.class_id)) : existingClassWithSameName;

          existingClass.name = newClass.code;
          existingClass.class_size_op1 = newClass.size_op1;
          existingClass.class_size_op2 = newClass.size_op2;
          existingClass.class_size_op3 = newClass.size_op3;
          existingClass.class_size_op4 = newClass.size_op4;

          _class = existingClass;

          _context._Class.Update(existingClass);
          _context.SaveChanges();
        }

        if (newClass.type.Equals("new_cell:end")) {
          for (int i = 1; i < 5; i++)
          {
            IQueryable<Cell> existingCells = _context.Cell
              .Include(c => c.OP)
                .ThenInclude(cop => cop.StudyYear)
                  .ThenInclude(sy => sy.Curriculum)
              .Include(c => c.Course)
              .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .Where(c => c.OP.StudyYear.year.Equals(study_year))
              .Where(c => c.OP.period.Equals(i));

            if (existingCells.Count() <= 0) {
              OP currentOP = _context.OP
              .Include(cop => cop.StudyYear)
                .ThenInclude(sy => sy.Curriculum)
              .Where(opp => opp.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .Where(opp => opp.StudyYear.year.Equals(study_year))
              .Where(opp => opp.period.Equals(i))
              .FirstOrDefault();

              Cell newCell = new Cell {
                OPID = currentOP.OPID,
                ClassID = _class.ClassID,
                x_position = 1,
                y_position = 1
              };

              _context.Cell.Add(newCell);
              _context.SaveChanges();
            } else {
              Cell lastCell = _context.Cell
                .Include(c => c.OP)
                  .ThenInclude(cop => cop.StudyYear)
                    .ThenInclude(sy => sy.Curriculum)
                .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
                .Where(c => c.OP.StudyYear.year.Equals(study_year))
                .Where(c => c.OP.period.Equals(i))
                .Where(c => c.y_position.Equals(1))
                .OrderByDescending(c => c.x_position)
                .FirstOrDefault();

              List<Cell> cells = _context.Cell
                .Include(c => c.OP)
                  .ThenInclude(cop => cop.StudyYear)
                    .ThenInclude(sy => sy.Curriculum)
                .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
                .Where(c => c.OP.StudyYear.year.Equals(study_year))
                .Where(c => c.OP.period.Equals(i))
                .Where(c => c.x_position.Equals(lastCell.x_position))
                .ToList();

              cells.ForEach(cell => {
                Cell newCell = new Cell {
                  OPID = cell.OPID,
                  CourseID = cell.CourseID,
                  ClassID = _class.ClassID,
                  x_position = cell.x_position + 1,
                  y_position = cell.y_position
                };

                _context.Cell.Add(newCell);
                _context.SaveChanges();
              });
            }
          }

          // IQueryable<Cell> cells = _context.Cell
          //   .Include(c => c.OP)
          //     .ThenInclude(cop => cop.StudyYear)
          //       .ThenInclude(sy => sy.Curriculum)
          //   .Include(c => c.Course)
          //   .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          //   .Where(c => c.OP.StudyYear.year.Equals(study_year))
          //   .Where(c => c.OP.period.Equals(op))
          //   .OrderByDescending(c => c.x_position);

          

          // List<Cell> allCells = cells.ToList();

          // if (allCells.Count() <= 0) {
          //   for (int i = 1; i < 5; i++)
          //   {
          //     OP OP = _context.OP
          //       .Include(op => op.StudyYear)
          //         .ThenInclude(sy => sy.Curriculum)
          //       .Where(op => op.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          //       .Where(op => op.StudyYear.year.Equals(study_year))
          //       .Where(opp => opp.period.Equals(i))
          //       .FirstOrDefault();

          //     Cell cell = new Cell {
          //       OPID = OP.OPID,
          //       ClassID = _class.ClassID,
          //       x_position = 1,
          //       y_position = 1
          //     };

          //     _context.Cell.Add(cell);
          //     _context.SaveChanges();
          //   }
          // } else {
          //   // Iterate through course cells and create new ones
          //   allCells.ForEach(c => {
          //     for (int i = 1; i < 5; i++)
          //     {
          //       OP OP = _context.OP
          //         .Include(op => op.StudyYear)
          //           .ThenInclude(sy => sy.Curriculum)
          //         .Where(op => op.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          //         .Where(op => op.StudyYear.year.Equals(study_year))
          //         .Where(opp => opp.period.Equals(i))
          //         .FirstOrDefault();

          //       Cell lastCellInX_Axis = _context.Cell
          //         .Include(co => co.OP)
          //           .ThenInclude(coop => coop.StudyYear)
          //             .ThenInclude(cosy => cosy.Curriculum)
          //         .Include(co => co.Class)
          //         .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          //         .Where(c => c.OP.StudyYear.year.Equals(study_year))
          //         .Where(c => c.OP.OPID.Equals(OP.OPID))
          //         // .Where(co => co.CourseID.Equals(c.CourseID))
          //         .OrderByDescending(co => co.x_position)
          //         .FirstOrDefault();

          //       if (lastCellInX_Axis.Class == null) {
          //         lastCellInX_Axis.ClassID = _class.ClassID;

          //         _context.Cell.Update(lastCellInX_Axis);
          //         _context.SaveChanges();
          //       } else {
          //         Cell cell = new Cell {
          //           OPID = lastCellInX_Axis.OPID,
          //           CourseID = lastCellInX_Axis.CourseID,
          //           ClassID = _class.ClassID,
          //           x_position = lastCellInX_Axis.x_position + 1,
          //           y_position = lastCellInX_Axis.y_position
          //         };

          //         _context.Cell.Add(cell);
          //         _context.SaveChanges();
          //       }
          //     }
          //   });
          // }
        }

        if (newClass.type.Equals("new_cell:left") || newClass.type.Equals("new_cell:right")) {
          for (int i = 1; i < 5; i++)
          {
            // Move columns to the right
            List<Cell> insertBeforeCells = _context.Cell
              .Include(c => c.OP)
                .ThenInclude(oop => oop.StudyYear)
                  .ThenInclude(osy => osy.Curriculum)
              .Include(c => c.Course)
              .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .Where(c => c.OP.StudyYear.year.Equals(study_year))
              .Where(c => c.OP.period.Equals(i))
              .Where(c => c.x_position >= newClass.pos_x)
              .OrderBy(c => c.x_position)
              .ToList();

            List<Cell> originalCells = _context.Cell
              .Include(c => c.OP)
                .ThenInclude(oop => oop.StudyYear)
                  .ThenInclude(osy => osy.Curriculum)
              .Include(c => c.Course)
              .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .Where(c => c.OP.StudyYear.year.Equals(study_year))
              .Where(c => c.OP.period.Equals(i))
              .Where(c => c.x_position.Equals(1))
              .ToList();

            insertBeforeCells.ForEach(c => {
              c.x_position = c.x_position + 1;

              _context.Cell.Update(c);
              _context.SaveChanges();
            });

            // Iterate through cells and create new ones
            originalCells.ForEach(c => {
              // for (int i = 1; i < 5; i++)
              // {
              //   OP OP = _context.OP
              //     .Include(op => op.StudyYear)
              //       .ThenInclude(sy => sy.Curriculum)
              //     .Where(op => op.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              //     .Where(op => op.StudyYear.year.Equals(study_year))
              //     .Where(opp => opp.period.Equals(i))
              //     .FirstOrDefault();

              //   Cell lastCellInX_Axis = _context.Cell
              //     .Include(co => co.OP)
              //       .ThenInclude(coop => coop.StudyYear)
              //         .ThenInclude(cosy => cosy.Curriculum)
              //     .Include(co => co.Class)
              //     .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              //     .Where(c => c.OP.StudyYear.year.Equals(study_year))
              //     .Where(c => c.OP.OPID.Equals(OP.OPID))
              //     .Where(co => co.CourseID.Equals(c.CourseID))
              //     .OrderByDescending(co => co.x_position)
              //     .FirstOrDefault();

              //   Cell cell = new Cell {
              //     OPID = lastCellInX_Axis.OPID,
              //     CourseID = lastCellInX_Axis.CourseID,
              //     ClassID = _class.ClassID,
              //     x_position = newClass.pos_x,
              //     y_position = lastCellInX_Axis.y_position
              //   };

              //   _context.Cell.Add(cell);
              //   _context.SaveChanges();
              // }
              Cell lastCellInX_Axis = _context.Cell
                .Include(co => co.OP)
                  .ThenInclude(coop => coop.StudyYear)
                    .ThenInclude(osy => osy.Curriculum)
                .Where(co => co.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
                .Where(co => co.OP.StudyYear.year.Equals(study_year))
                .Where(co => co.OP.period.Equals(i))
                .Where(co => co.CourseID.Equals(c.CourseID))
                .FirstOrDefault();

              Cell cell = new Cell {
                OPID = lastCellInX_Axis.OPID,
                CourseID = lastCellInX_Axis.CourseID,
                ClassID = _class.ClassID,
                x_position = newClass.pos_x,
                y_position = lastCellInX_Axis.y_position
              };

              _context.Cell.Add(cell);
              _context.SaveChanges();
            });
          }
        }

        if (newClass.type.Equals("existing_cell:empty:middle") || newClass.type.Equals("existing_cell:class:middle")) {
          for (int i = 1; i < 5; i++)
          {
            List<Cell> cells = _context.Cell
                .Include(c => c.OP)
                  .ThenInclude(coop => coop.StudyYear)
                    .ThenInclude(cosy => cosy.Curriculum)
              .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .Where(c => c.OP.StudyYear.year.Equals(study_year))
              .Where(c => c.OP.period.Equals(i))
              .Where(c => c.x_position.Equals(newClass.pos_x))
              .ToList();

            cells.ForEach(c => {
              c.ClassID = _class.ClassID;

              _context.Cell.Update(c);
              _context.SaveChanges();
            });
          }
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("course/{id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetCourseDetailsForPlanning(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell course = _context.Cell
          .Include(c => c.Course)
            .ThenInclude(c => c.Workmethods)
              .ThenInclude(cwm => cwm.Workmethod)
          .Include(c => c.Course)
            .ThenInclude(c => c.Teams)
              .ThenInclude(ct => ct.Team)
          .FirstOrDefault(c => c.CellID.Equals(id));
        if (course == null) 
          return NotFound(ExceptionResponse.Create("cell-not-found", $"Cell with ID: {id} was not found."));

        if (course.Course == null) {
          return Ok(new Planning_StudyYearCourseCellResponse {});
        }

        Planning_StudyYearCourseCellResponse CourseCell = new Planning_StudyYearCourseCellResponse {
          course_id = course.Course.CourseID,
          code = course.Course.code,
          name = course.Course.name,
          workmethod = new Planning_StudyYearAvailableWorkMethods {
            id = course.Course.Workmethods.First().Workmethod.WorkmethodID,
            name = course.Course.Workmethods.First().Workmethod.name
          },
          team = new Planning_StudyYearAvailableTeams {
            id = course.Course.Teams.First().Team.TeamID,
            name = course.Course.Teams.First().Team.name
          },
          meetings = course.Course.meetings,
          correction_time = course.Course.correction_time,
          travel_time = course.Course.travel_time,
          teachers_simultaniously_teaching = course.Course.teachers_simultaniously_teaching,
          contact_student = course.Course.contact_student,
          raise_factor = course.Course.raise_factor,
        };

        return Ok(CourseCell);
      } else {
        return Unauthorized();
      }
    }

    [HttpDelete("course/{id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult RemoveCourseFromPlanning(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        List<Cell> cells = _context.Cell
          .Include(c => c.Class)
          .Include(c => c.Course)
          .Where(c => c.Course.CourseID.Equals(id))
          .ToList();

        if (cells.Count >= 1) {
          List<Cell> cellsToMove = _context.Cell
            .Where(c => c.OPID.Equals(cells.First().OPID))
            .Where(c => !c.CourseID.Equals(cells.First().CourseID))
            .Where(c => c.y_position > cells.First().y_position)
            .ToList();

          cellsToMove.ForEach(c => {
            c.y_position = c.y_position - 1;

            _context.Cell.Update(c);
            _context.SaveChanges();
          });
        }

        List<Cell> otherCells = _context.Cell
          .Where(c => c.OPID.Equals(cells.First().OPID))
          .Where(c => !c.CourseID.Equals(cells.First().CourseID))
          .ToList();

        _context.Cell.RemoveRange(cells);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/default-values")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetDefaultCourseValues(int curriculum_1, int curriculum_2, string kind)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum c = _context.Curriculum
          .FirstOrDefault(c => c.name.Equals($"{curriculum_1}/{curriculum_2}"));

        Planning_DefaultValuesResponse res = new Planning_DefaultValuesResponse {
          meetings = c.course_formula_w,
          contact_students = c.course_formula_ct,
          raise_factor = c.course_formula_opslag,
          correction_time = c.course_formula_cor,
          travel_time = c.course_formula_r
        };

        return Ok(res);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{curriculum_1}/{curriculum_2}/{kind}/{study_year}/{op}/course")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult CreateCourseInPlanning(int curriculum_1, int curriculum_2, string kind, int study_year, int op, [FromBody] Planning_StudyYearCreateCourse newCourse)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Course course = new Course();

        if (newCourse.course_id.Equals(-1)) {
          // No course exists, create new course
          course.CurriculumID = _context.Curriculum.FirstOrDefault(c => c.name.Equals($"{curriculum_1}/{curriculum_2}")).CurriculumID;
          course.CurriculumKindID = _context.CurriculumKind.FirstOrDefault(ck => ck.name.Equals(kind)).CurriculumKindID;
          course.OPID = _context.OP.FirstOrDefault(dop => dop.period.Equals(op) && dop.StudyYear.year.Equals(study_year) && dop.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}")).OPID;
          course.code = newCourse.code;
          course.name = newCourse.name;
          course.meetings = newCourse.meetings;
          course.contact_student = newCourse.contact_student;
          course.correction_time = newCourse.correction_time;
          course.travel_time = newCourse.travel_time;
          course.meeting_duration = newCourse.meeting_duration;
          course.teachers_simultaniously_teaching = newCourse.teachers_simultaniously_teaching;
          course.contact_student = newCourse.contact_student;
          course.raise_factor = newCourse.raise_factor;
          course.correction_time = newCourse.correction_time;
          course.travel_time = newCourse.travel_time;

          _context.Course.Add(course);
          _context.SaveChanges();

          if (newCourse.workmethod.id.Equals(-1) && newCourse.workmethod.name.Equals("Standaard")) {
            Workmethod workmethod = _context.Workmethod
              .Include(wm => wm.Curriculum)
              .Where(wm => wm.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .FirstOrDefault(wm => wm.name.Equals("Standaard"));

            CourseWorkmethods courseWorkmethods = new CourseWorkmethods {
              CourseID = course.CourseID,
              WorkmethodID = workmethod.WorkmethodID
            };

            _context.CourseWorkmethods.Add(courseWorkmethods);
            _context.SaveChanges();
          } else if (!newCourse.workmethod.id.Equals(-1)) {
            Workmethod workmethod = _context.Workmethod
              .Include(wm => wm.Curriculum)
              .Where(wm => wm.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .FirstOrDefault(wm => wm.WorkmethodID.Equals(newCourse.workmethod.id));

            CourseWorkmethods courseWorkmethods = new CourseWorkmethods {
              CourseID = course.CourseID,
              WorkmethodID = workmethod.WorkmethodID
            };

            _context.CourseWorkmethods.Add(courseWorkmethods);
            _context.SaveChanges();
          } else {
            return BadRequest();
          }

          if (newCourse.team.id.Equals(-1) && newCourse.team.name.Equals("Geen team")) {
            Team team = _context.Team
              .Include(t => t.Curriculum)
              .Where(t => t.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .FirstOrDefault(t => t.name.Equals("Geen team"));

            CourseTeams courseTeams = new CourseTeams {
              CourseID = course.CourseID,
              TeamID = team.TeamID
            };

            _context.CourseTeams.Add(courseTeams);
            _context.SaveChanges();
          } else if (!newCourse.team.id.Equals(-1)) {
            Team team = _context.Team
              .Include(t => t.Curriculum)
              .Where(t => t.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .FirstOrDefault(t => t.TeamID.Equals(newCourse.team.id));

            CourseTeams courseTeams = new CourseTeams {
              CourseID = course.CourseID,
              TeamID = team.TeamID
            };

            _context.CourseTeams.Add(courseTeams);
            _context.SaveChanges();
          } else {
            return BadRequest();
          }
        } else {
          // Course exists, look up id and update with new properties
          Course existingCourse = _context.Course
            .Include(c => c.Workmethods)
              .ThenInclude(cwm => cwm.Workmethod)
            .Include(c => c.Teams)
              .ThenInclude(ct => ct.Team)
            .FirstOrDefault(c => c.CourseID.Equals(newCourse.course_id));

          existingCourse.code = newCourse.code;
          existingCourse.name = newCourse.name;
          existingCourse.meetings = newCourse.meetings;
          existingCourse.contact_student = newCourse.contact_student;
          existingCourse.correction_time = newCourse.correction_time;
          existingCourse.travel_time = newCourse.travel_time;
          existingCourse.meeting_duration = newCourse.meeting_duration;
          existingCourse.teachers_simultaniously_teaching = newCourse.teachers_simultaniously_teaching;
          existingCourse.contact_student = newCourse.contact_student;
          existingCourse.raise_factor = newCourse.raise_factor;

          CourseWorkmethods courseWorkmethods = _context.CourseWorkmethods.FirstOrDefault(cwm => cwm.CourseID.Equals(existingCourse.CourseID));
          Workmethod workmethod = _context.Workmethod.FirstOrDefault(t => t.WorkmethodID.Equals(newCourse.workmethod.id));

          if (!courseWorkmethods.WorkmethodID.Equals(workmethod.WorkmethodID)) {
            _context.CourseWorkmethods.Remove(courseWorkmethods);

            CourseWorkmethods newCourseWorkmethod = new CourseWorkmethods {
              CourseID = existingCourse.CourseID,
              WorkmethodID = workmethod.WorkmethodID
            };

            _context.CourseWorkmethods.Add(newCourseWorkmethod);
            _context.SaveChanges();
          }

          course = existingCourse;

          _context.Course.Update(existingCourse);
          _context.SaveChanges();

          if (newCourse.team.id.Equals(-1)) {
            IQueryable<CourseTeams> courseTeams = _context.CourseTeams.Where(ct => ct.CourseID.Equals(existingCourse.CourseID));

            _context.CourseTeams.RemoveRange(courseTeams);
            _context.SaveChanges();
          } else {
            if (!existingCourse.Teams.First().TeamID.Equals(newCourse.team.id)) {
              CourseTeams courseTeams = existingCourse.Teams.First();
              _context.CourseTeams.Remove(courseTeams);
              
              CourseTeams newCourseTeams = new CourseTeams {
                CourseID = existingCourse.CourseID,
                TeamID = newCourse.team.id
              };

              _context.CourseTeams.Add(newCourseTeams);
              _context.SaveChanges();
            }
          }
        }

        if (newCourse.type.Equals("new_cell:end")) {
          IQueryable<Cell> cells = _context.Cell
            .Include(c => c.OP)
              .ThenInclude(cop => cop.StudyYear)
                .ThenInclude(csy => csy.Curriculum)
            .Include(c => c.Class)
            .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
            .Where(c => c.OP.StudyYear.year.Equals(study_year))
            .Where(c => c.OP.period.Equals(op))
            .Where(c => c.y_position.Equals(1));

          if (cells.ToList().Count() <= 0) {
            // Planning is empty, just create new cell
            OP OP = _context.OP
              .Include(op => op.StudyYear)
                .ThenInclude(sy => sy.Curriculum)
              .Where(op => op.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .Where(op => op.StudyYear.year.Equals(study_year))
              .Where(opp => opp.period.Equals(op))
              .FirstOrDefault();

            Cell cell = new Cell {
              OPID = OP.OPID,
              CourseID = course.CourseID,
              x_position = 1,
              y_position = 1
            };

            _context.Cell.Add(cell);
            _context.SaveChanges();
          } else {
            // Iterate through course cells and create new ones
            cells.ToList().ForEach(c => {
              Cell lastCellInY_Axis = _context.Cell
                .Include(co => co.Course)
                .Include(co => co.OP)
                  .ThenInclude(coop => coop.StudyYear)
                    .ThenInclude(sy => sy.Curriculum)
                .Where(co => co.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
                .Where(co => co.OP.StudyYear.year.Equals(study_year))
                .Where(co => co.OP.period.Equals(op))
                .Where(co => co.ClassID.Equals(c.ClassID))
                .OrderByDescending(co => co.y_position)
                .FirstOrDefault();

              if (lastCellInY_Axis.Course == null) {
                lastCellInY_Axis.CourseID = course.CourseID;

                _context.Cell.Update(lastCellInY_Axis);
                _context.SaveChanges();
              } else {
                Cell cell = new Cell {
                  OPID = lastCellInY_Axis.OPID,
                  CourseID = course.CourseID,
                  ClassID = lastCellInY_Axis.ClassID,
                  x_position = lastCellInY_Axis.x_position,
                  y_position = lastCellInY_Axis.y_position + 1
                };

                _context.Cell.Add(cell);
                _context.SaveChanges();
              }
            });
          }
        }
        
        if (newCourse.type.Equals("new_cell:top") || newCourse.type.Equals("new_cell:bottom")) {
          // Move columns to the bottom
          List<Cell> insertBeforeCells = _context.Cell
            .Include(c => c.OP)
              .ThenInclude(cop => cop.StudyYear)
                .ThenInclude(sy => sy.Curriculum)
            .Include(c => c.Course)
            .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
            .Where(c => c.OP.StudyYear.year.Equals(study_year))
            .Where(c => c.OP.period.Equals(op))
            .Where(c => c.y_position >= newCourse.pos_y)
            .OrderBy(c => c.y_position)
            .ToList();

          List<Cell> originalCells = _context.Cell
            .Include(c => c.OP)
              .ThenInclude(cop => cop.StudyYear)
                .ThenInclude(sy => sy.Curriculum)
            .Include(c => c.Course)
            .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
            .Where(c => c.OP.StudyYear.year.Equals(study_year))
            .Where(c => c.OP.period.Equals(op))
            .Where(c => c.y_position.Equals(1))
            .ToList();

          insertBeforeCells.ForEach(c => {
            c.y_position = c.y_position + 1;

            _context.Cell.Update(c);
            _context.SaveChanges();
          });

          // Iterate through cells and create new ones
          originalCells.ForEach(c => {
            Cell lastCellInY_Axis = _context.Cell
              .Include(co => co.OP)
                .ThenInclude(coop => coop.StudyYear)
                  .ThenInclude(sy => sy.Curriculum)
              .Where(co => co.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
              .Where(co => co.OP.StudyYear.year.Equals(study_year))
              .Where(co => co.OP.period.Equals(op))
              .Where(co => co.ClassID.Equals(c.ClassID))
              .FirstOrDefault();

            Cell cell = new Cell {
              OPID = lastCellInY_Axis.OPID,
              CourseID = course.CourseID,
              ClassID = lastCellInY_Axis.ClassID,
              x_position = lastCellInY_Axis.x_position,
              y_position = newCourse.pos_y
            };

            _context.Cell.Add(cell);
            _context.SaveChanges();
          });
        }

        if (newCourse.type.Equals("existing_cell:empty:middle") || newCourse.type.Equals("existing_cell:course:middle")) {
          List<Cell> cells = _context.Cell
            .Include(c => c.OP)
              .ThenInclude(coop => coop.StudyYear)
                .ThenInclude(sy => sy.Curriculum)
            .Where(co => co.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
            .Where(co => co.OP.StudyYear.year.Equals(study_year))
            .Where(co => co.OP.period.Equals(op))
            .Where(c => c.y_position.Equals(newCourse.pos_y))
            .ToList();

          cells.ForEach(c => {
            c.CourseID = course.CourseID;

            _context.Cell.Update(c);
            _context.SaveChanges();
          });
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("cell/{id}/teacher-workload")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetTeacherWorkloadForCell(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        int workload = _WorkloadCalculatorHelper.GetWorkloadForCell(id).workloadWithRaiseFactor;

        return Ok(new Planning_CellTeacherWorkload { workload = workload });
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("cell/{id}/available-teachers")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetAvailableTeachersForCell(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell cell = _context.Cell
          .Include(c => c.Course)
            .ThenInclude(c => c.Curriculum)
          .Include(c => c.Course)
            .ThenInclude(c => c.Teams)
              .ThenInclude(c => c.Team)
          .Include(c => c.Teachers)
            .ThenInclude(tc => tc.Teacher)
          .Include(c => c.OP)
          .FirstOrDefault(c => c.CellID.Equals(id));

        List<Planning_AvailableTeachersForCell> teachers = new List<Planning_AvailableTeachersForCell>();

        _context.Team
          .Include(t => t.Teachers)
            .ThenInclude(t => t.Teacher)
              .ThenInclude(t => t.Cells)
          .Include(t => t.Curriculum)
          .Where(t => t.Curriculum.CurriculumID.Equals(cell.Course.CurriculumID))
          .ToList()
          .ForEach(team => {
            var teamTeachers = team.Teachers
              .Where(tc => tc.Teacher.Cells.FirstOrDefault(c => c.CellID.Equals(cell.CellID)) == null)
              .Select(teacher => new Planning_AvailableTeachers {
                id = teacher.Teacher.TeacherID,
                personnel_code = teacher.Teacher.personnel_code,
                available_hours = _WorkloadCalculatorHelper.GetOddmentForOP(teacher.TeacherID, cell.OP.period)
              })
              .OrderBy(t => t.personnel_code)
              .ToList();

            if (team.name.Equals(cell.Course.Teams.FirstOrDefault().Team.name)) {
              teachers.Insert(0, new Planning_AvailableTeachersForCell {
                name = team.name,
                teachers = teamTeachers
              });
            } else {
              teachers.Add(new Planning_AvailableTeachersForCell {
                name = team.name,
                teachers = teamTeachers
              });
            }
          });

        List<Teacher> teachersWithoutATeam = _context.Teacher
          .Where(t => t.Cells.FirstOrDefault(c => c.CellID.Equals(cell.CellID)) == null)
          .Where(t => t.Teams.Count.Equals(0))
          .ToList();

        teachers.Add(new Planning_AvailableTeachersForCell {
          name = "Geen team",
          teachers = teachersWithoutATeam.Select(t => new Planning_AvailableTeachers {
            id = t.TeacherID,
            personnel_code = t.personnel_code
          }).ToList()
        });

        return Ok(teachers);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("cell/{id}/teacher-workload-override")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult SetTeacherWorkloads(int id, [FromBody] List<Planning_AvailableTeachers> teachers)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        teachers.ForEach(t => {
          TeacherCells teacherCell = _context.TeacherCells
            .FirstOrDefault(tc => tc.TeacherID.Equals(t.id) && tc.CellID.Equals(t.cell_id));

          teacherCell.teacher_workload_override = t.workload_override;

          _context.TeacherCells.Update(teacherCell);
          _context.SaveChanges();
        });

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("cell/{id}/labels")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetLabelsForCell(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell cell = _context.Cell
          .Include(c => c.OP)
            .ThenInclude(op => op.StudyYear)
              .ThenInclude(s => s.Curriculum)
                .ThenInclude(c => c.Education)
                  .ThenInclude(e => e.Labels)
          .FirstOrDefault(c => c.CellID.Equals(id));

        List<Planning_CellLabel> labels = cell
          .OP.StudyYear.Curriculum.Education.Labels
          .Select(l => new Planning_CellLabel {
            id = l.LabelID,
            name = l.name,
            color = l.color
          })
          .ToList();

        return Ok(labels);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("cell/{id}/label")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult AssignLabelToCell(int id, [FromBody] Planning_AssignLabelToCellRequest label)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell cell = _context.Cell
          .FirstOrDefault(c => c.CellID.Equals(id));

        Label educationLabel = _context.Label
          .FirstOrDefault(l => l.LabelID.Equals(label.id));

        if (cell.CellLabelID == null) {
          CellLabel cellLabel = new CellLabel {
            name = educationLabel.name,
            color = educationLabel.color
          };

          _context.CellLabel.Add(cellLabel);
          _context.SaveChanges();

          cell.CellLabelID = cellLabel.CellLabelID;

          _context.Cell.Update(cell);
          _context.SaveChanges();
        } else {
          CellLabel cellLabel = _context.CellLabel
            .FirstOrDefault(cl => cl.CellLabelID.Equals(cell.CellLabelID));

          cellLabel.name = educationLabel.name;
          cellLabel.color = educationLabel.color;

          _context.CellLabel.Update(cellLabel);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpDelete("cell/{id}/label")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult RemoveLabelFromCell(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell cell = _context.Cell
          .FirstOrDefault(c => c.CellID.Equals(id));

        if (cell.CellLabelID != null) {
          CellLabel cellLabel = _context.CellLabel
            .FirstOrDefault(cl => cl.CellLabelID.Equals(cell.CellLabelID));

          cell.CellLabelID = null;
          
          _context.Cell.Update(cell);
          _context.SaveChanges();

          _context.CellLabel.Remove(cellLabel);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("cell/{id}/note")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetNoteForCell(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell cell = _context.Cell
          .Include(c => c.CellNote)
          .FirstOrDefault(c => c.CellID.Equals(id));

        Planning_CellNote cellNote = new Planning_CellNote();

        if (cell.CellNoteID != null) {
          cellNote.id = cell.CellNote.CellNoteID;
          cellNote.note = cell.CellNote.note;
        } else {
          cellNote.id = -1;
          cellNote.note = "";
        }

        return Ok(cellNote);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("cell/{id}/note")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult SaveNoteForCell(int id, [FromBody] Planning_SaveCellNoteRequest note)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell cell = _context.Cell
          .Include(c => c.CellNote)
          .FirstOrDefault(c => c.CellID.Equals(id));

        if (cell.CellNoteID == null) {
          CellNote cellNote = new CellNote {
            note = note.note
          };

          _context.CellNote.Add(cellNote);
          _context.SaveChanges();

          cell.CellNoteID = cellNote.CellNoteID;

          _context.Cell.Update(cell);
          _context.SaveChanges();
        } else {
          cell.CellNote.note = note.note;

          _context.CellNote.Update(cell.CellNote);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpDelete("cell/{id}/note")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult RemoveNoteFromCell(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell cell = _context.Cell
          .FirstOrDefault(c => c.CellID.Equals(id));

        if (cell.CellNoteID != null) {
          CellNote cellNote = _context.CellNote
            .FirstOrDefault(cn => cn.CellNoteID.Equals(cell.CellNoteID));

          cell.CellNoteID = null;
          
          _context.Cell.Update(cell);
          _context.SaveChanges();

          _context.CellNote.Remove(cellNote);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("cell/{id}/teacher")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult AssingTeacherToCell(int id, [FromBody] Planning_AssignTeacherToCellRequest teacher)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell cell = _context.Cell
          .FirstOrDefault(c => c.CellID.Equals(id));
        
        TeacherCells teacherCells = new TeacherCells {
          TeacherID = teacher.id,
          CellID = cell.CellID,
          teacher_workload_override = 0F
        };
        
        _context.TeacherCells.Add(teacherCells);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("cell/{id}/teacher/workload")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult SetWorkloadOverride(int id, [FromBody] Planning_SetWorkloadOverrideRequest workload)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell cell = _context.Cell
          .FirstOrDefault(c => c.CellID.Equals(id));
          
        TeacherCells teacherCells = _context.TeacherCells
          .Where(tc => tc.CellID.Equals(cell.CellID))
          .Where(tc => tc.TeacherID.Equals(workload.teacher_id))
          .FirstOrDefault();

        teacherCells.teacher_workload_override = workload.workload;
        
        _context.TeacherCells.Update(teacherCells);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpDelete("cell/{id}/teacher")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult RemoveTeacherFromCell(int id, [FromBody] Planning_RemoveTeacherToCellRequest teacher)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Cell cell = _context.Cell
          .FirstOrDefault(c => c.CellID.Equals(id));

        TeacherCells teacherCells = _context.TeacherCells
          .Where(tc => tc.TeacherID.Equals(teacher.id) && tc.CellID.Equals(cell.CellID))
          .FirstOrDefault();
      
        _context.TeacherCells.Remove(teacherCells);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_1}/{curriculum_2}/{kind}/{study_year}/{op}/notes")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetNotesForStudyYear(int curriculum_1, int curriculum_2, string kind, int study_year, int op)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        List<Planning_StudyYearNotesResponse> notes = _context.Cell
          .Include(c => c.OP)
            .ThenInclude(op => op.StudyYear)
              .ThenInclude(sy => sy.Curriculum)
                .ThenInclude(c => c.Education)
          .Include(c => c.Class)
            .ThenInclude(c => c.CurriculumKind)
          .Include(c => c.CellNote)
          .Where(c => c.OP.period.Equals(op))
          .Where(c => c.OP.StudyYear.year.Equals(study_year))
          .Where(c => c.OP.StudyYear.Curriculum.name.Equals($"{curriculum_1}/{curriculum_2}"))
          .Where(c => c.Class.CurriculumKind.name.Equals(kind))
          .Where(c => c.CellNote != null)
          .Select(c => new Planning_StudyYearNotesResponse {
            id = c.CellNote.CellNoteID,
            cell_id = c.CellID,
            date = c.CellNote.date,
            note = c.CellNote.note,
            _class_name = c.Class.name,
            course_name = c.Course.code,
            teachers = c.Teachers.Select(tc => tc.Teacher.personnel_code).ToList()
          })
          .OrderBy(c => c.date)
          .ToList();

        return Ok(notes);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{curriculum_1}/{curriculum_2}/{kind}/{study_year}/{op}/export")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult ExportPlanning(int curriculum_1, int curriculum_2, string kind, int study_year, int op, [FromBody] ExportPlanningConfig config)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);
      
      if (user.role.Equals("manager")) {
        Curriculum curr = _context.Curriculum.FirstOrDefault();

        if (curr == null)
          return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum: {curriculum_1}/{curriculum_2} was not found."));

        PlanningBase response = _ExportHelper.ExportPlanning($"{curriculum_1}/{curriculum_2}", kind, study_year, op, config);

        return Ok(response);
      } else {
        return Unauthorized();
      }
    }
  }
}
