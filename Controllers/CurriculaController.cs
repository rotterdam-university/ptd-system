using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using ptd_system.Helpers;
using ptd_system.Models;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using ptd_system.Controllers.DTO;
using System.Text.Json;

namespace ptd_system.Controllers
{
  [ApiController]
  [Authorize]
  [Route("api/[controller]")]
  public class CurriculaController : Controller
  {
    private readonly DatabaseContext _context;
    private readonly AuthenticationHelper _AuthenticationHelper;

    public CurriculaController(DatabaseContext context)
    {
      _context = context;
      _AuthenticationHelper = new AuthenticationHelper(context);
    }

    [HttpGet]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult Index()
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {

        var curricula = _context.Education.Include(e => e.Curriculums).FirstOrDefault(e => e.name.Equals(user.education)).Curriculums.OrderByDescending(c => c.name).Select(c => new { id = c.CurriculumID, name = c.name }).ToList();
        return Ok(curricula);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetCurriculumDetails(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var curriculum = _context.Curriculum
          .Include(c => c.MOTs)
          .Include(c => c.AvailabilityKind)
          .Where(c => c.CurriculumID.Equals(id))
          .Select(c => new CurriculumDetailsWithMOTTasks() {
            id = c.CurriculumID,
            name = c.name,
            default_fte = c.default_fte,
            default_fte_hours = c.default_fte_hours,
            default_di = c.default_di,
            default_organisational = c.default_organisational,
            default_professional = c.default_professional,
            course_formula_w = c.course_formula_w,
            course_formula_ct = c.course_formula_ct,
            course_formula_opslag = c.course_formula_opslag,
            course_formula_n = c.course_formula_n,
            course_formula_cor = c.course_formula_cor,
            course_formula_r = c.course_formula_r,
            start_date = c.start_date,
            end_date = c.end_date,
            mot_tasks = c.MOTs.Select(m => new MOTTask() {
              id = m.MOTID,
              type = m.type,
              name = m.name,
              hours = m.hours,
              active_op1 = m.active_op1,
              active_op2 = m.active_op2,
              active_op3 = m.active_op3,
              active_op4 = m.active_op4
            }).ToList(),
            availability_type = $"{c.AvailabilityKind.block_duration}min x {c.AvailabilityKind.block_amount} blokken"
          }).First();
        if (curriculum == null)
          return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum with ID {id} was not found."));

        return Ok(curriculum);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult CreateCurriculum(NewCurriculum new_curriculum)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum curriculum = new Curriculum() {
          EducationID = _context.Education.FirstOrDefault(e => e.name.Equals(user.education)).EducationID,
          AvailabilityKindID = new_curriculum.availability_kind,
          name = new_curriculum.name,
          start_date = new_curriculum.start_date,
          end_date = new_curriculum.end_date,
          visibility_for_teachers = false,
          default_fte = new_curriculum.default_fte,
          default_fte_hours = 1659,
          default_di = new_curriculum.default_di,
          default_organisational = new_curriculum.default_organisational,
          default_professional = new_curriculum.default_professional,
          course_formula_w = new_curriculum.course_formula_w,
          course_formula_ct = new_curriculum.course_formula_ct,
          course_formula_opslag = new_curriculum.course_formula_opslag,
          course_formula_n = new_curriculum.course_formula_n,
          course_formula_cor = new_curriculum.course_formula_cor,
          course_formula_r = new_curriculum.course_formula_r
        };

        _context.Curriculum.Add(curriculum);
        _context.SaveChanges();

        for (int i = 1; i < 5; i++)
        {
          StudyYear studyYear = new StudyYear(){ CurriculumID = curriculum.CurriculumID, year = i };
          _context.StudyYear.Add(studyYear);
          _context.SaveChanges();

          for (int j = 1; j < 5; j++)
          {
            OP op = new OP() { StudyYearID = studyYear.StudyYearID, period = j };
            _context.OP.Add(op);
            _context.SaveChanges();
          }
        }

        if (new_curriculum.curriculum_to_copy_from_id != -1) {
          System.Console.WriteLine($"Copy entities from curriculum {new_curriculum.curriculum_to_copy_from_id}");
          int ctcf = new_curriculum.curriculum_to_copy_from_id;

          List<CopyEntityIndex> _teams = new List<CopyEntityIndex>();
          List<CopyEntityIndex> _positions = new List<CopyEntityIndex>();
          List<CopyEntityIndex> _teachers = new List<CopyEntityIndex>();
          List<CopyEntityIndex> _workmethods = new List<CopyEntityIndex>();
          List<CopyEntityIndex> _courses = new List<CopyEntityIndex>();
          List<CopyEntityIndex> _classes = new List<CopyEntityIndex>();
          List<CopyEntityIndex> _motTasks = new List<CopyEntityIndex>();

          List<Workmethod> workmethods = _context.Workmethod
            .Where(wm => wm.CurriculumID.Equals(ctcf)).ToList();

          workmethods.ForEach(wm => {
            Workmethod newWorkmethod = new Workmethod() {
              CurriculumID = curriculum.CurriculumID,
              name = wm.name
            };

            _context.Workmethod.Add(newWorkmethod);
            _context.SaveChanges();

            _workmethods.Add(new CopyEntityIndex(){ oldId = wm.WorkmethodID, newId = newWorkmethod.WorkmethodID });
          });

          List<Position> positions = _context.Position.Where(p => p.CurriculumID.Equals(ctcf)).ToList();

          positions.ForEach(p => {
            Position newPosition = new Position() {
              CurriculumID = curriculum.CurriculumID,
              name = p.name
            };

            _context.Position.Add(newPosition);
            _context.SaveChanges();
            _positions.Add(new CopyEntityIndex() { oldId = p.PositionID, newId = newPosition.PositionID });
          });

          if (new_curriculum.copy_entities.Any(ce => ce.Equals("Teams")))
          {
            List<Team> teams = _context.Team.Where(t => t.CurriculumID.Equals(ctcf)).ToList();

            teams.ForEach(t => {
              Team newTeam = new Team()
              {
                CurriculumID = curriculum.CurriculumID,
                name = t.name
              };

              _context.Team.Add(newTeam);
              _context.SaveChanges();

              _teams.Add(new CopyEntityIndex() { oldId = t.TeamID, newId = newTeam.TeamID });
            });
          }

          if (new_curriculum.copy_entities.Any(ce => ce.Equals("Docenten")))
          {
            List<Teacher> teachers = _context.Teacher
              .Where(t => t.CurriculumID.Equals(ctcf)).ToList();

            teachers.ForEach(t => {
              FTE oldFTE = _context.FTE.FirstOrDefault(fte => fte.TeacherID.Equals(t.TeacherID));
              DI oldDI = _context.DI.FirstOrDefault(di => di.TeacherID.Equals(t.TeacherID));

              CopyEntityIndex newPositionId = _positions.FirstOrDefault(p => p.oldId.Equals(t.PositionID));
              if (newPositionId == null)
                Console.WriteLine(JsonSerializer.Serialize(ExceptionResponse.Create("position-not-found", $"New position with old ID: {t.PositionID} not found")));

              Teacher newTeacher = new Teacher() {
                CurriculumID = curriculum.CurriculumID,
                PositionID = newPositionId.newId,
                personnel_code = t.personnel_code,
                first_name = t.first_name,
                last_name = t.last_name,
                placeholder = t.placeholder
              };

              _context.Teacher.Add(newTeacher);
              _context.SaveChanges();

              _teachers.Add(new CopyEntityIndex() { oldId = t.TeacherID, newId = newTeacher.TeacherID });

              FTE newFTE = new FTE()
              {
                TeacherID = newTeacher.TeacherID,
                contract = oldFTE.contract,
                months_active = oldFTE.months_active,
                fte_op1 = oldFTE.fte_op1,
                fte_op2 = oldFTE.fte_op2,
                fte_op3 = oldFTE.fte_op3,
                fte_op4 = oldFTE.fte_op4,
                organisational = oldFTE.organisational,
                professional = oldFTE.professional
              };

              _context.FTE.Add(newFTE);
              _context.SaveChanges();

              DI newDI = new DI()
              {
                TeacherID = newTeacher.TeacherID,
                di_op1 = oldDI.di_op1,
                di_op2 = oldDI.di_op2,
                di_op3 = oldDI.di_op3,
                di_op4 = oldDI.di_op4,
              };

              _context.DI.Add(newDI);
              _context.SaveChanges();

              List<TeacherMOT> teacherMOTs = _context.TeacherMOT
                .Where(tm => tm.TeacherID.Equals(t.TeacherID))
                .ToList();

              teacherMOTs.ForEach(tm => {
                TeacherMOT new_teacherMOT = new TeacherMOT {
                  TeacherID = newTeacher.TeacherID,
                  name = tm.name,
                  hours = tm.hours,
                  type = tm.type,
                  active_op1 = tm.active_op1,
                  active_op2 = tm.active_op2,
                  active_op3 = tm.active_op3,
                  active_op4 = tm.active_op4
                };

                _context.TeacherMOT.Add(new_teacherMOT);
                _context.SaveChanges();
              });

              if (_teams.Count > 0)
              {
                List<TeacherTeams> oldTeacherTeams = _context.TeacherTeams.Where(tt => tt.TeacherID.Equals(t.TeacherID)).ToList();

                oldTeacherTeams.ForEach(ott => {
                  CopyEntityIndex newTeamId = _teams.FirstOrDefault(t => t.oldId.Equals(ott.TeamID));
                  if (newTeamId == null)
                    Console.WriteLine(JsonSerializer.Serialize(ExceptionResponse.Create("team-not-found", $"New team with old ID: {ott.TeamID} not found")));

                  TeacherTeams newTeacherTeam = new TeacherTeams()
                  {
                    TeacherID = newTeacher.TeacherID,
                    TeamID = newTeamId.newId,
                    is_team_leader = ott.is_team_leader
                  };

                  _context.TeacherTeams.Add(newTeacherTeam);
                  _context.SaveChanges();
                });
              }
            });
          }

          if (new_curriculum.copy_entities.Any(ce => ce.Equals("Cursussen")))
          {
            List<Course> courses = _context.Course
              .Include(c => c.OP)
                .ThenInclude(op => op.StudyYear)
              .Include(c => c.Workmethods)
                .ThenInclude(c => c.Workmethod)
              .Where(c => c.CurriculumID.Equals(ctcf)).ToList();

            courses.ForEach(c => {
              OP newOP = _context.OP
                .Include(op => op.StudyYear)
                .Where(op => op.period.Equals(c.OP.period))
                .Where(op => op.StudyYear.year.Equals(c.OP.StudyYear.year))
                .Where(op => op.StudyYear.CurriculumID.Equals(curriculum.CurriculumID))
                .First();

              // StudyYear newStudyYear = _context.StudyYear
              //   .FirstOrDefault(s => s.year.Equals(c.OP.StudyYear.year) && s.CurriculumID.Equals(curriculum.CurriculumID));

              // OP newOP = _context.OP
              //   .FirstOrDefault(op => op.period.Equals(c.OP.period) && op.StudyYear.StudyYearID.Equals(newStudyYear.StudyYearID));

              Course newCourse = new Course() {
                CurriculumID = curriculum.CurriculumID,
                CurriculumKindID = c.CurriculumKindID,
                OPID = newOP.OPID,
                code = c.code,
                name = c.name,
                ects = c.ects,
                raise_factor = c.raise_factor,
                meetings = c.meetings,
                teachers_simultaniously_teaching = c.teachers_simultaniously_teaching,
                contact_student = c.contact_student
              };

              _context.Course.Add(newCourse);
              _context.SaveChanges();

              _courses.Add(new CopyEntityIndex() { oldId = c.CourseID, newId = newCourse.CourseID });

              c.Workmethods.ForEach(wm => {
                CopyEntityIndex newWorkmethodId = _workmethods.FirstOrDefault(w => w.oldId.Equals(wm.WorkmethodID));
                  if (newWorkmethodId == null)
                    Console.WriteLine(JsonSerializer.Serialize(ExceptionResponse.Create("workmethod-not-found", $"New workmethod with old ID: {wm.WorkmethodID} not found")));

                CourseWorkmethods newCourseWorkmethod = new CourseWorkmethods() {
                  CourseID = newCourse.CourseID,
                  WorkmethodID = newWorkmethodId.newId
                };

                _context.CourseWorkmethods.Add(newCourseWorkmethod);
                _context.SaveChanges();
              });

              if (_teachers.Count > 0)
              {
                List<CourseOwner> oldCourseOwners = _context.CourseOwner.Where(co => co.CourseID.Equals(c.CourseID)).ToList();
                
                oldCourseOwners.ForEach(co => {
                  CopyEntityIndex newTeacherId = _teachers.FirstOrDefault(t => t.oldId.Equals(co.TeacherID));
                  if (newTeacherId == null)
                    Console.WriteLine(JsonSerializer.Serialize(ExceptionResponse.Create("teacher-not-found", $"New teacher with old ID: {co.TeacherID} not found")));

                  CourseOwner newCourseOwner = new CourseOwner()
                  {
                    TeacherID = newTeacherId.newId,
                    CourseID = newCourse.CourseID
                  };

                  _context.CourseOwner.Add(newCourseOwner);
                  _context.SaveChanges();
                });
              }

              if (_teams.Count > 0)
              {
                List<CourseTeams> courseTeams = _context.CourseTeams.Where(ct => ct.CourseID.Equals(c.CourseID)).ToList();

                courseTeams.ForEach(ct => {
                  CopyEntityIndex newTeamId = _teams.FirstOrDefault(t => t.oldId.Equals(ct.TeamID));
                  if (newTeamId == null)
                    Console.WriteLine(JsonSerializer.Serialize(ExceptionResponse.Create("team-not-found", $"New team with old ID: {ct.TeamID} not found")));

                  CourseTeams newCourseTeams = new CourseTeams()
                  {
                    CourseID = newCourse.CourseID,
                    TeamID = newTeamId.newId
                  };

                  _context.CourseTeams.Add(newCourseTeams);
                  _context.SaveChanges();
                });
              }
            });
          }

          if (new_curriculum.copy_entities.Any(ce => ce.Equals("Klassen")))
          {
            List<_Class> classes = _context._Class.Where(c => c.CurriculumID.Equals(ctcf)).ToList();

            classes.ForEach(c => {
              _Class newClass = new _Class()
              {
                CurriculumID = curriculum.CurriculumID,
                CurriculumKindID = c.CurriculumKindID,
                name = c.name,
                class_size_op1 = c.class_size_op1,
                class_size_op2 = c.class_size_op2,
                class_size_op3 = c.class_size_op3,
                class_size_op4 = c.class_size_op4
              };

              _context._Class.Add(newClass);
              _context.SaveChanges();
            });
          }

          if (new_curriculum.copy_entities.Any(ce => ce.Equals("MOT taken")))
          {
            List<MOT> motTasks = _context.MOT.Where(mot => mot.CurriculumID.Equals(ctcf)).ToList();

            motTasks.ForEach(m => {
              MOT newMOTTask = new MOT()
              {
                CurriculumID = curriculum.CurriculumID,
                type = m.type,
                name = m.name,
                hours = m.hours,
                active_op1 = m.active_op1,
                active_op2 = m.active_op2,
                active_op3 = m.active_op3,
                active_op4 = m.active_op4
              };

              _context.MOT.Add(newMOTTask);
              _context.SaveChanges();
            });
          }
        } else {
          // Workmethods
          List<Workmethod> new_workmethods = new List<Workmethod>();

          new_workmethods.Add(new Workmethod { CurriculumID = curriculum.CurriculumID, name = "Standaard" });
          new_workmethods.Add(new Workmethod { CurriculumID = curriculum.CurriculumID, name = "Theorie" });
          new_workmethods.Add(new Workmethod { CurriculumID = curriculum.CurriculumID, name = "Praktijk" });

          _context.Workmethod.AddRange(new_workmethods);
          _context.SaveChanges();

          // Positions
          List<Position> new_positions = new List<Position>();

          new_positions.Add(new Position { CurriculumID = curriculum.CurriculumID, name = "Instructeur" });
          new_positions.Add(new Position { CurriculumID = curriculum.CurriculumID, name = "Peercoach" });
          new_positions.Add(new Position { CurriculumID = curriculum.CurriculumID, name = "Trainee" });
          new_positions.Add(new Position { CurriculumID = curriculum.CurriculumID, name = "Docent" });
          new_positions.Add(new Position { CurriculumID = curriculum.CurriculumID, name = "Kerndocent" });
          new_positions.Add(new Position { CurriculumID = curriculum.CurriculumID, name = "Hogeschooldocent" });
          new_positions.Add(new Position { CurriculumID = curriculum.CurriculumID, name = "Hoofddocent" });
          new_positions.Add(new Position { CurriculumID = curriculum.CurriculumID, name = "Lector" });

          _context.Position.AddRange(new_positions);
          _context.SaveChanges();

          // Teams
          List<Team> new_teams = new List<Team>();

          new_teams.Add(new Team { CurriculumID = curriculum.CurriculumID, name = "Project" });
          new_teams.Add(new Team { CurriculumID = curriculum.CurriculumID, name = "SLC" });
          new_teams.Add(new Team { CurriculumID = curriculum.CurriculumID, name = "Skills" });
          new_teams.Add(new Team { CurriculumID = curriculum.CurriculumID, name = "Analyse" });
          new_teams.Add(new Team { CurriculumID = curriculum.CurriculumID, name = "Development" });
          new_teams.Add(new Team { CurriculumID = curriculum.CurriculumID, name = "Geen team" });

          _context.Team.AddRange(new_teams);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("availability-kinds")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetAllAvailabilityKinds()
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var availabilityKinds = _context.Education
          .Include(e => e.AvailabilityKinds)
          .FirstOrDefault(e => e.name.Equals(user.education))
          .AvailabilityKinds.Select(ak => new {
            id = ak.AvailabilityKindID,
            block_duration = ak.block_duration,
            block_amount = ak.block_amount
          });

        return Ok(availabilityKinds);
      } else {
        return Unauthorized();
      }
    }


    [HttpDelete("delete/{id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult DeleteCurriculum(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum curriculum = _context.Curriculum.FirstOrDefault(c => c.CurriculumID.Equals(id));
        if (curriculum == null)
          return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum with ID: {id} was not found."));
        
        _context.Curriculum.Remove(curriculum);

        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("delete-multiple")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult DeleteMultipleCurricula([FromBody] List<DeleteCurricula> curriculaToDelete)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        foreach (var curriculum in curriculaToDelete)
        {
          Curriculum curriculumToDelete = _context.Curriculum.FirstOrDefault(c => c.CurriculumID.Equals(curriculum.id));
          if (curriculaToDelete == null)
            return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum with ID: {curriculum.id} was not found."));
          
          _context.Curriculum.Remove(curriculumToDelete);
        }

        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{curriculum_id}/mot/create")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult CreateMOTTask(int curriculum_id, [FromBody] CreateMOTTask new_mot_task)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum curriculum = _context.Curriculum.FirstOrDefault(c => c.CurriculumID.Equals(curriculum_id));
        if (curriculum == null)
          return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum with ID: {curriculum_id} was not found."));

        MOT mot = new MOT() {
          CurriculumID = curriculum.CurriculumID,
          type = new_mot_task.type,
          name = new_mot_task.name,
          hours = new_mot_task.hours,
          active_op1 = new_mot_task.active_op1,
          active_op2 = new_mot_task.active_op2,
          active_op3 = new_mot_task.active_op3,
          active_op4 = new_mot_task.active_op4
        };
        
        _context.MOT.Add(mot);

        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{curriculum_id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult UpdateCurriculum(int curriculum_id, [FromBody] CurriculumDetailsWithMOTTasks new_curriculum)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum curriculum = _context.Curriculum.FirstOrDefault(c => c.CurriculumID.Equals(new_curriculum.id));
        if (curriculum == null)
          return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum with ID: {new_curriculum.id} was not found."));

        curriculum.name = new_curriculum.name;
        curriculum.default_fte = new_curriculum.default_fte;
        curriculum.default_fte_hours = new_curriculum.default_fte_hours;
        curriculum.default_di = new_curriculum.default_di;
        curriculum.default_organisational= new_curriculum.default_organisational;
        curriculum.default_professional = new_curriculum.default_professional;
        curriculum.course_formula_w = new_curriculum.course_formula_w;
        curriculum.course_formula_ct = new_curriculum.course_formula_ct;
        curriculum.course_formula_opslag = new_curriculum.course_formula_opslag;
        curriculum.course_formula_n = new_curriculum.course_formula_n;
        curriculum.course_formula_cor = new_curriculum.course_formula_cor;
        curriculum.course_formula_r = new_curriculum.course_formula_r;
        curriculum.start_date = new_curriculum.start_date;
        curriculum.end_date = new_curriculum.end_date;

        _context.Curriculum.Update(curriculum);

        foreach (var task in new_curriculum.mot_tasks)
        {
          MOT mot = _context.MOT.FirstOrDefault(m => m.MOTID.Equals(task.id));
          if (mot == null)
            return NotFound(ExceptionResponse.Create("mot-not-found", $"MOT with ID: {task.id} was not found."));

          mot.type = task.type;
          mot.name = task.name;
          mot.hours = task.hours;
          mot.active_op1 = task.active_op1;
          mot.active_op2 = task.active_op2;
          mot.active_op3 = task.active_op3;
          mot.active_op4 = task.active_op4;

          _context.MOT.Update(mot);
        }

        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{curriculum_id}/mot/delete-multiple")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult DeleteMOTTasks(int curriculum_id, [FromBody] List<DeleteMOTTask> MOTTasksToDelete)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        List<MOT> mots = _context.Curriculum.Include(c => c.MOTs).FirstOrDefault(c => c.CurriculumID.Equals(curriculum_id)).MOTs;
        if (mots.Count <= 0)
          return NotFound(ExceptionResponse.Create("mots-not-found", $"Curriculum with ID: {curriculum_id} has 0 MOT tasks."));

        mots.ForEach(task => {
          MOTTasksToDelete.ForEach(task_to_delete => {
            if (task.MOTID.Equals(task_to_delete.id))
            {
              _context.MOT.Remove(task);
            };
          });
        });

        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }
  }
  public class CopyEntityIndex
  {
    public int oldId { get; set; }
    public int newId { get; set; }
  }
}