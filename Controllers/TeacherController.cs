using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using ptd_system.Helpers;
using ptd_system.Models;
using System.Linq;
using System.Collections.Generic;
using ptd_system.Controllers.DTO;
using Microsoft.AspNetCore.Authorization;
using static ptd_system.Helpers.WorkloadCalculatorHelper;

namespace ptd_system.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class TeacherController : Controller
  {
    private readonly DatabaseContext _context;
    private readonly AuthenticationHelper _AuthenticationHelper;
    private readonly WorkloadCalculatorHelper _WorkloadCalculatorHelper;
    private readonly ExportHelper _ExportHelper;

    public TeacherController(DatabaseContext context)
    {
      _context = context;
      _AuthenticationHelper = new AuthenticationHelper(context);
      _WorkloadCalculatorHelper = new WorkloadCalculatorHelper(context);
      _ExportHelper = new ExportHelper(context);
    }

    [HttpGet("all/{id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetAllTeachers(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var teachers = _context.Education
          .Include(e => e.Curriculums)
            .ThenInclude(e => e.Teachers)
        .FirstOrDefault(e => e.name.Equals(user.education)).Curriculums
        .FirstOrDefault(c => c.CurriculumID.Equals(id))
        .Teachers.OrderBy(t => t.personnel_code)
        .Select(t => new {
          id = t.TeacherID,
          personnel_code = t.personnel_code
        }).ToList();

        return Ok(teachers);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("available-organisations")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetAvailableOrganisations()
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var educations = _context.Education.Select(e => new {
          id = e.EducationID,
          name = e.name
        }).ToList();

        return Ok(educations);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_id}/default-mot-tasks")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetDefaultMOTTasks(int curriculum_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var mot_tasks = _context.MOT
          .Where(mot => mot.CurriculumID.Equals(curriculum_id))
          .Select(mot => new {
            id = mot.MOTID,
            name = mot.name,
            hours = mot.hours
          })
          .ToList();

        return Ok(mot_tasks);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_id}/default-fte")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetDefaultFTE(int curriculum_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum curriculum = _context.Curriculum
          .FirstOrDefault(c => c.CurriculumID.Equals(curriculum_id));
        
        if (curriculum == null)
          return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum with ID: {curriculum_id} was not found."));

        return Ok(new { fte = curriculum.default_fte });
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_id}/default-di")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetDefaultDI(int curriculum_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum curriculum = _context.Curriculum
          .FirstOrDefault(c => c.CurriculumID.Equals(curriculum_id));
        
        if (curriculum == null)
          return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum with ID: {curriculum_id} was not found."));

        return Ok(new { di = curriculum.default_di });
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_id}/default-organisational")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetDefaultOrganisational(int curriculum_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum curriculum = _context.Curriculum
          .FirstOrDefault(c => c.CurriculumID.Equals(curriculum_id));
        
        if (curriculum == null)
          return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum with ID: {curriculum_id} was not found."));

        return Ok(new { organisational = curriculum.default_organisational });
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_id}/default-professional")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetDefaultProfessional(int curriculum_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Curriculum curriculum = _context.Curriculum
          .FirstOrDefault(c => c.CurriculumID.Equals(curriculum_id));
        
        if (curriculum == null)
          return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum with ID: {curriculum_id} was not found."));

        return Ok(new { professional = curriculum.default_professional });
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{curriculum_id}/create")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult CreateNewTeacher(int curriculum_id, [FromBody] NewTeacherDTO nt)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        if (!_context.Curriculum.Any(c => c.CurriculumID.Equals(curriculum_id)))
          return NotFound(ExceptionResponse.Create("curriculum-not-found", $"Curriculum with ID: {curriculum_id} was not found."));

        if (!_context.Position.Any(c => c.PositionID.Equals(nt.position.id)))
          return NotFound(ExceptionResponse.Create("position-not-found", $"Position with ID: {nt.position.id} was not found."));

        if (!_context.Team.Any(c => c.TeamID.Equals(nt.team.id)))
          return NotFound(ExceptionResponse.Create("team-not-found", $"Team with ID: {nt.team.id} was not found."));

        Teacher teacher = new Teacher()
        {
          CurriculumID = curriculum_id,
          PositionID = nt.position.id,
          personnel_code = nt.personnel_code,
          first_name = nt.first_name,
          last_name = nt.last_name,
          placeholder = nt.isPlaceholder
        };

        _context.Teacher.Add(teacher);
        _context.SaveChanges();

        FTE fte = new FTE()
        {
          TeacherID = teacher.TeacherID,
          contract = nt.contract,
          months_active = nt.months_active,
          fte_op1 = nt.fte_op1,
          fte_op2 = nt.fte_op2,
          fte_op3 = nt.fte_op3,
          fte_op4 = nt.fte_op4,
          organisational = nt.organisational,
          professional = nt.professional
        };

        DI di = new DI()
        {
          TeacherID = teacher.TeacherID,
          di_op1 = nt.di_op1,
          di_op2 = nt.di_op2,
          di_op3 = nt.di_op3,
          di_op4 = nt.di_op4,
        };

        _context.FTE.Add(fte);
        _context.DI.Add(di);
        _context.SaveChanges();

        TeacherTeams teacherTeams = new TeacherTeams()
        {
          TeamID = nt.team.id,
          TeacherID = teacher.TeacherID,
          is_team_leader = false,
        };

        _context.TeacherTeams.Add(teacherTeams);
        _context.SaveChanges();

        nt.mot_tasks.ForEach(t => {
          TeacherMOT teachermot = new TeacherMOT()
          {
            TeacherID = teacher.TeacherID,
            name = t.name,
            type = t.type,
            hours = t.hours,
            active_op1 = t.active_op1,
            active_op2 = t.active_op2,
            active_op3 = t.active_op3,
            active_op4 = t.active_op4
          };

          _context.TeacherMOT.Add(teachermot);
          _context.SaveChanges();
        });

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{id}/personnel_code")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetTeacherPersonnelCode(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher.Include(t => t.Curriculum).FirstOrDefault(t => t.TeacherID.Equals(id));
        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {id} was not found."));

        return Ok(new { name = teacher.personnel_code, curriculum = teacher.Curriculum.name });
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{id}/personalia")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetTeacherPersonalia(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher t = _context.Teacher
          .Include(t => t.Position)
          .Include(t => t.CourseOwnerOf)
            .ThenInclude(co => co.Course)
          .Include(t => t.Teams)
            .ThenInclude(t => t.Team)
          .Include(t => t.Curriculum)
            .ThenInclude(c => c.Education)
          .FirstOrDefault(t => t.TeacherID.Equals(id));
        if (t == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {id} was not found."));

        var teacherPersonalia = new {
          id = t.TeacherID,
          curriculum_id = t.CurriculumID,
          personnel_code = t.personnel_code,
          first_name = t.first_name,
          last_name = t.last_name,
          placeholder = t.placeholder,
          team = t.Teams.Any() ? new { id = t.Teams.First().Team.TeamID, name = t.Teams.First().Team.name } : new { id = -1, name = "" },
          is_teamleader_of_team = t.Teams.Any() ? false : t.Teams.First().is_team_leader,
          position = new { id = t.Position.PositionID, name = t.Position.name },
          courseowner = t.CourseOwnerOf.Select(co => new { id = co.Course.CourseID, code = co.Course.code }).ToList(),
          organisation = t.Curriculum.Education.name
        };

        return Ok(teacherPersonalia);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{id}/available-positions")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetPositionsForTeachers(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var positions = _context.Position
          .Where(p => p.CurriculumID.Equals(id))
          .Select(p => new {
            id = p.PositionID,
            name = p.name
          })
          .ToList();

        return Ok(positions);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_id}/available-courseowner")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetCoursesToOwnForTeachers(int curriculum_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var coursesToOwn = _context.Course
          .Where(c => c.CurriculumID.Equals(curriculum_id))
          .Select(c => new {
            id = c.CourseID,
            code = c.code
          })
          .ToList();

        return Ok(coursesToOwn);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_id}/available-teams")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetTeamsForTeachers(int curriculum_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        var teams = _context.Team
          .Where(c => c.CurriculumID.Equals(curriculum_id))
          .Select(c => new {
            id = c.TeamID,
            name = c.name
          })
          .ToList();

        return Ok(teams);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("update")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult UpdateTeacher([FromBody] TeacherDTO ut)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.Teams)
          .Include(t => t.Position)
          .FirstOrDefault(t => t.TeacherID.Equals(ut.id));
        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {ut.id} was not found."));

        teacher.personnel_code = ut.personnel_code;
        teacher.first_name = ut.first_name;
        teacher.last_name = ut.last_name;
        teacher.placeholder = ut.placeholder;
        teacher.PositionID = ut.position.id;
        
        _context.Teacher.Update(teacher);
        _context.SaveChanges();

        TeacherTeams oldTeacherTeams = teacher.Teams.First();
        if (oldTeacherTeams != null)
        {
          TeacherTeams newTeacherTeam = new TeacherTeams()
          {
            TeamID = ut.team.id,
            TeacherID = teacher.TeacherID
          };

          _context.TeacherTeams.Remove(oldTeacherTeams);
          _context.TeacherTeams.Add(newTeacherTeam);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("delete-multiple")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult DeleteMultipleTeachers([FromBody] List<DeleteTeachers> teachersToDelete)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        foreach (var teacherToDelete in teachersToDelete)
        {
          Teacher teacher = _context.Teacher.FirstOrDefault(c => c.TeacherID.Equals(teacherToDelete.id));
          if (teacher == null)
            return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher wiht ID: {teacherToDelete.id} was not found"));

          _context.Teacher.Remove(teacher);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpDelete("delete/{id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult DeleteTeacher(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .FirstOrDefault(t => t.TeacherID.Equals(id));
        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {id} was not found."));

        _context.Teacher.Remove(teacher);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{id}/notes")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetTeacherNotes(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.Notes)
          .Include(t => t.Curriculum)
          .FirstOrDefault(t => t.TeacherID.Equals(id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {id} was not found."));
        
        List<Teacher_NotesDTO> notes = teacher.Notes.OrderByDescending(n => n.date).Select(n => new Teacher_NotesDTO() {
          id = n.TeacherNoteID,
          curriculum_name = teacher.Curriculum.name,
          note = n.note,
          date = n.date
        }).ToList();

        return Ok(notes);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{id}/notes-from-previous-curricula")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetNotesFromPreviousCurricula(int id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        List<Teacher_NotesDTO> notes = new List<Teacher_NotesDTO>();

        Teacher teacher = _context.Teacher.FirstOrDefault(t => t.TeacherID.Equals(id));
        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {id} was not found."));

        List<Teacher> teachersFromPreviousCurricula = _context.Teacher
          .Include(t => t.Notes)
          .Include(t => t.Curriculum)
          .Where(t => t.personnel_code.Equals(teacher.personnel_code)).ToList();

        teachersFromPreviousCurricula.ForEach(t => {
          if (!t.TeacherID.Equals(teacher.TeacherID))
          {
            t.Notes.OrderByDescending(n => n.date).ToList().ForEach(n => {
              notes.Add(new Teacher_NotesDTO() {
                id = n.TeacherNoteID,
                curriculum_name = t.Curriculum.name,
                note = n.note,
                date = n.date
              });
            });
          }
        });

        return Ok(notes);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{id}/note")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult SaveTeacherNote(int id, [FromBody] Teacher_NewNotesDTO note)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.Notes)
          .FirstOrDefault(t => t.TeacherID.Equals(id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {id} was not found."));
        
        TeacherNote teacherNote = new TeacherNote() {
          TeacherID = teacher.TeacherID,
          note = note.note,
          date = DateTime.Now
        };

        _context.TeacherNote.Add(teacherNote);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/note/update/{note_id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult UpdateTeacherNote(int teacher_id, int note_id, [FromBody] Teacher_NewNotesDTO nn)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        TeacherNote teacherNote = _context.TeacherNote.FirstOrDefault(tn => tn.TeacherNoteID.Equals(note_id));
        if (teacherNote == null)
          return NotFound(ExceptionResponse.Create("note-not-found", $"Note with ID: {note_id} was not found."));

        teacherNote.note = nn.note;
        teacherNote.date = DateTime.Now;

        _context.TeacherNote.Update(teacherNote);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpDelete("{teacher_id}/note/delete/{note_id}")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult DeleteTeacherNote(int teacher_id, int note_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        TeacherNote teacherNote = _context.TeacherNote.FirstOrDefault(tn => tn.TeacherNoteID.Equals(note_id));
        if (teacherNote == null)
          return NotFound(ExceptionResponse.Create("note-not-found", $"Note with ID: {note_id} was not found."));

        _context.TeacherNote.Remove(teacherNote);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{teacher_id}/aanstelling")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetTeacherAanstelling(int teacher_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.Curriculum)
          .Include(t => t.FTE)
          .Include(t => t.DI)
          .Include(t => t.TeacherSecondments)
          .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {teacher_id} was not found."));

        TeacherAanstellingResponse res = new TeacherAanstellingResponse()
        {
          contract = teacher.FTE.contract,
          default_fte = teacher.Curriculum.default_fte,
          fte_op1 = teacher.FTE.fte_op1,
          fte_op2 = teacher.FTE.fte_op2,
          fte_op3 = teacher.FTE.fte_op3,
          fte_op4 = teacher.FTE.fte_op4,
          default_di = teacher.Curriculum.default_di,
          di_op1 = teacher.DI.di_op1,
          di_op2 = teacher.DI.di_op2,
          di_op3 = teacher.DI.di_op3,
          di_op4 = teacher.DI.di_op4,
          organisational = teacher.FTE.organisational,
          professional = teacher.FTE.professional,
          hours_subtracted_from_available_hours = teacher.FTE.hours_subtracted_from_available_hours,
          secondments = teacher.TeacherSecondments.Select(ts => new TeacherAanstellingSecondmentResponse {
            id = ts.TeacherSecondmentID,
            hours = ts.hours,
            reason = ts.reason,
            active_op1 = ts.active_op1,
            active_op2 = ts.active_op2,
            active_op3 = ts.active_op3,
            active_op4 = ts.active_op4
          }).ToList()
        };

        return Ok(res);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/aanstelling/update")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult UpdateTeacherAanstelling(int teacher_id, [FromBody] TeacherAanstellingRequest aanstelling)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.Curriculum)
          .Include(t => t.FTE)
          .Include(t => t.DI)
          .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {teacher_id} was not found."));

        teacher.FTE.contract = aanstelling.contract;
        teacher.FTE.fte_op1 = aanstelling.fte_op1;
        teacher.FTE.fte_op2 = aanstelling.fte_op2;
        teacher.FTE.fte_op3 = aanstelling.fte_op3;
        teacher.FTE.fte_op4 = aanstelling.fte_op4;
        teacher.DI.di_op1 = aanstelling.di_op1;
        teacher.DI.di_op2 = aanstelling.di_op2;
        teacher.DI.di_op3 = aanstelling.di_op3;
        teacher.DI.di_op4 = aanstelling.di_op4;
        teacher.FTE.organisational = aanstelling.organisational;
        teacher.FTE.professional = aanstelling.professional;
        teacher.FTE.hours_subtracted_from_available_hours = aanstelling.hours_subtracted_from_available_hours;

        _context.Teacher.Update(teacher);
        _context.SaveChanges();

        aanstelling.secondments.ForEach(ts => {
          TeacherSecondment teacherSecondment = _context.TeacherSecondment.FirstOrDefault(tese => tese.TeacherSecondmentID.Equals(ts.id));
          if (teacherSecondment == null)
            System.Console.WriteLine(ExceptionResponse.Create("teacher-secondment-not-found", $"Teacher secondment with ID: {ts.id} was not found."));
          
          teacherSecondment.hours = ts.hours;
          teacherSecondment.reason = ts.reason;
          teacherSecondment.active_op1 = ts.active_op1;
          teacherSecondment.active_op2 = ts.active_op2;
          teacherSecondment.active_op3 = ts.active_op3;
          teacherSecondment.active_op4 = ts.active_op4;

          _context.TeacherSecondment.Update(teacherSecondment);
          _context.SaveChanges();
        });

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{teacher_id}/taaktoedeling")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetTeacherTaaktoedeling(int teacher_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {teacher_id} was not found."));

        TeacherTaaktoedelingResponse res = _WorkloadCalculatorHelper.GetAvailableHoursForTaaktoedeling(teacher_id);

        return Ok(res);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/detachering")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult CreateNewDetachering(int teacher_id, [FromBody] TeacherNewDetacheringRequest newDetachering)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        TeacherSecondment teacherSecondment = new TeacherSecondment {
          TeacherID = teacher_id,
          hours = newDetachering.hours,
          reason = newDetachering.reason,
          active_op1 = newDetachering.active_op1,
          active_op2 = newDetachering.active_op2,
          active_op3 = newDetachering.active_op3,
          active_op4 = newDetachering.active_op4
        };

        _context.TeacherSecondment.Add(teacherSecondment);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/detachering/delete-multiple")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult DeleteMultipleDetachering([FromBody] List<DeleteTeachers> secondmentsToDelete)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        foreach (var secondmentToDelete in secondmentsToDelete)
        {
          TeacherSecondment teacherSecondment = _context.TeacherSecondment.FirstOrDefault(c => c.TeacherSecondmentID.Equals(secondmentToDelete.id));
          if (teacherSecondment == null)
            return NotFound(ExceptionResponse.Create("secondment-not-found", $"Secondment wiht ID: {secondmentToDelete.id} was not found"));

          _context.TeacherSecondment.Remove(teacherSecondment);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{teacher_id}/supervision")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetSupervision(int teacher_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.Supervisions)
          .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {teacher_id} was not found."));

        List<Supervision> inters = teacher.Supervisions
          .Where(s => s.supervision_kind.Equals("Stagiairs"))
          .ToList();

        List<Supervision> graduates = teacher.Supervisions
          .Where(s => s.supervision_kind.Equals("Afstudeerders"))
          .ToList();

        TeacherSupervisionResponse res = new TeacherSupervisionResponse {
          interns_summary = new TeacherSupervisionSummaryResponse {
            total = inters.Count(),
            op1 = inters.Aggregate(0, (total, next) => next.start_op.Equals(1) ? total + 1 : total) + 
              inters.Aggregate(0, (total, next) => next.end_op.Equals(1) ? total + 1 : total),
            op2 = inters.Aggregate(0, (total, next) => next.start_op.Equals(2) ? total + 1 : total) + 
              inters.Aggregate(0, (total, next) => next.end_op.Equals(2) ? total + 1 : total),
            op3 = inters.Aggregate(0, (total, next) => next.start_op.Equals(3) ? total + 1 : total) + 
              inters.Aggregate(0, (total, next) => next.end_op.Equals(3) ? total + 1 : total),
            op4 = inters.Aggregate(0, (total, next) => next.start_op.Equals(4) ? total + 1 : total) + 
              inters.Aggregate(0, (total, next) => next.end_op.Equals(4) ? total + 1 : total),
          },
          graduates_summary = new TeacherSupervisionSummaryResponse {
            total = graduates.Count(),
            op1 = graduates.Aggregate(0, (total, next) => next.start_op.Equals(1) ? total + 1 : total) + 
              graduates.Aggregate(0, (total, next) => next.end_op.Equals(1) ? total + 1 : total),
            op2 = graduates.Aggregate(0, (total, next) => next.start_op.Equals(2) ? total + 1 : total) + 
              graduates.Aggregate(0, (total, next) => next.end_op.Equals(2) ? total + 1 : total),
            op3 = graduates.Aggregate(0, (total, next) => next.start_op.Equals(3) ? total + 1 : total) + 
              graduates.Aggregate(0, (total, next) => next.end_op.Equals(3) ? total + 1 : total),
            op4 = graduates.Aggregate(0, (total, next) => next.start_op.Equals(4) ? total + 1 : total) + 
              graduates.Aggregate(0, (total, next) => next.end_op.Equals(4) ? total + 1 : total),
          },
          interns = teacher.Supervisions
            .Where(s => s.supervision_kind.Equals("Stagiairs"))
            .Select(s => new TeacherSupervisionObjectResponse {
              id = s.SupervisionID,
              start_op = s.start_op,
              end_op = s.end_op
            }).ToList(),
          graduates = teacher.Supervisions
            .Where(s => s.supervision_kind.Equals("Afstudeerders"))
            .Select(s => new TeacherSupervisionObjectResponse {
              id = s.SupervisionID,
              start_op = s.start_op,
              end_op = s.end_op
            }).ToList()
        };

        return Ok(res);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/supervision")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult CreateSupervision(int teacher_id, [FromBody] TeacherCreateSupervisionRequest newSupervision)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.Curriculum)
          .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {teacher_id} was not found."));

        Supervision supervision = new Supervision {
          CurriculumID = teacher.Curriculum.CurriculumID,
          TeacherID = teacher.TeacherID,
          supervision_kind = newSupervision.type,
          start_op = newSupervision.start_op,
          end_op = newSupervision.end_op
        };

        _context.Supervision.Add(supervision);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/supervision/update")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult UpdateSupervision(int teacher_id, [FromBody] List<TeacherUpdateSupervision> supervisionsToUpdate)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        foreach (var supervisionToUpdate in supervisionsToUpdate)
        {
          Supervision supervision = _context.Supervision.FirstOrDefault(c => c.SupervisionID.Equals(supervisionToUpdate.id));
          if (supervision == null)
            return NotFound(ExceptionResponse.Create("supervision-not-found", $"Supervision wiht ID: {supervisionToUpdate.id} was not found"));

          supervision.start_op = supervisionToUpdate.start_op;
          supervision.end_op = supervisionToUpdate.end_op;

          _context.Supervision.Update(supervision);
          _context.SaveChanges();
        }
        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/supervision/delete-multiple")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult DeleteMultipleSupervisions([FromBody] List<TeacherDeleteSupervisionRequest> supervisionsToDelete)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        foreach (var supervisionToDelete in supervisionsToDelete)
        {
          Supervision supervision = _context.Supervision.FirstOrDefault(c => c.SupervisionID.Equals(supervisionToDelete.id));
          if (supervision == null)
            return NotFound(ExceptionResponse.Create("supervision-not-found", $"Supervision wiht ID: {supervisionToDelete.id} was not found"));

          _context.Supervision.Remove(supervision);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{teacher_id}/onderwijstaken")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetOnderwijstaken(int teacher_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.Cells)
            .ThenInclude(tc => tc.Cell)
              .ThenInclude(c => c.Course)
                .ThenInclude(c => c.CurriculumKind)
          .Include(t => t.Cells)
            .ThenInclude(tc => tc.Cell)
              .ThenInclude(c => c.Course)
                .ThenInclude(c => c.Workmethods)
                  .ThenInclude(wm => wm.Workmethod)
          .Include(t => t.Cells)
            .ThenInclude(tc => tc.Cell)
              .ThenInclude(c => c.OP)
          .Include(t => t.Cells)
            .ThenInclude(tc => tc.Cell)
              .ThenInclude(c => c.Class)
          .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {teacher_id} was not found."));

        List<TeacherOnderwijsResponse> res = teacher.Cells
          .Select(c => new TeacherOnderwijsResponse {
            period = c.Cell.OP.period,
            course_code = c.Cell.Course.code,
            course_name = c.Cell.Course.name,
            curriculum_kind = c.Cell.Course.CurriculumKind.name,
            workmethod = c.Cell.Course.Workmethods.First().Workmethod.name,
            student_amount = c.Cell.OP.period.Equals(1)
              ? c.Cell.Class.class_size_op1
            : c.Cell.OP.period.Equals(2)
              ? c.Cell.Class.class_size_op2
            : c.Cell.OP.period.Equals(3)
              ? c.Cell.Class.class_size_op3
            : c.Cell.Class.class_size_op4,
            contact_student = c.Cell.Course.contact_student,
            meetings = c.Cell.Course.meetings,
            meeting_duration = c.Cell.Course.meeting_duration,
            raise_factor = c.Cell.Course.raise_factor,
            correction_time = c.Cell.Course.correction_time,
            travel_time = c.Cell.Course.travel_time
          }).ToList();

        return Ok(res);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{teacher_id}/mot")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetMOTTasks(int teacher_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.MOTs)
          .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {teacher_id} was not found."));

        List<TeacherMOTsResponse> res = teacher.MOTs
          .OrderBy(mot => mot.name)
          .Select(mot => new TeacherMOTsResponse {
            id = mot.TeacherMOTID,
            name = mot.name,
            hours = mot.hours,
            type = mot.type,
            active_op1 = mot.active_op1,
            active_op2 = mot.active_op2,
            active_op3 = mot.active_op3,
            active_op4 = mot.active_op4
          }).ToList();

        return Ok(res);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/mot/new")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult CreateNewMOTTask(int teacher_id, [FromBody] TeacherNewMOTTaskRequest newTask)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        TeacherMOT task = new TeacherMOT() {
          TeacherID = teacher_id,
          name = newTask.name,
          type = newTask.type,
          hours = newTask.hours,
          active_op1 = newTask.active_op1,
          active_op2 = newTask.active_op2,
          active_op3 = newTask.active_op3,
          active_op4 = newTask.active_op4
        };

        _context.TeacherMOT.Add(task);
        _context.SaveChanges();

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/mot/update")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult UpdateMOTTasks(int teacher_id, [FromBody] List<TeacherMOTsUpdateRequest> tasksToUpdate)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        foreach (TeacherMOTsUpdateRequest taskToUpdate in tasksToUpdate)
        {
          TeacherMOT task = _context.TeacherMOT.FirstOrDefault(t => t.TeacherMOTID.Equals(taskToUpdate.id));
          if (task == null)
            return NotFound(ExceptionResponse.Create("mot-task-not-found", $"MOT task wiht ID: {taskToUpdate.id} was not found"));

          task.name = taskToUpdate.name;
          task.type = taskToUpdate.type;
          task.hours = taskToUpdate.hours;
          task.active_op1 = taskToUpdate.active_op1;
          task.active_op2 = taskToUpdate.active_op2;
          task.active_op3 = taskToUpdate.active_op3;
          task.active_op4 = taskToUpdate.active_op4;

          _context.TeacherMOT.Update(task);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{teacher_id}/mot/options")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetMOTOptions(int teacher_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.Curriculum)
            .ThenInclude(c => c.MOTs)
          .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {teacher_id} was not found."));

        List<TeacherMOTOptionsResponse> res = teacher
          .Curriculum.MOTs.Select(m => new TeacherMOTOptionsResponse {
            id = m.MOTID,
            name = m.name,
            type = m.type,
            hours = m.hours,
            active_op1 = m.active_op1,
            active_op2 = m.active_op2,
            active_op3 = m.active_op3,
            active_op4 = m.active_op4
          })
          .OrderBy(mot => mot.name)
          .ToList();

        return Ok(res);
      } else {
        return Unauthorized();
      }
    }

    [HttpGet("{curriculum_id}/mot/new-teacher-options")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetMOTOptionsForNewTeacher(int curriculum_id)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        List<TeacherMOTOptionsResponse> mots = _context.Curriculum
          .Include(c => c.MOTs)
          .FirstOrDefault(c => c.CurriculumID.Equals(curriculum_id))
          .MOTs
          .Select(m => new TeacherMOTOptionsResponse {
            id = m.MOTID,
            name = m.name,
            type = m.type,
            hours = m.hours,
            active_op1 = m.active_op1,
            active_op2 = m.active_op2,
            active_op3 = m.active_op3,
            active_op4 = m.active_op4
          })
          .OrderBy(mot => mot.name)
          .ToList();

        return Ok(mots);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/mot/delete-multiple")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult GetMOTTasks(int teacher_id, [FromBody] List<TeacherMOTDeleteRequest> tasksToDelete)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        foreach (TeacherMOTDeleteRequest taskToDelete in tasksToDelete)
        {
          TeacherMOT task = _context.TeacherMOT.FirstOrDefault(t => t.TeacherMOTID.Equals(taskToDelete.id));
          if (task == null)
            return NotFound(ExceptionResponse.Create("mot-task-not-found", $"MOT task wiht ID: {taskToDelete.id} was not found"));

          _context.TeacherMOT.Remove(task);
          _context.SaveChanges();
        }

        return Ok();
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/export")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult Export(int teacher_id, [FromBody] ExportTeacherConfig config)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {teacher_id} was not found."));

        TeacherBase response = _ExportHelper.ExportTeacher(teacher_id, config);

        return Ok(response);
      } else {
        return Unauthorized();
      }
    }

    [HttpPost("{teacher_id}/export-onderwijstaken")]
    [Authorize(AuthenticationSchemes = "CWIPS", Policy = "teacher")]
    public IActionResult ExportOnderwijstaken(int teacher_id, [FromBody] Teacher_ExportOnderwijstakenRequest config)
    {
      AuthenticatedUser user = _AuthenticationHelper.ValidateUser(User);

      if (user.role.Equals("manager"))
      {
        Teacher teacher = _context.Teacher
          .Include(t => t.Cells)
            .ThenInclude(tc => tc.Cell)
              .ThenInclude(c => c.Course)
                .ThenInclude(c => c.CurriculumKind)
          .Include(t => t.Cells)
            .ThenInclude(tc => tc.Cell)
              .ThenInclude(c => c.Course)
                .ThenInclude(c => c.Workmethods)
                  .ThenInclude(wm => wm.Workmethod)
          .Include(t => t.Cells)
            .ThenInclude(tc => tc.Cell)
              .ThenInclude(c => c.OP)
          .Include(t => t.Cells)
            .ThenInclude(tc => tc.Cell)
              .ThenInclude(c => c.Class)
          .Include(t => t.Curriculum)
          .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

        if (teacher == null)
          return NotFound(ExceptionResponse.Create("teacher-not-found", $"Teacher with ID: {teacher_id} was not found."));

        List<TeacherOnderwijsResponse> onderwijstaken = teacher.Cells
          .Select(c => new TeacherOnderwijsResponse {
            period = c.Cell.OP.period,
            course_code = c.Cell.Course.code,
            course_name = c.Cell.Course.name,
            curriculum_kind = c.Cell.Course.CurriculumKind.name,
            workmethod = c.Cell.Course.Workmethods.First().Workmethod.name,
            student_amount = c.Cell.OP.period.Equals(1)
              ? c.Cell.Class.class_size_op1
            : c.Cell.OP.period.Equals(2)
              ? c.Cell.Class.class_size_op2
            : c.Cell.OP.period.Equals(3)
              ? c.Cell.Class.class_size_op3
            : c.Cell.Class.class_size_op4,
            contact_student = c.Cell.Course.contact_student,
            meetings = c.Cell.Course.meetings,
            meeting_duration = c.Cell.Course.meeting_duration,
            raise_factor = c.Cell.Course.raise_factor,
            correction_time = c.Cell.Course.correction_time,
            travel_time = c.Cell.Course.travel_time
          }).ToList();

        Teacher_ExportOnderwijstakenResponse response = new Teacher_ExportOnderwijstakenResponse {
            name = teacher.first_name,
            surname = teacher.last_name,
          personnel_code = teacher.personnel_code,
          curriculum = teacher.Curriculum.name,
          onderwijstaken = onderwijstaken
        };

        return Ok(response);
      } else {
        return Unauthorized();
      }
    }
  }
}