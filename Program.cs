using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using ptd_system.Models;
using Sentry.AspNetCore;

namespace ptd_system
{
  public class Program
  {
    public static void Main(string[] args)
    {
			IHost server = CreateHostBuilder(args).Build();
      CommandLineApplication app = new CommandLineApplication();

      app.HelpOption("-? | -h | --help");

      app.Command("seed", (command) =>
      {
        command.Description = "Seed the database";
        command.HelpOption("-? | -h | --help");

        command.OnExecute(() =>
        {
          using (IServiceScope scope = server.Services.CreateScope())
          {
            IServiceProvider services = scope.ServiceProvider;
            try
            {
              DatabaseContext context = services.GetRequiredService<DatabaseContext>();
              System.Console.WriteLine("Seeding database...");
              DatabaseSeeds.Initialize(context);
              System.Console.WriteLine("Database seeded.");
              return 0;
            }
            catch (Exception e)
            {
              System.Console.WriteLine("Error seeding database:");
              System.Console.WriteLine(e.ToString());
              return 1;
            }
          }
        });
      });

      app.OnExecute(() =>
      {
        using (IServiceScope scope = server.Services.CreateScope())
        {
          IServiceProvider services = scope.ServiceProvider;
          try
          {
            DatabaseContext context = services.GetRequiredService<DatabaseContext>();
            System.Console.WriteLine("Seeding database with default data...");
            DatabaseDefaultData.SeedData(context);
            System.Console.WriteLine("Database seeded with default data.");
          }
          catch (Exception e)
          {
            System.Console.WriteLine("Error seeding database with default data:");
            System.Console.WriteLine(e.ToString());
          }
        }

        server.Run();
        return 0;
      });

      app.Execute(args);

      // CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
          .ConfigureWebHostDefaults(webBuilder =>
          {
            webBuilder.UseSentry();
            webBuilder.UseStartup<Startup>();
          });
  }
}
