﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ptd_system.Migrations
{
    public partial class AddedTeacherSecondment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TeacherSecondment",
                columns: table => new
                {
                    TeacherSecondmentID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TeacherID = table.Column<int>(nullable: false),
                    hours = table.Column<float>(nullable: false),
                    reason = table.Column<string>(nullable: true),
                    active_op1 = table.Column<bool>(nullable: false),
                    active_op2 = table.Column<bool>(nullable: false),
                    active_op3 = table.Column<bool>(nullable: false),
                    active_op4 = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherSecondment", x => x.TeacherSecondmentID);
                    table.ForeignKey(
                        name: "FK_TeacherSecondment_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TeacherSecondment_TeacherID",
                table: "TeacherSecondment",
                column: "TeacherID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TeacherSecondment");
        }
    }
}
