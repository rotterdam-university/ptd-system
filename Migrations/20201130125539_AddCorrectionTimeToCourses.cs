﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ptd_system.Migrations
{
    public partial class AddCorrectionTimeToCourses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Course_CurriculumKind_CurriculumKindID",
                table: "Course");

            migrationBuilder.AlterColumn<int>(
                name: "CurriculumKindID",
                table: "Course",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddColumn<float>(
                name: "correction_time",
                table: "Course",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddForeignKey(
                name: "FK_Course_CurriculumKind_CurriculumKindID",
                table: "Course",
                column: "CurriculumKindID",
                principalTable: "CurriculumKind",
                principalColumn: "CurriculumKindID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Course_CurriculumKind_CurriculumKindID",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "correction_time",
                table: "Course");

            migrationBuilder.AlterColumn<int>(
                name: "CurriculumKindID",
                table: "Course",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Course_CurriculumKind_CurriculumKindID",
                table: "Course",
                column: "CurriculumKindID",
                principalTable: "CurriculumKind",
                principalColumn: "CurriculumKindID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
