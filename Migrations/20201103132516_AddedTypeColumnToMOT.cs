﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ptd_system.Migrations
{
    public partial class AddedTypeColumnToMOT : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "type",
                table: "TeacherMOT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "type",
                table: "MOT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "type",
                table: "TeacherMOT");

            migrationBuilder.DropColumn(
                name: "type",
                table: "MOT");
        }
    }
}
