﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ptd_system.Migrations
{
    public partial class MovedTeacherWorkloadOverrideToTeacherCells : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Team_Teacher_TeacherID",
                table: "Team");

            migrationBuilder.DropIndex(
                name: "IX_Team_TeacherID",
                table: "Team");

            migrationBuilder.DropColumn(
                name: "TeacherID",
                table: "Team");

            migrationBuilder.DropColumn(
                name: "teacher_workload_override",
                table: "Course");

            migrationBuilder.AddColumn<float>(
                name: "teacher_workload_override",
                table: "TeacherCells",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "teacher_workload_override",
                table: "TeacherCells");

            migrationBuilder.AddColumn<int>(
                name: "TeacherID",
                table: "Team",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<float>(
                name: "teacher_workload_override",
                table: "Course",
                type: "real",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.CreateIndex(
                name: "IX_Team_TeacherID",
                table: "Team",
                column: "TeacherID");

            migrationBuilder.AddForeignKey(
                name: "FK_Team_Teacher_TeacherID",
                table: "Team",
                column: "TeacherID",
                principalTable: "Teacher",
                principalColumn: "TeacherID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
