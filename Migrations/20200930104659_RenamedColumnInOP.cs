﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ptd_system.Migrations
{
    public partial class RenamedColumnInOP : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "name",
                table: "OP");

            migrationBuilder.AddColumn<int>(
                name: "period",
                table: "OP",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "period",
                table: "OP");

            migrationBuilder.AddColumn<string>(
                name: "name",
                table: "OP",
                type: "text",
                nullable: true);
        }
    }
}
