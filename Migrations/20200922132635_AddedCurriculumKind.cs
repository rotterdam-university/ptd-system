﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ptd_system.Migrations
{
    public partial class AddedCurriculumKind : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Course_CourseKind_CourseKindID",
                table: "Course");

            migrationBuilder.DropTable(
                name: "CourseKind");

            migrationBuilder.DropIndex(
                name: "IX_Course_CourseKindID",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "CourseKindID",
                table: "Course");

            migrationBuilder.AddColumn<int>(
                name: "CurriculumKindID",
                table: "Course",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CurriculumKindID",
                table: "_Class",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CurriculumKind",
                columns: table => new
                {
                    CurriculumKindID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EducationID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurriculumKind", x => x.CurriculumKindID);
                    table.ForeignKey(
                        name: "FK_CurriculumKind_Education_EducationID",
                        column: x => x.EducationID,
                        principalTable: "Education",
                        principalColumn: "EducationID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Course_CurriculumKindID",
                table: "Course",
                column: "CurriculumKindID");

            migrationBuilder.CreateIndex(
                name: "IX__Class_CurriculumKindID",
                table: "_Class",
                column: "CurriculumKindID");

            migrationBuilder.CreateIndex(
                name: "IX_CurriculumKind_EducationID",
                table: "CurriculumKind",
                column: "EducationID");

            migrationBuilder.AddForeignKey(
                name: "FK__Class_CurriculumKind_CurriculumKindID",
                table: "_Class",
                column: "CurriculumKindID",
                principalTable: "CurriculumKind",
                principalColumn: "CurriculumKindID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Course_CurriculumKind_CurriculumKindID",
                table: "Course",
                column: "CurriculumKindID",
                principalTable: "CurriculumKind",
                principalColumn: "CurriculumKindID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK__Class_CurriculumKind_CurriculumKindID",
                table: "_Class");

            migrationBuilder.DropForeignKey(
                name: "FK_Course_CurriculumKind_CurriculumKindID",
                table: "Course");

            migrationBuilder.DropTable(
                name: "CurriculumKind");

            migrationBuilder.DropIndex(
                name: "IX_Course_CurriculumKindID",
                table: "Course");

            migrationBuilder.DropIndex(
                name: "IX__Class_CurriculumKindID",
                table: "_Class");

            migrationBuilder.DropColumn(
                name: "CurriculumKindID",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "CurriculumKindID",
                table: "_Class");

            migrationBuilder.AddColumn<int>(
                name: "CourseKindID",
                table: "Course",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CourseKind",
                columns: table => new
                {
                    CourseKindID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseKind", x => x.CourseKindID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Course_CourseKindID",
                table: "Course",
                column: "CourseKindID");

            migrationBuilder.AddForeignKey(
                name: "FK_Course_CourseKind_CourseKindID",
                table: "Course",
                column: "CourseKindID",
                principalTable: "CourseKind",
                principalColumn: "CourseKindID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
