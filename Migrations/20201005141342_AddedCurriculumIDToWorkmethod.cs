﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ptd_system.Migrations
{
    public partial class AddedCurriculumIDToWorkmethod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurriculumID",
                table: "Workmethod",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Workmethod_CurriculumID",
                table: "Workmethod",
                column: "CurriculumID");

            migrationBuilder.AddForeignKey(
                name: "FK_Workmethod_Curriculum_CurriculumID",
                table: "Workmethod",
                column: "CurriculumID",
                principalTable: "Curriculum",
                principalColumn: "CurriculumID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Workmethod_Curriculum_CurriculumID",
                table: "Workmethod");

            migrationBuilder.DropIndex(
                name: "IX_Workmethod_CurriculumID",
                table: "Workmethod");

            migrationBuilder.DropColumn(
                name: "CurriculumID",
                table: "Workmethod");
        }
    }
}
