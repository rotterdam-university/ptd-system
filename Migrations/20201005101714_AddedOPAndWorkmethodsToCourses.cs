﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ptd_system.Migrations
{
    public partial class AddedOPAndWorkmethodsToCourses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OPID",
                table: "Course",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Workmethod",
                columns: table => new
                {
                    WorkmethodID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workmethod", x => x.WorkmethodID);
                });

            migrationBuilder.CreateTable(
                name: "CourseWorkmethods",
                columns: table => new
                {
                    CourseID = table.Column<int>(nullable: false),
                    WorkmethodID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseWorkmethods", x => new { x.CourseID, x.WorkmethodID });
                    table.ForeignKey(
                        name: "FK_CourseWorkmethods_Course_CourseID",
                        column: x => x.CourseID,
                        principalTable: "Course",
                        principalColumn: "CourseID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CourseWorkmethods_Workmethod_WorkmethodID",
                        column: x => x.WorkmethodID,
                        principalTable: "Workmethod",
                        principalColumn: "WorkmethodID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Course_OPID",
                table: "Course",
                column: "OPID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseWorkmethods_WorkmethodID",
                table: "CourseWorkmethods",
                column: "WorkmethodID");

            migrationBuilder.AddForeignKey(
                name: "FK_Course_OP_OPID",
                table: "Course",
                column: "OPID",
                principalTable: "OP",
                principalColumn: "OPID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Course_OP_OPID",
                table: "Course");

            migrationBuilder.DropTable(
                name: "CourseWorkmethods");

            migrationBuilder.DropTable(
                name: "Workmethod");

            migrationBuilder.DropIndex(
                name: "IX_Course_OPID",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "OPID",
                table: "Course");
        }
    }
}
