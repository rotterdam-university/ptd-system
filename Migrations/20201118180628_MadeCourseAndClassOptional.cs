﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ptd_system.Migrations
{
    public partial class MadeCourseAndClassOptional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cell__Class_ClassID",
                table: "Cell");

            migrationBuilder.DropForeignKey(
                name: "FK_Cell_Course_CourseID",
                table: "Cell");

            migrationBuilder.AlterColumn<int>(
                name: "CourseID",
                table: "Cell",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "ClassID",
                table: "Cell",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Cell__Class_ClassID",
                table: "Cell",
                column: "ClassID",
                principalTable: "_Class",
                principalColumn: "ClassID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cell_Course_CourseID",
                table: "Cell",
                column: "CourseID",
                principalTable: "Course",
                principalColumn: "CourseID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cell__Class_ClassID",
                table: "Cell");

            migrationBuilder.DropForeignKey(
                name: "FK_Cell_Course_CourseID",
                table: "Cell");

            migrationBuilder.AlterColumn<int>(
                name: "CourseID",
                table: "Cell",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ClassID",
                table: "Cell",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Cell__Class_ClassID",
                table: "Cell",
                column: "ClassID",
                principalTable: "_Class",
                principalColumn: "ClassID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cell_Course_CourseID",
                table: "Cell",
                column: "CourseID",
                principalTable: "Course",
                principalColumn: "CourseID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
