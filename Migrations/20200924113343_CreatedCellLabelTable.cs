﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ptd_system.Migrations
{
    public partial class CreatedCellLabelTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cell_Label_LabelID",
                table: "Cell");

            migrationBuilder.DropIndex(
                name: "IX_Cell_LabelID",
                table: "Cell");

            migrationBuilder.DropColumn(
                name: "LabelID",
                table: "Cell");

            migrationBuilder.AddColumn<int>(
                name: "CellLabelID",
                table: "Cell",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CellLabel",
                columns: table => new
                {
                    CellLabelID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(nullable: true),
                    color = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CellLabel", x => x.CellLabelID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cell_CellLabelID",
                table: "Cell",
                column: "CellLabelID");

            migrationBuilder.AddForeignKey(
                name: "FK_Cell_CellLabel_CellLabelID",
                table: "Cell",
                column: "CellLabelID",
                principalTable: "CellLabel",
                principalColumn: "CellLabelID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cell_CellLabel_CellLabelID",
                table: "Cell");

            migrationBuilder.DropTable(
                name: "CellLabel");

            migrationBuilder.DropIndex(
                name: "IX_Cell_CellLabelID",
                table: "Cell");

            migrationBuilder.DropColumn(
                name: "CellLabelID",
                table: "Cell");

            migrationBuilder.AddColumn<int>(
                name: "LabelID",
                table: "Cell",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Cell_LabelID",
                table: "Cell",
                column: "LabelID");

            migrationBuilder.AddForeignKey(
                name: "FK_Cell_Label_LabelID",
                table: "Cell",
                column: "LabelID",
                principalTable: "Label",
                principalColumn: "LabelID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
