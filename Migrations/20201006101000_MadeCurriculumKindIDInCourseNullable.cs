﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ptd_system.Migrations
{
    public partial class MadeCurriculumKindIDInCourseNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Course_CurriculumKind_CurriculumKindID",
                table: "Course");

            migrationBuilder.AlterColumn<int>(
                name: "CurriculumKindID",
                table: "Course",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Course_CurriculumKind_CurriculumKindID",
                table: "Course",
                column: "CurriculumKindID",
                principalTable: "CurriculumKind",
                principalColumn: "CurriculumKindID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Course_CurriculumKind_CurriculumKindID",
                table: "Course");

            migrationBuilder.AlterColumn<int>(
                name: "CurriculumKindID",
                table: "Course",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Course_CurriculumKind_CurriculumKindID",
                table: "Course",
                column: "CurriculumKindID",
                principalTable: "CurriculumKind",
                principalColumn: "CurriculumKindID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
