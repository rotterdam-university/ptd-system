﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ptd_system.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CellNote",
                columns: table => new
                {
                    CellNoteID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CellNote", x => x.CellNoteID);
                });

            migrationBuilder.CreateTable(
                name: "CourseKind",
                columns: table => new
                {
                    CourseKindID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseKind", x => x.CourseKindID);
                });

            migrationBuilder.CreateTable(
                name: "Institute",
                columns: table => new
                {
                    InstituteID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Institute", x => x.InstituteID);
                });

            migrationBuilder.CreateTable(
                name: "Education",
                columns: table => new
                {
                    EducationID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    InstituteID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    default_curr_start_date = table.Column<DateTime>(nullable: false),
                    default_curr_end_date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Education", x => x.EducationID);
                    table.ForeignKey(
                        name: "FK_Education_Institute_InstituteID",
                        column: x => x.InstituteID,
                        principalTable: "Institute",
                        principalColumn: "InstituteID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AvailabilityKind",
                columns: table => new
                {
                    AvailabilityKindID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EducationID = table.Column<int>(nullable: false),
                    block_duration = table.Column<float>(nullable: false),
                    block_amount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AvailabilityKind", x => x.AvailabilityKindID);
                    table.ForeignKey(
                        name: "FK_AvailabilityKind_Education_EducationID",
                        column: x => x.EducationID,
                        principalTable: "Education",
                        principalColumn: "EducationID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Label",
                columns: table => new
                {
                    LabelID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EducationID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    color = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Label", x => x.LabelID);
                    table.ForeignKey(
                        name: "FK_Label_Education_EducationID",
                        column: x => x.EducationID,
                        principalTable: "Education",
                        principalColumn: "EducationID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Manager",
                columns: table => new
                {
                    ManagerID = table.Column<int>(nullable: false),
                    EducationID = table.Column<int>(nullable: false),
                    code = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Manager", x => new { x.ManagerID, x.EducationID });
                    table.ForeignKey(
                        name: "FK_Manager_Education_EducationID",
                        column: x => x.EducationID,
                        principalTable: "Education",
                        principalColumn: "EducationID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Curriculum",
                columns: table => new
                {
                    CurriculumID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    EducationID = table.Column<int>(nullable: false),
                    AvailabilityKindID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    start_date = table.Column<DateTime>(nullable: false),
                    end_date = table.Column<DateTime>(nullable: false),
                    visibility_for_teachers = table.Column<bool>(nullable: false),
                    default_fte = table.Column<float>(nullable: false),
                    default_fte_hours = table.Column<float>(nullable: false),
                    default_di = table.Column<float>(nullable: false),
                    course_formula_w = table.Column<float>(nullable: false),
                    course_formula_ct = table.Column<float>(nullable: false),
                    course_formula_opslag = table.Column<float>(nullable: false),
                    course_formula_n = table.Column<float>(nullable: false),
                    course_formula_cor = table.Column<float>(nullable: false),
                    course_formula_r = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Curriculum", x => x.CurriculumID);
                    table.ForeignKey(
                        name: "FK_Curriculum_AvailabilityKind_AvailabilityKindID",
                        column: x => x.AvailabilityKindID,
                        principalTable: "AvailabilityKind",
                        principalColumn: "AvailabilityKindID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Curriculum_Education_EducationID",
                        column: x => x.EducationID,
                        principalTable: "Education",
                        principalColumn: "EducationID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "_Class",
                columns: table => new
                {
                    ClassID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CurriculumID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    class_size_op1 = table.Column<int>(nullable: false),
                    class_size_op2 = table.Column<int>(nullable: false),
                    class_size_op3 = table.Column<int>(nullable: false),
                    class_size_op4 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Class", x => x.ClassID);
                    table.ForeignKey(
                        name: "FK__Class_Curriculum_CurriculumID",
                        column: x => x.CurriculumID,
                        principalTable: "Curriculum",
                        principalColumn: "CurriculumID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Course",
                columns: table => new
                {
                    CourseID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CurriculumID = table.Column<int>(nullable: false),
                    CourseKindID = table.Column<int>(nullable: false),
                    code = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    ects = table.Column<int>(nullable: false),
                    raise_factor = table.Column<float>(nullable: false),
                    meetings = table.Column<int>(nullable: false),
                    meeting_duration = table.Column<float>(nullable: false),
                    teachers_simultaniously_teaching = table.Column<int>(nullable: false),
                    contact_student = table.Column<float>(nullable: false),
                    teached_on_day = table.Column<int>(nullable: false),
                    teacher_workload_override = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Course", x => x.CourseID);
                    table.ForeignKey(
                        name: "FK_Course_CourseKind_CourseKindID",
                        column: x => x.CourseKindID,
                        principalTable: "CourseKind",
                        principalColumn: "CourseKindID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Course_Curriculum_CurriculumID",
                        column: x => x.CurriculumID,
                        principalTable: "Curriculum",
                        principalColumn: "CurriculumID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MOT",
                columns: table => new
                {
                    MOTID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CurriculumID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    hours = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MOT", x => x.MOTID);
                    table.ForeignKey(
                        name: "FK_MOT_Curriculum_CurriculumID",
                        column: x => x.CurriculumID,
                        principalTable: "Curriculum",
                        principalColumn: "CurriculumID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Position",
                columns: table => new
                {
                    PositionID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CurriculumID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Position", x => x.PositionID);
                    table.ForeignKey(
                        name: "FK_Position_Curriculum_CurriculumID",
                        column: x => x.CurriculumID,
                        principalTable: "Curriculum",
                        principalColumn: "CurriculumID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudyYear",
                columns: table => new
                {
                    StudyYearID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CurriculumID = table.Column<int>(nullable: false),
                    year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudyYear", x => x.StudyYearID);
                    table.ForeignKey(
                        name: "FK_StudyYear_Curriculum_CurriculumID",
                        column: x => x.CurriculumID,
                        principalTable: "Curriculum",
                        principalColumn: "CurriculumID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Teacher",
                columns: table => new
                {
                    TeacherID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CurriculumID = table.Column<int>(nullable: false),
                    PositionID = table.Column<int>(nullable: false),
                    personnel_code = table.Column<string>(nullable: true),
                    first_name = table.Column<string>(nullable: true),
                    last_name = table.Column<string>(nullable: true),
                    placeholder = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teacher", x => x.TeacherID);
                    table.ForeignKey(
                        name: "FK_Teacher_Curriculum_CurriculumID",
                        column: x => x.CurriculumID,
                        principalTable: "Curriculum",
                        principalColumn: "CurriculumID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Teacher_Position_PositionID",
                        column: x => x.PositionID,
                        principalTable: "Position",
                        principalColumn: "PositionID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OP",
                columns: table => new
                {
                    OPID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    StudyYearID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OP", x => x.OPID);
                    table.ForeignKey(
                        name: "FK_OP_StudyYear_StudyYearID",
                        column: x => x.StudyYearID,
                        principalTable: "StudyYear",
                        principalColumn: "StudyYearID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CourseOwner",
                columns: table => new
                {
                    TeacherID = table.Column<int>(nullable: false),
                    CourseID = table.Column<int>(nullable: false),
                    CourseOwnerID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseOwner", x => new { x.TeacherID, x.CourseID });
                    table.ForeignKey(
                        name: "FK_CourseOwner_Course_CourseID",
                        column: x => x.CourseID,
                        principalTable: "Course",
                        principalColumn: "CourseID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CourseOwner_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DI",
                columns: table => new
                {
                    DIID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TeacherID = table.Column<int>(nullable: false),
                    di_op1 = table.Column<float>(nullable: false),
                    di_op2 = table.Column<float>(nullable: false),
                    di_op3 = table.Column<float>(nullable: false),
                    di_op4 = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DI", x => x.DIID);
                    table.ForeignKey(
                        name: "FK_DI_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FTE",
                columns: table => new
                {
                    FTEID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TeacherID = table.Column<int>(nullable: false),
                    contract = table.Column<string>(nullable: true),
                    fte_op1 = table.Column<float>(nullable: false),
                    fte_op2 = table.Column<float>(nullable: false),
                    fte_op3 = table.Column<float>(nullable: false),
                    fte_op4 = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FTE", x => x.FTEID);
                    table.ForeignKey(
                        name: "FK_FTE_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Supervision",
                columns: table => new
                {
                    SupervisionID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CurriculumID = table.Column<int>(nullable: false),
                    TeacherID = table.Column<int>(nullable: false),
                    supervision_kind = table.Column<string>(nullable: true),
                    start_op = table.Column<int>(nullable: false),
                    end_op = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supervision", x => x.SupervisionID);
                    table.ForeignKey(
                        name: "FK_Supervision_Curriculum_CurriculumID",
                        column: x => x.CurriculumID,
                        principalTable: "Curriculum",
                        principalColumn: "CurriculumID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Supervision_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherAvailability",
                columns: table => new
                {
                    TeacherAvailabilityID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TeacherID = table.Column<int>(nullable: false),
                    AvailabilityKindID = table.Column<int>(nullable: false),
                    day = table.Column<int>(nullable: false),
                    block = table.Column<int>(nullable: false),
                    last_updated = table.Column<DateTime>(nullable: false),
                    definitive = table.Column<bool>(nullable: false),
                    available = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherAvailability", x => x.TeacherAvailabilityID);
                    table.ForeignKey(
                        name: "FK_TeacherAvailability_AvailabilityKind_AvailabilityKindID",
                        column: x => x.AvailabilityKindID,
                        principalTable: "AvailabilityKind",
                        principalColumn: "AvailabilityKindID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherAvailability_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherLeave",
                columns: table => new
                {
                    TeacherLeaveID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TeacherID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    hours_override = table.Column<float>(nullable: false),
                    start_date = table.Column<DateTime>(nullable: false),
                    end_date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherLeave", x => x.TeacherLeaveID);
                    table.ForeignKey(
                        name: "FK_TeacherLeave_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherMOT",
                columns: table => new
                {
                    TeacherMOTID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TeacherID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    hours = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherMOT", x => x.TeacherMOTID);
                    table.ForeignKey(
                        name: "FK_TeacherMOT_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherNote",
                columns: table => new
                {
                    TeacherNoteID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TeacherID = table.Column<int>(nullable: false),
                    note = table.Column<string>(nullable: true),
                    date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherNote", x => x.TeacherNoteID);
                    table.ForeignKey(
                        name: "FK_TeacherNote_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherPreference",
                columns: table => new
                {
                    TeacherPreferenceID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TeacherID = table.Column<int>(nullable: false),
                    CourseID = table.Column<int>(nullable: false),
                    last_updated = table.Column<DateTime>(nullable: false),
                    definitive = table.Column<bool>(nullable: false),
                    not_preferred = table.Column<bool>(nullable: false),
                    no_preference = table.Column<bool>(nullable: false),
                    preferred = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherPreference", x => x.TeacherPreferenceID);
                    table.ForeignKey(
                        name: "FK_TeacherPreference_Course_CourseID",
                        column: x => x.CourseID,
                        principalTable: "Course",
                        principalColumn: "CourseID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherPreference_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Team",
                columns: table => new
                {
                    TeamID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CurriculumID = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    CourseID = table.Column<int>(nullable: true),
                    TeacherID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Team", x => x.TeamID);
                    table.ForeignKey(
                        name: "FK_Team_Course_CourseID",
                        column: x => x.CourseID,
                        principalTable: "Course",
                        principalColumn: "CourseID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Team_Curriculum_CurriculumID",
                        column: x => x.CurriculumID,
                        principalTable: "Curriculum",
                        principalColumn: "CurriculumID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Team_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Cell",
                columns: table => new
                {
                    CellID = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    OPID = table.Column<int>(nullable: false),
                    CourseID = table.Column<int>(nullable: false),
                    ClassID = table.Column<int>(nullable: false),
                    CellNoteID = table.Column<int>(nullable: false),
                    LabelID = table.Column<int>(nullable: false),
                    x_position = table.Column<int>(nullable: false),
                    y_position = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cell", x => x.CellID);
                    table.ForeignKey(
                        name: "FK_Cell_CellNote_CellNoteID",
                        column: x => x.CellNoteID,
                        principalTable: "CellNote",
                        principalColumn: "CellNoteID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cell__Class_ClassID",
                        column: x => x.ClassID,
                        principalTable: "_Class",
                        principalColumn: "ClassID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cell_Course_CourseID",
                        column: x => x.CourseID,
                        principalTable: "Course",
                        principalColumn: "CourseID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cell_Label_LabelID",
                        column: x => x.LabelID,
                        principalTable: "Label",
                        principalColumn: "LabelID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cell_OP_OPID",
                        column: x => x.OPID,
                        principalTable: "OP",
                        principalColumn: "OPID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CourseTeams",
                columns: table => new
                {
                    CourseID = table.Column<int>(nullable: false),
                    TeamID = table.Column<int>(nullable: false),
                    CourseTeamsID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseTeams", x => new { x.CourseID, x.TeamID });
                    table.ForeignKey(
                        name: "FK_CourseTeams_Course_CourseID",
                        column: x => x.CourseID,
                        principalTable: "Course",
                        principalColumn: "CourseID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CourseTeams_Team_TeamID",
                        column: x => x.TeamID,
                        principalTable: "Team",
                        principalColumn: "TeamID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherTeams",
                columns: table => new
                {
                    TeacherID = table.Column<int>(nullable: false),
                    TeamID = table.Column<int>(nullable: false),
                    TeacherTeamsID = table.Column<int>(nullable: false),
                    is_team_leader = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherTeams", x => new { x.TeacherID, x.TeamID });
                    table.ForeignKey(
                        name: "FK_TeacherTeams_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherTeams_Team_TeamID",
                        column: x => x.TeamID,
                        principalTable: "Team",
                        principalColumn: "TeamID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherCells",
                columns: table => new
                {
                    TeacherID = table.Column<int>(nullable: false),
                    CellID = table.Column<int>(nullable: false),
                    TeacherCellsID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherCells", x => new { x.CellID, x.TeacherID });
                    table.ForeignKey(
                        name: "FK_TeacherCells_Cell_CellID",
                        column: x => x.CellID,
                        principalTable: "Cell",
                        principalColumn: "CellID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherCells_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "TeacherID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX__Class_CurriculumID",
                table: "_Class",
                column: "CurriculumID");

            migrationBuilder.CreateIndex(
                name: "IX_AvailabilityKind_EducationID",
                table: "AvailabilityKind",
                column: "EducationID");

            migrationBuilder.CreateIndex(
                name: "IX_Cell_CellNoteID",
                table: "Cell",
                column: "CellNoteID");

            migrationBuilder.CreateIndex(
                name: "IX_Cell_ClassID",
                table: "Cell",
                column: "ClassID");

            migrationBuilder.CreateIndex(
                name: "IX_Cell_CourseID",
                table: "Cell",
                column: "CourseID");

            migrationBuilder.CreateIndex(
                name: "IX_Cell_LabelID",
                table: "Cell",
                column: "LabelID");

            migrationBuilder.CreateIndex(
                name: "IX_Cell_OPID",
                table: "Cell",
                column: "OPID");

            migrationBuilder.CreateIndex(
                name: "IX_Course_CourseKindID",
                table: "Course",
                column: "CourseKindID");

            migrationBuilder.CreateIndex(
                name: "IX_Course_CurriculumID",
                table: "Course",
                column: "CurriculumID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseOwner_CourseID",
                table: "CourseOwner",
                column: "CourseID");

            migrationBuilder.CreateIndex(
                name: "IX_CourseTeams_TeamID",
                table: "CourseTeams",
                column: "TeamID");

            migrationBuilder.CreateIndex(
                name: "IX_Curriculum_AvailabilityKindID",
                table: "Curriculum",
                column: "AvailabilityKindID");

            migrationBuilder.CreateIndex(
                name: "IX_Curriculum_EducationID",
                table: "Curriculum",
                column: "EducationID");

            migrationBuilder.CreateIndex(
                name: "IX_DI_TeacherID",
                table: "DI",
                column: "TeacherID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Education_InstituteID",
                table: "Education",
                column: "InstituteID");

            migrationBuilder.CreateIndex(
                name: "IX_FTE_TeacherID",
                table: "FTE",
                column: "TeacherID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Label_EducationID",
                table: "Label",
                column: "EducationID");

            migrationBuilder.CreateIndex(
                name: "IX_Manager_EducationID",
                table: "Manager",
                column: "EducationID");

            migrationBuilder.CreateIndex(
                name: "IX_MOT_CurriculumID",
                table: "MOT",
                column: "CurriculumID");

            migrationBuilder.CreateIndex(
                name: "IX_OP_StudyYearID",
                table: "OP",
                column: "StudyYearID");

            migrationBuilder.CreateIndex(
                name: "IX_Position_CurriculumID",
                table: "Position",
                column: "CurriculumID");

            migrationBuilder.CreateIndex(
                name: "IX_StudyYear_CurriculumID",
                table: "StudyYear",
                column: "CurriculumID");

            migrationBuilder.CreateIndex(
                name: "IX_Supervision_CurriculumID",
                table: "Supervision",
                column: "CurriculumID");

            migrationBuilder.CreateIndex(
                name: "IX_Supervision_TeacherID",
                table: "Supervision",
                column: "TeacherID");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_CurriculumID",
                table: "Teacher",
                column: "CurriculumID");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_PositionID",
                table: "Teacher",
                column: "PositionID");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherAvailability_AvailabilityKindID",
                table: "TeacherAvailability",
                column: "AvailabilityKindID");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherAvailability_TeacherID",
                table: "TeacherAvailability",
                column: "TeacherID");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherCells_TeacherID",
                table: "TeacherCells",
                column: "TeacherID");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherLeave_TeacherID",
                table: "TeacherLeave",
                column: "TeacherID");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherMOT_TeacherID",
                table: "TeacherMOT",
                column: "TeacherID");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherNote_TeacherID",
                table: "TeacherNote",
                column: "TeacherID");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherPreference_CourseID",
                table: "TeacherPreference",
                column: "CourseID");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherPreference_TeacherID",
                table: "TeacherPreference",
                column: "TeacherID");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherTeams_TeamID",
                table: "TeacherTeams",
                column: "TeamID");

            migrationBuilder.CreateIndex(
                name: "IX_Team_CourseID",
                table: "Team",
                column: "CourseID");

            migrationBuilder.CreateIndex(
                name: "IX_Team_CurriculumID",
                table: "Team",
                column: "CurriculumID");

            migrationBuilder.CreateIndex(
                name: "IX_Team_TeacherID",
                table: "Team",
                column: "TeacherID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CourseOwner");

            migrationBuilder.DropTable(
                name: "CourseTeams");

            migrationBuilder.DropTable(
                name: "DI");

            migrationBuilder.DropTable(
                name: "FTE");

            migrationBuilder.DropTable(
                name: "Manager");

            migrationBuilder.DropTable(
                name: "MOT");

            migrationBuilder.DropTable(
                name: "Supervision");

            migrationBuilder.DropTable(
                name: "TeacherAvailability");

            migrationBuilder.DropTable(
                name: "TeacherCells");

            migrationBuilder.DropTable(
                name: "TeacherLeave");

            migrationBuilder.DropTable(
                name: "TeacherMOT");

            migrationBuilder.DropTable(
                name: "TeacherNote");

            migrationBuilder.DropTable(
                name: "TeacherPreference");

            migrationBuilder.DropTable(
                name: "TeacherTeams");

            migrationBuilder.DropTable(
                name: "Cell");

            migrationBuilder.DropTable(
                name: "Team");

            migrationBuilder.DropTable(
                name: "CellNote");

            migrationBuilder.DropTable(
                name: "_Class");

            migrationBuilder.DropTable(
                name: "Label");

            migrationBuilder.DropTable(
                name: "OP");

            migrationBuilder.DropTable(
                name: "Course");

            migrationBuilder.DropTable(
                name: "Teacher");

            migrationBuilder.DropTable(
                name: "StudyYear");

            migrationBuilder.DropTable(
                name: "CourseKind");

            migrationBuilder.DropTable(
                name: "Position");

            migrationBuilder.DropTable(
                name: "Curriculum");

            migrationBuilder.DropTable(
                name: "AvailabilityKind");

            migrationBuilder.DropTable(
                name: "Education");

            migrationBuilder.DropTable(
                name: "Institute");
        }
    }
}
