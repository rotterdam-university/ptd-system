﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ptd_system.Migrations
{
    public partial class AddedActiveInOPFORMOT : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "teached_on_day",
                table: "Course");

            migrationBuilder.AddColumn<bool>(
                name: "active_op1",
                table: "TeacherMOT",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "active_op2",
                table: "TeacherMOT",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "active_op3",
                table: "TeacherMOT",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "active_op4",
                table: "TeacherMOT",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "active_op1",
                table: "MOT",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "active_op2",
                table: "MOT",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "active_op3",
                table: "MOT",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "active_op4",
                table: "MOT",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "teached_on_friday",
                table: "Course",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "teached_on_monday",
                table: "Course",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "teached_on_thursday",
                table: "Course",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "teached_on_tuesday",
                table: "Course",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "teached_on_wednesday",
                table: "Course",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "active_op1",
                table: "TeacherMOT");

            migrationBuilder.DropColumn(
                name: "active_op2",
                table: "TeacherMOT");

            migrationBuilder.DropColumn(
                name: "active_op3",
                table: "TeacherMOT");

            migrationBuilder.DropColumn(
                name: "active_op4",
                table: "TeacherMOT");

            migrationBuilder.DropColumn(
                name: "active_op1",
                table: "MOT");

            migrationBuilder.DropColumn(
                name: "active_op2",
                table: "MOT");

            migrationBuilder.DropColumn(
                name: "active_op3",
                table: "MOT");

            migrationBuilder.DropColumn(
                name: "active_op4",
                table: "MOT");

            migrationBuilder.DropColumn(
                name: "teached_on_friday",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "teached_on_monday",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "teached_on_thursday",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "teached_on_tuesday",
                table: "Course");

            migrationBuilder.DropColumn(
                name: "teached_on_wednesday",
                table: "Course");

            migrationBuilder.AddColumn<int>(
                name: "teached_on_day",
                table: "Course",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
