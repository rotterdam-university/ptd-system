﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ptd_system.Migrations
{
    public partial class MadeCellNoteAndLabelNullable2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cell_CellLabel_CellLabelID",
                table: "Cell");

            migrationBuilder.DropForeignKey(
                name: "FK_Cell_CellNote_CellNoteID",
                table: "Cell");

            migrationBuilder.AlterColumn<int>(
                name: "CellNoteID",
                table: "Cell",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "CellLabelID",
                table: "Cell",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddForeignKey(
                name: "FK_Cell_CellLabel_CellLabelID",
                table: "Cell",
                column: "CellLabelID",
                principalTable: "CellLabel",
                principalColumn: "CellLabelID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cell_CellNote_CellNoteID",
                table: "Cell",
                column: "CellNoteID",
                principalTable: "CellNote",
                principalColumn: "CellNoteID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cell_CellLabel_CellLabelID",
                table: "Cell");

            migrationBuilder.DropForeignKey(
                name: "FK_Cell_CellNote_CellNoteID",
                table: "Cell");

            migrationBuilder.AlterColumn<int>(
                name: "CellNoteID",
                table: "Cell",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CellLabelID",
                table: "Cell",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Cell_CellLabel_CellLabelID",
                table: "Cell",
                column: "CellLabelID",
                principalTable: "CellLabel",
                principalColumn: "CellLabelID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cell_CellNote_CellNoteID",
                table: "Cell",
                column: "CellNoteID",
                principalTable: "CellNote",
                principalColumn: "CellNoteID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
