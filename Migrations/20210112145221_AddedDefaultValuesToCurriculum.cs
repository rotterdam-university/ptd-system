﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ptd_system.Migrations
{
    public partial class AddedDefaultValuesToCurriculum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<float>(
                name: "default_organisational",
                table: "Curriculum",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "default_professional",
                table: "Curriculum",
                nullable: false,
                defaultValue: 0f);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "default_organisational",
                table: "Curriculum");

            migrationBuilder.DropColumn(
                name: "default_professional",
                table: "Curriculum");
        }
    }
}
