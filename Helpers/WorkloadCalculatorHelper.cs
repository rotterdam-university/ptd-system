using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ptd_system.Models;
using ptd_system.Controllers.DTO;

namespace ptd_system.Helpers
{
  public class WorkloadCalculatorHelper
  {
    private readonly DatabaseContext _context;
    private readonly float TotalHours = 1659f;

    public WorkloadCalculatorHelper(DatabaseContext context)
    {
      _context = context;
    }

    public float GetHoursAfterDecimal(float op1, float op2, float op3, float op4)
    {
      return (float)(op1 + (op2 - Math.Truncate(op2)) + (op3 - Math.Truncate(op3)) + (op4 - Math.Truncate(op4)));
    }

    public float GetAvailableHoursForOP(float fte)
    {
      // return (fte * (TotalHours / 4)) - (di * (TotalHours));
      return (fte * (TotalHours / 4));
    }

    public DIHours GetDIHours(DI di, FTE fte)
    {
      DIHours hours = new DIHours { total = 0F, total_rounded_down = 0, op1 = 0F, op2 = 0F, op3 = 0F, op4 = 0F };

      // hours.op1 = di.di_op1 * TotalHours * fte.fte_op1;
      // hours.op2 = di.di_op2 * TotalHours * fte.fte_op2;
      // hours.op3 = di.di_op3 * TotalHours * fte.fte_op3;
      // hours.op4 = di.di_op4 * TotalHours * fte.fte_op4;
      // hours.op1 = (int)Math.Ceiling(GetHoursAfterDecimal(hours.op1, hours.op2, hours.op3, hours.op4));
      // hours.op2 = (int)Math.Round(hours.op2);
      // hours.op3 = (int)Math.Round(hours.op3);
      // hours.op4 = (int)Math.Round(hours.op4);
      // hours.total = hours.op1 + hours.op2 + hours.op3 + hours.op4;
      // hours.total_rounded_down = (int)(hours.op1 + hours.op2 + hours.op3 + hours.op4);

      hours.op1 = di.di_op1;
      hours.op2 = di.di_op2;
      hours.op3 = di.di_op3;
      hours.op4 = di.di_op4;
      hours.total = hours.op1 + hours.op2 + hours.op3 + hours.op4;
      hours.total_rounded_down = (int)Math.Round(hours.op1 + hours.op2 + hours.op3 + hours.op4);

      return hours;
    }

    public WorkloadCell GetWorkloadForCell(int cell_id)
    {
      Cell cell = _context.Cell
        .Include(c => c.Course)
        .Include(c => c.Class)
        .Include(c => c.OP)
        .FirstOrDefault(c => c.CellID.Equals(cell_id));

      WorkloadCell workload = new WorkloadCell {
        workload = 0,
        workloadWithRaiseFactor = 0
      };

      float w = cell.Course.meetings;
      float Ct = cell.Course.contact_student;
      float Opslag = cell.Course.raise_factor;
      float n = cell.OP.period.Equals(1)
          ? cell.Class.class_size_op1
        : cell.OP.period.Equals(2)
          ? cell.Class.class_size_op2
        : cell.OP.period.Equals(3)
          ? cell.Class.class_size_op3
        : cell.Class.class_size_op4;
      float Cor = cell.Course.correction_time;
      float R = cell.Course.travel_time;

      // Formule vorm: (w x Ct x opslag) + (n x Cor) + R ) = y min / 60 = z uur

      workload.workload = (int)Math.Round(((w * Ct) + (n * Cor) + R) / 60);
      workload.workloadWithRaiseFactor = (int)Math.Round(((w * Ct * Opslag) + (n * Cor) + R) / 60);

      return workload;
    }

    public SecondmentHours GetSecondmentHours(int teacher_id)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.TeacherSecondments)
        .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

      SecondmentHours hours = new SecondmentHours { total = 0F, total_rounded_down = 0, op1 = 0F, op2 = 0F, op3 = 0F, op4 = 0F };

      teacher.TeacherSecondments.ForEach(s =>
      {
        if (s.active_op1) { hours.op1 = hours.op1 + s.hours; }
        if (s.active_op2) { hours.op2 = hours.op2 + s.hours; }
        if (s.active_op3) { hours.op3 = hours.op3 + s.hours; }
        if (s.active_op4) { hours.op4 = hours.op4 + s.hours; }
      });

      hours.total = hours.op1 + hours.op2 + hours.op3 + hours.op4;
      hours.total_rounded_down = (int)Math.Round(hours.op1 + hours.op2 + hours.op3 + hours.op4);

      return hours;
    }

    public OrganisationalHours GetOrganisationalHours(FTE fte)
    {
      OrganisationalHours hours = new OrganisationalHours { total = 0F, total_rounded_up = 0, op1 = 0F, op2 = 0F, op3 = 0F, op4 = 0F };

      hours.op1 = (int)Math.Round(TotalHours * fte.fte_op1 * ((fte.organisational / 100) / 4));
      hours.op2 = (int)Math.Round(TotalHours * fte.fte_op2 * ((fte.organisational / 100) / 4));
      hours.op3 = (int)Math.Round(TotalHours * fte.fte_op3 * ((fte.organisational / 100) / 4));
      hours.op4 = (int)Math.Round(TotalHours * fte.fte_op4 * ((fte.organisational / 100) / 4));
      hours.total = hours.op1 + hours.op2 + hours.op3 + hours.op4;
      hours.total_rounded_up = (int)(hours.op1 + hours.op2 + hours.op3 + hours.op4);

      return hours;
    }

    public ProfessionalHours GetProfessionalHours(FTE fte)
    {
      ProfessionalHours hours = new ProfessionalHours { total = 0F, total_rounded_up = 0, op1 = 0F, op2 = 0F, op3 = 0F, op4 = 0F };

      hours.op1 = (int)Math.Round(TotalHours * fte.fte_op1 * ((fte.professional / 100) / 4));
      hours.op2 = (int)Math.Round(TotalHours * fte.fte_op2 * ((fte.professional / 100) / 4));
      hours.op3 = (int)Math.Round(TotalHours * fte.fte_op3 * ((fte.professional / 100) / 4));
      hours.op4 = (int)Math.Round(TotalHours * fte.fte_op4 * ((fte.professional / 100) / 4));
      hours.total = hours.op1 + hours.op2 + hours.op3 + hours.op4;
      hours.total_rounded_up = (int)(hours.op1 + hours.op2 + hours.op3 + hours.op4);

      return hours;
    }

    public MOTHours GetMOTTaskHours(int teacher_id, string type)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.MOTs)
        .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

      MOTHours hours = new MOTHours { total = 0F, total_rounded_down = 0, op1 = 0F, op2 = 0F, op3 = 0F, op4 = 0F };

      teacher.MOTs.ForEach(m =>
      {
        if (m.type == type)
        {
          if (m.active_op1) { hours.op1 = hours.op1 + m.hours; }
          if (m.active_op2) { hours.op2 = hours.op2 + m.hours; }
          if (m.active_op3) { hours.op3 = hours.op3 + m.hours; }
          if (m.active_op4) { hours.op4 = hours.op4 + m.hours; }
        }
      });

      hours.total = hours.op1 + hours.op2 + hours.op3 + hours.op4;
      hours.total_rounded_down = (int)Math.Round(hours.op1 + hours.op2 + hours.op3 + hours.op4);

      return hours;
    }

    public ContactTimesAndRaiseFactor GetContactTimesAndRaiseFactor(int teacher_id, int period)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.Cells)
          .ThenInclude(tc => tc.Cell)
            .ThenInclude(c => c.OP)
        .Include(t => t.Cells)
          .ThenInclude(tc => tc.Cell)
            .ThenInclude(c => c.Course)
        .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

      ContactTimesAndRaiseFactor res = new ContactTimesAndRaiseFactor() {
        contactTime = 0f,
        contactTimeWithRaiseFactor = 0f,
        raiseFactor = 0f
      };

      teacher.Cells.Where(c => c.Cell.OP.period.Equals(period)).ToList().ForEach(tc => {
        res.contactTime += GetWorkloadForCell(tc.CellID).workload + tc.teacher_workload_override;
        res.contactTimeWithRaiseFactor += GetWorkloadForCell(tc.CellID).workloadWithRaiseFactor + tc.teacher_workload_override;
        res.raiseFactor += tc.Cell.Course.raise_factor;
      });

      if (res.raiseFactor > 0)
        res.raiseFactor = res.raiseFactor / teacher.Cells.Where(c => c.Cell.OP.period.Equals(period)).ToList().Count();

      res.contactTime = (int)Math.Round(res.contactTime);
      res.contactTimeWithRaiseFactor = (int)Math.Round(res.contactTimeWithRaiseFactor);
      res.raiseFactor = (int)Math.Round(res.raiseFactor);

      return res;
    }

    public InternAndGraduationHours GetSupervisionHours(int teacher_id)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.Supervisions)
        .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

      InternAndGraduationHours res = new InternAndGraduationHours() {
        interns = new SupervisionHours() { total = 0, op1 = 0, op2 = 0, op3 = 0, op4 = 0 },
        graduates = new SupervisionHours() { total = 0, op1 = 0, op2 = 0, op3 = 0, op4 = 0 }
      };

      teacher.Supervisions.ForEach(s => {
        if (s.supervision_kind.Equals("Stagiairs")) {
          if (s.start_op.Equals(1) && s.end_op.Equals(2)) {
            res.interns.total += 14;
            res.interns.op1 += 7;
            res.interns.op2 += 7;
          } else if (s.start_op.Equals(2) && s.end_op.Equals(3)) {
            res.interns.total += 14;
            res.interns.op2 += 7;
            res.interns.op3 += 7;
          } else if (s.start_op.Equals(3) && s.end_op.Equals(4)) {
            res.interns.total += 14;
            res.interns.op3 += 7;
            res.interns.op4 += 7;
          } else if (s.start_op.Equals(4) && s.end_op.Equals(0)) {
            res.interns.total += 7;
            res.interns.op4 += 7;
          }
        }

        if (s.supervision_kind.Equals("Afstudeerders")) {
          if (s.start_op.Equals(1) && s.end_op.Equals(2)) {
            res.graduates.total += 20;
            res.graduates.op1 += 10;
            res.graduates.op2 += 10;
          } else if (s.start_op.Equals(2) && s.end_op.Equals(3)) {
            res.graduates.total += 20;
            res.graduates.op2 += 10;
            res.graduates.op3 += 10;
          } else if (s.start_op.Equals(3) && s.end_op.Equals(4)) {
            res.graduates.total += 20;
            res.graduates.op3 += 10;
            res.graduates.op4 += 10;
          } else if (s.start_op.Equals(4) && s.end_op.Equals(0)) {
            res.graduates.total += 10;
            res.graduates.op4 += 10;
          } else if (s.start_op.Equals(0) && s.end_op.Equals(1)) {
            res.graduates.total += 10;
            res.graduates.op1 += 10;
          }
        }
      });

      return res;
    }

    public int GetOddmentForOP(int teacher_id, int op)
    {
      TeacherTaaktoedelingResponse taaktoedeling = GetAvailableHoursForTaaktoedeling(teacher_id);
      
      switch (op) {
        case 1:
          return taaktoedeling.oddment_op1;
        case 2:
          return taaktoedeling.oddment_op2;
        case 3:
          return taaktoedeling.oddment_op3;
        case 4:
          return taaktoedeling.oddment_op4;
        default:
          return 0;
      }
    }

    public int GetAvailableHoursForStudyyear(int teacher_id)
    {
      TeacherTaaktoedelingResponse taaktoedeling = GetAvailableHoursForTaaktoedeling(teacher_id);
      
      return taaktoedeling.oddment_total;
    }

    public TeacherTaaktoedelingResponse GetAvailableHoursForTaaktoedeling(int teacher_id)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.FTE)
        .Include(t => t.DI)
        .FirstOrDefault(t => t.TeacherID.Equals(teacher_id));

      // Get total available hours
      float AH_OP1 = GetAvailableHoursForOP(teacher.FTE.fte_op1);
      float AH_OP2 = GetAvailableHoursForOP(teacher.FTE.fte_op2);
      float AH_OP3 = GetAvailableHoursForOP(teacher.FTE.fte_op3);
      float AH_OP4 = GetAvailableHoursForOP(teacher.FTE.fte_op4);

      AH_OP1 = GetHoursAfterDecimal(AH_OP1, AH_OP2, AH_OP3, AH_OP4);

      AH_OP1 = (int)Math.Round(AH_OP1);
      AH_OP2 = (int)Math.Round(AH_OP2);
      AH_OP3 = (int)Math.Round(AH_OP3);
      AH_OP4 = (int)Math.Round(AH_OP4);
      int AHTotal = (int)(AH_OP1 + AH_OP2 + AH_OP3 + AH_OP4);

      // DI
      DIHours DIHours = GetDIHours(teacher.DI, teacher.FTE);

      // Secondments
      SecondmentHours secondmentHours = GetSecondmentHours(teacher_id);
      
      // Organizational tasks
      // General
      OrganisationalHours organisational = GetOrganisationalHours(teacher.FTE);

      // MOT
      MOTHours organizationalMOTHours = GetMOTTaskHours(teacher_id, "Organisatorische");

      // Professional tasks
      // General
      ProfessionalHours professional = GetProfessionalHours(teacher.FTE);

      // MOT
      MOTHours professionalMOTHours = GetMOTTaskHours(teacher_id, "Professionalisering");

      // Contacttimes
      ContactTimesAndRaiseFactor contactTimesOP1 = GetContactTimesAndRaiseFactor(teacher_id, 1);
      ContactTimesAndRaiseFactor contactTimesOP2 = GetContactTimesAndRaiseFactor(teacher_id, 2);
      ContactTimesAndRaiseFactor contactTimesOP3 = GetContactTimesAndRaiseFactor(teacher_id, 3);
      ContactTimesAndRaiseFactor contactTimesOP4 = GetContactTimesAndRaiseFactor(teacher_id, 4);

      float totalContactTime = contactTimesOP1.contactTime + contactTimesOP2.contactTime + contactTimesOP3.contactTime + contactTimesOP4.contactTime;
      float totalContactTimeWithRaiseFactor = contactTimesOP1.contactTimeWithRaiseFactor + contactTimesOP2.contactTimeWithRaiseFactor + contactTimesOP3.contactTimeWithRaiseFactor + contactTimesOP4.contactTimeWithRaiseFactor;
      float meanRaiseFactor = (contactTimesOP1.raiseFactor + contactTimesOP2.raiseFactor + contactTimesOP3.raiseFactor + contactTimesOP4.raiseFactor) / 4;

      // Supervision
      InternAndGraduationHours supervisionHours = GetSupervisionHours(teacher_id);

      // Oddment
      float oddmentOP1 = AH_OP1 - DIHours.op1 - secondmentHours.op1 - organisational.op1 - organizationalMOTHours.op1 - professional.op1 - professionalMOTHours.op1 - contactTimesOP1.contactTimeWithRaiseFactor - supervisionHours.interns.op1 - supervisionHours.graduates.op1;
      float oddmentOP2 = AH_OP2 - DIHours.op2 - secondmentHours.op2 - organisational.op2 - organizationalMOTHours.op2 - professional.op2 - professionalMOTHours.op2 - contactTimesOP2.contactTimeWithRaiseFactor - supervisionHours.interns.op2 - supervisionHours.graduates.op2;
      float oddmentOP3 = AH_OP3 - DIHours.op3 - secondmentHours.op3 - organisational.op3 - organizationalMOTHours.op3 - professional.op3 - professionalMOTHours.op3 - contactTimesOP3.contactTimeWithRaiseFactor - supervisionHours.interns.op3 - supervisionHours.graduates.op3;
      float oddmentOP4 = AH_OP4 - DIHours.op4 - secondmentHours.op4 - organisational.op4 - organizationalMOTHours.op4 - professional.op4 - professionalMOTHours.op4 - contactTimesOP4.contactTimeWithRaiseFactor - supervisionHours.interns.op4 - supervisionHours.graduates.op4;
      int oddmentTotal = (int)(oddmentOP1 + oddmentOP2 + oddmentOP3 + oddmentOP4);

      TeacherTaaktoedelingResponse taaktoedeling = new TeacherTaaktoedelingResponse()
      {
        total_available_hours = AHTotal,
        total_available_hours_op1 = (int)Math.Round(AH_OP1),
        total_available_hours_op2 = (int)Math.Round(AH_OP2),
        total_available_hours_op3 = (int)Math.Round(AH_OP3),
        total_available_hours_op4 = (int)Math.Round(AH_OP4),
        di = new TeacherTaaktoedelingDIResponse() {
          total = DIHours.total_rounded_down,
          op1 = (int)Math.Round(DIHours.op1),
          op2 = (int)Math.Round(DIHours.op2),
          op3 = (int)Math.Round(DIHours.op3),
          op4 = (int)Math.Round(DIHours.op4)
        },
        secondments = new TeacherTaaktoedelingSecondmentsResponse() {
          total = secondmentHours.total_rounded_down,
          op1 = (int)Math.Round(secondmentHours.op1),
          op2 = (int)Math.Round(secondmentHours.op2),
          op3 = (int)Math.Round(secondmentHours.op3),
          op4 = (int)Math.Round(secondmentHours.op4),
        },
        organizational_tasks = new TeacherTaaktoedelingOrganizationalTasksResponse() {
          total = organisational.total_rounded_up,
          op1 = (int)Math.Round(organisational.op1),
          op2 = (int)Math.Round(organisational.op2),
          op3 = (int)Math.Round(organisational.op3),
          op4 = (int)Math.Round(organisational.op4),
        },
        organization_tasks_other = new TeacherTaaktoedelingOrganizationalTasksOtherResponse() {
          total = organizationalMOTHours.total_rounded_down,
          op1 = (int)Math.Round(organizationalMOTHours.op1),
          op2 = (int)Math.Round(organizationalMOTHours.op2),
          op3 = (int)Math.Round(organizationalMOTHours.op3),
          op4 = (int)Math.Round(organizationalMOTHours.op4),
        },
        professionalization_tasks = new TeacherTaaktoedelingProfessionalizationTasksResponse() {
          total = professional.total_rounded_up,
          op1 = (int)Math.Round(professional.op1),
          op2 = (int)Math.Round(professional.op2),
          op3 = (int)Math.Round(professional.op3),
          op4 = (int)Math.Round(professional.op4),
        },
        professionalization_tasks_other = new TeacherTaaktoedelingProfessionalizationTasksOtherResponse() {
          total = professionalMOTHours.total_rounded_down,
          op1 = (int)Math.Round(professionalMOTHours.op1),
          op2 = (int)Math.Round(professionalMOTHours.op2),
          op3 = (int)Math.Round(professionalMOTHours.op3),
          op4 = (int)Math.Round(professionalMOTHours.op4),
        },
        contact_time = new TeacherTaaktoedelingContactTimeResponse() {
          total = (int)Math.Round(totalContactTime),
          op1 = (int)Math.Round(contactTimesOP1.contactTime),
          op2 = (int)Math.Round(contactTimesOP2.contactTime),
          op3 = (int)Math.Round(contactTimesOP3.contactTime),
          op4 = (int)Math.Round(contactTimesOP4.contactTime)
        },
        contact_time_with_factor = new TeacherTaaktoedelingContactTimeWithFactorResponse() {
          total = (int)Math.Round(totalContactTimeWithRaiseFactor),
          op1 = (int)Math.Round(contactTimesOP1.contactTimeWithRaiseFactor),
          op2 = (int)Math.Round(contactTimesOP2.contactTimeWithRaiseFactor),
          op3 = (int)Math.Round(contactTimesOP3.contactTimeWithRaiseFactor),
          op4 = (int)Math.Round(contactTimesOP4.contactTimeWithRaiseFactor)
        },
        supervision = new TeacherTaaktoedelingInternsAndGraduatesSupervisionResponse() {
          interns = new TeacherTaaktoedelingSupervisionResponse() {
            total = supervisionHours.interns.total,
            op1 = supervisionHours.interns.op1,
            op2 = supervisionHours.interns.op2,
            op3 = supervisionHours.interns.op3,
            op4 = supervisionHours.interns.op4
          },
          graduates = new TeacherTaaktoedelingSupervisionResponse() {
            total = supervisionHours.graduates.total,
            op1 = supervisionHours.graduates.op1,
            op2 = supervisionHours.graduates.op2,
            op3 = supervisionHours.graduates.op3,
            op4 = supervisionHours.graduates.op4
          }
        },
        mean_raise_factor = Math.Round(meanRaiseFactor, 2),
        oddment_total = oddmentTotal,
        oddment_op1 = (int)Math.Round(oddmentOP1),
        oddment_op2 = (int)Math.Round(oddmentOP2),
        oddment_op3 = (int)Math.Round(oddmentOP3),
        oddment_op4 = (int)Math.Round(oddmentOP4)
      };

      return taaktoedeling;
    }

    public class WorkloadCell
    {
      public int workload { get; set; }
      public int workloadWithRaiseFactor { get; set; }
    }

    public class DIHours
    {
      public float total { get; set; }
      public int total_rounded_down { get; set; }
      public float op1 { get; set; }
      public float op2 { get; set; }
      public float op3 { get; set; }
      public float op4 { get; set; }
    }

    public class SecondmentHours
    {
      public float total { get; set; }
      public int total_rounded_down { get; set; }
      public float op1 { get; set; }
      public float op2 { get; set; }
      public float op3 { get; set; }
      public float op4 { get; set; }
    }

    public class OrganisationalHours
    {
      public float total { get; set; }
      public int total_rounded_up { get; set; }
      public float op1 { get; set; }
      public float op2 { get; set; }
      public float op3 { get; set; }
      public float op4 { get; set; }
    }

    public class ProfessionalHours
    {
      public float total { get; set; }
      public int total_rounded_up { get; set; }
      public float op1 { get; set; }
      public float op2 { get; set; }
      public float op3 { get; set; }
      public float op4 { get; set; }
    }

    public class MOTHours
    {
      public float total { get; set; }
      public int total_rounded_down { get; set; }
      public float op1 { get; set; }
      public float op2 { get; set; }
      public float op3 { get; set; }
      public float op4 { get; set; }
    }

    public class ContactTimesAndRaiseFactor
    {
      public float contactTime { get; set; }
      public float contactTimeWithRaiseFactor { get; set; }
      public float raiseFactor { get; set; }
    }

    public class InternAndGraduationHours
    {
      public SupervisionHours interns { get; set; }
      public SupervisionHours graduates { get; set; }
    }

    public class SupervisionHours
    {
      public int total { get; set; }
      public int op1 { get; set; }
      public int op2 { get; set; }
      public int op3 { get; set; }
      public int op4 { get; set; }
    }
  }
}