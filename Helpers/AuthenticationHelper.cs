using System.Linq;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ptd_system.Models;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace ptd_system.Helpers
{
  public class AuthenticationHelper
  {
    private readonly DatabaseContext _context;
    public AuthenticationHelper(DatabaseContext context)
    {
      _context = context;
    }

    public AuthenticatedUser ValidateUser(ClaimsPrincipal user)
    {
      // if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT").Equals("Development"))
      // {
      //   return new AuthenticatedUser {
      //     institute = "CMI",
      //     education = "INF",
      //     personnel_code = "HOFSA",
      //     role = "manager"
      //   };
      // } 
      var claimsIdentity = user.Identities.FirstOrDefault(id => id.AuthenticationType == "CWIPS");

      if (user != null)
      {
        List<string> UserProfile = claimsIdentity.Claims
        .Where(claim => claim.Type == System.Security.Claims.ClaimTypes.Role)
          .Select(claim => claim.Value).ToList();
        
        string userCode = "";
        string role = "";
        string institute = "";

        foreach (var item in UserProfile)
        {
          if (item.Length == 7)
            userCode = item.ToUpper();

          if (item.Length == 5)
            userCode = item.ToUpper();
            
          if (_context.Institute.Any(i => i.name.Equals(item.ToUpper())))
            institute = item.ToUpper();
        }

        foreach (var item in UserProfile)
        {
          if (item == "dop")
          {
            role = "dop"; // Medewerker docerend personeel
          } else if (item == "obp")
          {
            role = "obp"; // Medewerker ondersteunend personeel
          } else if (item == "stud")
          {
            role = "stud"; // Student
          }
        }

        Manager manager = _context.Manager
          .Include(m => m.Education)
          .FirstOrDefault(m => m.code.Equals(userCode));

        if (manager != null) {
          return new AuthenticatedUser {
            institute = institute,
            education = manager.Education.name,
            personnel_code = userCode,
            role = "manager"
          };
        } else {
          System.Console.WriteLine($"No manager found");
          if (role == "stud")
            throw new System.UnauthorizedAccessException();

          Teacher teacher = teacher = _context.Teacher
            .Where(t => t.personnel_code.Equals(userCode)).OrderByDescending(t => t.Curriculum.end_date)
            .Include(t => t.Curriculum)
              .ThenInclude(c => c.Education)
              .ThenInclude(e => e.Institute)
            .FirstOrDefault(t => t.Curriculum.end_date < DateTime.Now);

          if (teacher != null) {
            return new AuthenticatedUser {
              institute = institute,
              education = teacher.Curriculum.Education.name,
              personnel_code = teacher.personnel_code,
              role = "teacher"
            };
          } else {
            throw new System.UnauthorizedAccessException();
          }
        }
        throw new System.UnauthorizedAccessException();
      } else {
        throw new System.UnauthorizedAccessException();
      }
    }
  }

  public class AuthenticatedUser
  {
    public string institute { get; set; }
    public string education { get; set; }
    public string role { get; set; }
    public string personnel_code { get; set; }
  }
}