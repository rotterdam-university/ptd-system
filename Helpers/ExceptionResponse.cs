namespace ptd_system.Helpers
{
  public static class ExceptionResponse
  {
    public static dynamic Create(string type, string message = null)
    {
      if (message == null)
      {
        return new Error { Type = type };
      } else {
        return new ErrorWithMessage { Type = type, Message = message };
      }
    }
  }

  public class Error
  {
    public string Type { get; set; }
  }

  public class ErrorWithMessage
  {
    public string Type { get; set; }
    public string Message { get; set; }
  }
}