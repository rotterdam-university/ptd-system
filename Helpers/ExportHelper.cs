using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using ptd_system.Models;
using ptd_system.Controllers.DTO;

namespace ptd_system.Helpers
{
  public class ExportHelper
  {
    private readonly DatabaseContext _context;
    private readonly WorkloadCalculatorHelper _WorkloadCalculatorHelper;

    public ExportHelper(DatabaseContext context)
    {
      _context = context;
      _WorkloadCalculatorHelper = new WorkloadCalculatorHelper(context);
    }

    public TeacherExportPersonalia ExportTeacherPersonalia(int id)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.Position)
        .Include(t => t.Teams)
          .ThenInclude(t => t.Team)
        .Include(t => t.FTE)
        .FirstOrDefault(t => t.TeacherID.Equals(id));

      return new TeacherExportPersonalia
      {
        personnel_code = teacher.personnel_code,
        name = teacher.first_name,
        surname = teacher.last_name,
        position = teacher.Position.name,
        team = teacher.Teams.First().Team.name,
        contract = teacher.FTE.contract
      };
    }

    public List<TeacherExportOnderwijstaken> ExportTeacherOnderwijstaken(int id)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.Cells)
          .ThenInclude(tc => tc.Cell)
            .ThenInclude(c => c.Course)
              .ThenInclude(c => c.CurriculumKind)
        .Include(t => t.Cells)
          .ThenInclude(tc => tc.Cell)
            .ThenInclude(c => c.Course)
              .ThenInclude(c => c.Workmethods)
                .ThenInclude(wm => wm.Workmethod)
        .Include(t => t.Cells)
          .ThenInclude(tc => tc.Cell)
            .ThenInclude(c => c.OP)
        .Include(t => t.Cells)
          .ThenInclude(tc => tc.Cell)
            .ThenInclude(c => c.Class)
        .Include(t => t.Curriculum)
        .FirstOrDefault(t => t.TeacherID.Equals(id));

      return teacher.Cells
        .Select(c => new TeacherExportOnderwijstaken {
          period = c.Cell.OP.period,
          course_code = c.Cell.Course.code,
          course_name = c.Cell.Course.name,
          curriculum_kind = c.Cell.Course.CurriculumKind.name,
          workmethod = c.Cell.Course.Workmethods.First().Workmethod.name,
          student_amount = c.Cell.OP.period.Equals(1)
            ? c.Cell.Class.class_size_op1
          : c.Cell.OP.period.Equals(2)
            ? c.Cell.Class.class_size_op2
          : c.Cell.OP.period.Equals(3)
            ? c.Cell.Class.class_size_op3
          : c.Cell.Class.class_size_op4,
          contact_student = c.Cell.Course.contact_student,
          meetings = c.Cell.Course.meetings,
          meeting_duration = c.Cell.Course.meeting_duration,
          raise_factor = c.Cell.Course.raise_factor,
          correction_time = c.Cell.Course.correction_time,
          travel_time = c.Cell.Course.travel_time
        }).ToList();
    }

    public TeacherExportTaaktoedeling ExportTeacherTaaktoedeling(int id)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.FTE)
        .Include(t => t.DI)
        .FirstOrDefault(t => t.TeacherID.Equals(id));

      return new TeacherExportTaaktoedeling {
        fte_op1 = teacher.FTE.fte_op1,
        fte_op2 = teacher.FTE.fte_op2,
        fte_op3 = teacher.FTE.fte_op3,
        fte_op4 = teacher.FTE.fte_op4,
        di_op1 = teacher.DI.di_op1,
        di_op2 = teacher.DI.di_op2,
        di_op3 = teacher.DI.di_op3,
        di_op4 = teacher.DI.di_op4,
        organisational = teacher.FTE.organisational,
        professional = teacher.FTE.professional,
        taaktoedeling = _WorkloadCalculatorHelper.GetAvailableHoursForTaaktoedeling(id)
      };
    }

    public List<TeacherExportMOT> ExportTeacherMot(int id)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.MOTs)
        .FirstOrDefault(t => t.TeacherID.Equals(id));

      return teacher.MOTs
        .OrderBy(mot => mot.name)
        .Select(mot => new TeacherExportMOT {
        name = mot.name,
        type = mot.type,
        hours = mot.hours,
        op1 = mot.active_op1,
        op2 = mot.active_op2,
        op3 = mot.active_op3,
        op4 = mot.active_op4
      }).ToList();
    }

    public List<TeacherExportSecondment> ExportTeacherSecondment(int id)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.TeacherSecondments)
        .FirstOrDefault(t => t.TeacherID.Equals(id));

      return teacher.TeacherSecondments.Select(s => new TeacherExportSecondment {
        hours = s.hours,
        reason = s.reason,
        op1 = s.active_op1,
        op2 = s.active_op2,
        op3 = s.active_op3,
        op4 = s.active_op4
      }).ToList();
    }

    public TeacherBase ExportTeacher(int id, ExportTeacherConfig config)
    {
      Teacher teacher = _context.Teacher
        .Include(t => t.Curriculum)
          .ThenInclude(c => c.Education)
            .ThenInclude(c => c.Institute)
        .FirstOrDefault(t => t.TeacherID.Equals(id));

      TeacherBase teacher_base = new TeacherBase
      {
        name = teacher.first_name,
        surname = teacher.last_name,
        personnel_code = teacher.personnel_code,
        education = teacher.Curriculum.Education.name,
        institute = teacher.Curriculum.Education.Institute.name,
        curriculum = teacher.Curriculum.name,
      };

      if (config.personalia)
        teacher_base.personalia = ExportTeacherPersonalia(id);

      if (config.onderwijstaken)
        teacher_base.onderwijstaken = ExportTeacherOnderwijstaken(id);

      if (config.taaktoedeling)
        teacher_base.taaktoedeling = ExportTeacherTaaktoedeling(id);

      if (config.mot)
        teacher_base.mot = ExportTeacherMot(id);

      if (config.secondments)
        teacher_base.secondments = ExportTeacherSecondment(id);

      return teacher_base;
    }

    public PlanningBase ExportPlanning(string curriculum, string kind, int study_year, int op, ExportPlanningConfig config)
    {
      Curriculum curr = _context.Curriculum
        .Include(c => c.Education)
          .ThenInclude(e => e.Institute)
        .FirstOrDefault();

      PlanningBase planning_base = new PlanningBase
      {
        study_year = study_year,
        op = op,
        curriculum = curr.name,
        education = curr.Education.name,
        institute = curr.Education.Institute.name
      };

      return planning_base;
    }
  }

  public class ExportTeacherConfig
  {
    public bool personalia { get; set; }
    public bool taaktoedeling { get; set; }
    public bool mot { get; set; }
    public bool secondments { get; set; }
    public bool onderwijstaken { get; set; }
  }

  public class ExportPlanningConfig
  {
    public bool notes { get; set; }
  }

  public class TeacherBase
  {
    public string name { get; set; }
    public string surname { get; set; }
    public string personnel_code { get; set; }
    public string curriculum { get; set; }
    public string education { get; set; }
    public string institute { get; set; }
    public TeacherExportPersonalia personalia { get; set; }
    public List<TeacherExportOnderwijstaken> onderwijstaken { get; set; }
    public TeacherExportTaaktoedeling taaktoedeling { get; set; }
    public List<TeacherExportMOT> mot { get; set; }
    public List<TeacherExportSecondment> secondments { get; set; }
  }

  public class TeacherExportPersonalia
  {
    public string personnel_code { get; set; }
    public string name { get; set; }
    public string surname { get; set; }
    public string position { get; set; }
    public string team { get; set; }
    public string contract { get; set; }
  }

  public class TeacherExportOnderwijstaken
  {
    public int period { get; set; }
    public string course_code { get; set; }
    public string course_name { get; set; }
    public string curriculum_kind { get; set; }
    public string workmethod { get; set; }
    public float student_amount { get; set; }
    public float contact_student { get; set; }
    public float meetings { get; set; }
    public float meeting_duration { get; set; }
    public float raise_factor { get; set; }
    public float correction_time { get; set; }
    public float travel_time { get; set; }
  }

  public class TeacherExportTaaktoedeling
  {
    public float fte_op1 { get; set; }
    public float fte_op2 { get; set; }
    public float fte_op3 { get; set; }
    public float fte_op4 { get; set; }
    public float di_op1 { get; set; }
    public float di_op2 { get; set; }
    public float di_op3 { get; set; }
    public float di_op4 { get; set; }
    public float organisational { get; set; }
    public float professional { get; set; }
    public TeacherTaaktoedelingResponse taaktoedeling { get; set; }
  }

  public class TeacherExportMOT
  {
    public string name { get; set; }
    public string type { get; set; }
    public float hours { get; set; }
    public bool op1 { get; set; }
    public bool op2 { get; set; }
    public bool op3 { get; set; }
    public bool op4 { get; set; }
  }

  public class TeacherExportSecondment
  {
    public float hours { get; set; }
    public string reason { get; set; }
    public bool op1 { get; set; }
    public bool op2 { get; set; }
    public bool op3 { get; set; }
    public bool op4 { get; set; }
  }

  public class PlanningBase
  {
    public int study_year { get; set; }
    public int op { get; set; }
    public string curriculum { get; set; }
    public string education { get; set; }
    public string institute { get; set; }
  }
}